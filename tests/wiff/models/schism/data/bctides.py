import unittest

from wiff.models.schism.data.bctides import BCTidesIn


class BctidesIn(unittest.TestCase):

    _no_boundaries = (
        'test',
        '0 0.0',
        '0',
        '0',
    )

    def test_no_boundaries(self):
        bctides = BCTidesIn(header='test')

        self.assertEqual(bctides.render(), self._no_boundaries)
