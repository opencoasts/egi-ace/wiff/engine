import requests


class ncWMS:

    # convert to classmethod
    url_template = '{schema}://{host}:{port}{path}'
    url_composer = lambda host=None, port=None, path='', is_secure=False: (
        __class__.url_template.format(
            schema = 'https' if is_secure else 'http',
            host = host,
            port = port or (443 if is_secure else 80),
            path = path,
        )
    )

    def __init__(self, url, auth=None):

        self.url = url
        self.auth = auth

    API_URLS = dict(
        info = '{host}/api/datasetinfo',
        status = '{host}/admin/datasetStatus',
        refresh = '{host}/refresh/dataset',

        add = '{host}/admin/addDataset',
        remove = '{host}/admin/removeDataset',
    )
    api_url = lambda self, endpoint: self.API_URLS[endpoint].format(
                                                                  host=self.url)

    get = lambda self, endpoint, params: requests.get(
        url = self.api_url(endpoint),
        auth = self.auth,
        params = params,
    )

    # def get(self, endpoint, params):

    #     return requests.get(
    #         url = self.api_url(endpoint),
    #         auth = self.auth,
    #         params = params,
    #     )

    post = lambda self, endpoint, data: requests.post(
        url = self.api_url(endpoint),
        auth = self.auth,
        data = data,
    )

    # def post(self, endpoint, data):

    #     return requests.post(
    #         url = self.api_url(endpoint),
    #         auth = self.auth,
    #         data = data,
    #     )

    def dataset_info(self, did):

        params = dict(
            id = did,
        )
        return self.get('info', params)

    def dataset_add(self, did, title, location, copyright='', more_info='',
          queryable=True, downloadable=False, auto_refresh_minutes=-1, **extra):

        new_dataset_config = dict(
            id = did,
            title = title,
            location = location,
            moreInfo = more_info,
            copyright = copyright,
            queryable = queryable,
            downloadable = downloadable,
            autoRefreshMinutes = auto_refresh_minutes,

            **extra
        )
        return self.post('add', new_dataset_config)

    def dataset_refresh(self, did):

        params = dict(
            id = did,
        )
        return self.get('refresh', params)

    def dataset_remove(self, did):

        data = dict(
            id = did,
        )
        return self.post('remove', data)
