import numpy as np
from datetime import datetime, date, time, timedelta
from netCDF4 import Dataset, MFDataset, num2date, date2num


class BaseNetCDF:

    default_global_attrs = dict()

    def __init__(self, file_name, *, time_data, time_attrs=None,
                                              global_attrs=None, compress=True):
        self.dataset = Dataset(file_name, mode='w')

        global_attrs = global_attrs or dict()
        self.dataset.setncatts({**self.default_global_attrs, **global_attrs})

        self.compress = compress
        self.default_dimensions = list()
        self.default_variable_attrs = dict()

        self._add_time_variable(time_data, time_attrs)

    @classmethod
    def from_datetime(cls, *args, instants, units='hours', calendar='standard',
                                     reference=None, time_attrs=None, **kwargs):
        reference = reference or f'{instants[0]:%Y-%m-%d %H:%M:%S}'
        time_units = f'{units} since {reference}'

        time_data = date2num(instants, time_units, calendar)

        dt_time_attrs = dict(
            units = time_units,
            calendar = 'standard',
            reference = reference,
        )
        if time_attrs:
            dt_time_attrs.update(time_attrs)

        return cls(*args, time_data=time_data, time_attrs=dt_time_attrs,
                                                                       **kwargs)

    def create_dimension(self, name, size=None, *, default=False):
        if default:
            self.default_dimensions.append(name)
        return self.dataset.createDimension(name, size)

    default_variable_attrs_map = dict(
        time = dict(
            standard_name = 'time',
            long_name = 'Time',
        ),
        lon = dict(
            standard_name = 'longitude',
            long_name = 'Longitude',
            units = 'degrees_east',
        ),
        lat = dict(
            standard_name = 'latitude',
            long_name = 'Latitude',
            units = 'degrees_north',
        ),
        depth = dict(
            standard_name = 'depth',
            long_name = 'Depth',
            units = 'm',
        ),
        height = dict(
            standard_name = 'height',
            long_name = 'Height',
            units = 'm',
        ),
    )

    def add_variable(self, name, data, attributes=None, *, kind='f',
                                                     dimensions=None, **kwargs):
        kwargs.setdefault('zlib', self.compress)
        #print(name, kwargs)

        dimensions = dimensions or self.default_dimensions
        #print(dimensions)

        attributes = attributes or self.default_variable_attrs_map.get(name) \
                                                                       or dict()
        all_attributes = {**self.default_variable_attrs, **attributes}
        #print(all_attributes)

        var = self.dataset.createVariable(name, kind, dimensions, **kwargs)
        var.setncatts(all_attributes)
        var[...] = data
        print(var)

        return var

    def _add_time_variable(self, data, attributes=None):
        self.create_dimension('time', default=True)
        return self.add_variable('time', data, attributes)

    def close(self):
        self.dataset.close()


class ncCF(BaseNetCDF):
    '''http://cfconventions.org/Data/cf-documents/requirements-recommendations/conformance-1.8.html'''

    default_global_attrs = dict(BaseNetCDF.default_global_attrs,
        Conventions = 'CF-1.8',
    )

    def __init__(self, *base_args, coordinates, vertical=None, **base_kwargs):
        super().__init__(*base_args, **base_kwargs)

        self._add_space_variables(coordinates, vertical)

    def _add_space_variables(self, coordinates, vertical=None):

        space_vars = list()
        def new_space_var_from_(var_map):
            dim_name = f"grid_{var_map['name']}"
            self.create_dimension(dim_name, len(var_map['data']), default=True)
            space_vars.append(
                           self.add_variable(dimensions=[dim_name], **var_map) )

        if vertical:
            new_space_var_from_(vertical)

        for coord_map in coordinates:
            new_space_var_from_(coord_map)

        self.default_variable_attrs.update(
            coordinates = ' '.join( cmap['name'] for cmap in coordinates ),
        )
        return tuple(space_vars)


class ncUGRID(BaseNetCDF):
    '''https://ugrid-conventions.github.io/ugrid-conventions/'''

    default_global_attrs = dict(BaseNetCDF.default_global_attrs,
        Conventions = 'UGRID-1.0',
    )

    def __init__(self, *base_args, coordinates, elements, vertical=None,
                                                                 **base_kwargs):
        super().__init__(*base_args, **base_kwargs)

        self._add_space_variables(coordinates, elements, vertical)

    def _add_space_variables(self, coordinates, elements, vertical=None):

        self.create_dimension('face_size', 3)
        self.create_dimension('face', len(elements) )

        mesh = self.dataset.createVariable('mesh', 'c')
        mesh.setncatts( dict(
            cf_role = 'mesh_topology',
            topology_dimension = 2,
            node_coordinates = ' '.join(coordinates),
            face_node_connectivity = 'face_nodes',
            long_name = "Topology data of 2D unstructured mesh",
        ) )

        face_nodes = self.dataset.createVariable('face_nodes', 'u4',
                                      ('face', 'face_size'), zlib=self.compress)
        face_nodes.setncatts( dict(
            cf_role = 'face_node_connectivity',
            start_index = 1,
            long_name = "Maps every face to its corner nodes",
        ) )
        face_nodes[...] = elements

        coords = tuple( coordinates.items() )

        self.create_dimension('node', len(coords[0][1]), default=True)

        for name, data in coords:
            var = self.add_variable(name, data, dimensions=['node'])

        # TODO: vertical aspect

        self.default_variable_attrs.update(
            coordinates = ' '.join(coordinates),
            mesh = 'mesh',
            location = 'node',
        )


if __name__ == '__main__':
    from sys import argv
    out_nc, in_nc = argv[1:3]

    print('in =', in_nc, '| out =', out_nc)

    # in_ds = Dataset(in_nc)
    # print(in_ds)

    # today = datetime.combine(date.today(), time() )
    # instants = tuple( today + timedelta(hours=offset) for offset in range(73) )

    # coords = (
    #     dict(
    #         name = 'lat',
    #         data = in_ds.variables['lat'][:,0],
    #     ),
    #     dict(
    #         name = 'lon',
    #         data = in_ds.variables['lon'][0],
    #     ),
    # )

    # nc = ncCF.from_datetime(out_nc, instants=instants, coordinates=coords)
    # nc.add_variable('u_wind', in_ds.variables['uwind'][...], dict(
    #     long_name='Surface Eastward Air Velocity (10m AGL)',
    #     standard_name = 'eastward_wind',
    #     units = 'm/s',
    # ) )
    # nc.add_variable('v_wind', in_ds.variables['vwind'][...], dict(
    #     long_name='Surface Northward Air Velocity (10m AGL)',
    #     standard_name = 'northward_wind',
    #     units = 'm/s',
    # ) )
    # nc.add_variable('air_temp', in_ds.variables['stmp'][...], dict(
    #     long_name = 'Surface Air Temperature (2m AGL)',
    #     standard_name = 'air_temperature',
    #     units = 'K',
    # ) )
    # nc.close()

    in_ds = MFDataset(in_nc)
    print(in_ds)

    time_var = in_ds.variables['time'][...]

    coords = dict(
        lat = in_ds.variables['latitude'][...],
        lon = in_ds.variables['longitude'][...],
    )
    print(in_ds.variables['time'].ncattrs())

    nc = ncUGRID(out_nc,
        time_data = in_ds.variables['time'][...],
        time_attrs = dict(
            units = in_ds.variables['time'].units,
        ),
        coordinates=coords,
        elements=in_ds.variables['tri'][...]
    )
    nc.add_variable('hs', in_ds.variables['hs'][...], dict(
        long_name = 'Significant Height Of Wind And Swell Waves',
        standard_name = 'sea_surface_wave_significant_height',
        units = 'm',
    ) )
    nc.close()
