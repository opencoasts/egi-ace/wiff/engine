from collections import OrderedDict
from datetime import time as dt_time, timedelta as dt_timedelta
from functools import lru_cache
from logging import getLogger
from netCDF4 import Dataset
from numpy import logical_and, concatenate


class Editing:

    @classmethod
    def _open_datasets(cls, paths):
        datasets = list()

        try:
            for path in paths:
                datasets.append( Dataset(path) )
        except:
            cls._close_datasets(datasets)
            raise

        return datasets

    @staticmethod
    def _close_datasets(datasets):
        for dataset in datasets:
            dataset.close()

    @staticmethod
    def join_datasets_along_time(output_dataset, input_datasets,
                      dim_time_name='time', exclude_attrs=(), compression=True):

        def copy_meta(input_ds, output_ds):
            for dim in input_ds.dimensions.values():

                dim_size = None if dim.name == dim_time_name else dim.size
                output_ds.createDimension(dim.name, dim_size)

            not_excluded_name = lambda item: item[0] not in exclude_attrs
            filtered_attrs_items = filter(not_excluded_name,
                                                    input_ds.__dict__.items() )

            # TODO: handle changing attrs (different in each dataset)
            output_ds.setncatts( dict(filtered_attrs_items) )

        variables_time_axis_map = dict()

        def setup_variables(input_ds, output_ds):
            for var in input_ds.variables.values():

                output_var = output_ds.createVariable(var.name, var.datatype,
                                               var.dimensions, zlib=compression)
                output_var.setncatts(var.__dict__)

                # if a variable doesn't have time dimension (lat/lon ), it's
                # assumed to be replicated in all input files, hence it's copied
                # only once
                try:
                    time_axis = var.dimensions.index(dim_time_name)
                    variables_time_axis_map[var.name] = time_axis

                except ValueError:
                    output_var[:] = var[:]


        def join_variable_extents(input_dses, output_ds):
            for name, time_axis in variables_time_axis_map.items():

                variable_extents = tuple( dataset.variables[name][:]
                                                     for dataset in input_dses )

                output_ds.variables[name][:] = concatenate(variable_extents,
                                                                 axis=time_axis)

        copy_meta(input_datasets[0], output_dataset)
        setup_variables(input_datasets[0], output_dataset)
        join_variable_extents(input_datasets, output_dataset)


    @classmethod
    def join_files_along_time(cls, output_path, input_paths, **kwargs):

        with Dataset(output_path, mode='w') as output_dataset:

            input_datasets = cls._open_datasets(input_paths)

            try:
                cls.join_datasets_along_time(output_dataset, input_datasets,
                                                                       **kwargs)

            finally:
                cls._close_datasets(input_datasets)


def _main_join(out_nc, *in_ncs):

    Editing.join_files_along_time(out_nc, in_ncs,
        exclude_attrs=(
            'variables_time_axis_map',
            'southernmost_latitude',
            'northernmost_latitude',
            'latitude_resolution',
            'westernmost_longitude',
            'easternmost_longitude',
            'longitude_resolution',
            'minimum_altitude',
            'maximum_altitude',
            'altitude_resolution',
            'start_date',
            'stop_date',
        )
    )

_main_func = _main_join

if __name__ == '__main__':
    from sys import argv
    _main_func(*argv[1:])