from wiff.engine.parts import Stage, Layer


class Prepare(Layer):

    date_time = Layer.Input() # Series provides it

    def execute(self):
        self.logger.info(
            'prepering simulation environment for %s', self.date_time)

    def finalize(self):
        self.logger.info(
            'finalizing simulation environment for %s',self.date_time)


class Compute(Stage):

    model = Stage.Input() # Must be provided by the user since nothing does it
    execution_date_time = Stage.Output()

    def execute(self):
        self.logger.info('executing %s simulation model', self.model)
        self.execution_date_time = datetime.now()


class Finalize(Stage):

    execution_date_time = Stage.Input()

    def execute(self):
        self.logger.info(
            'processing model results of run %s', self.execution_date_time)


if __name__ == '__main__':
    import logging
    logging.basicConfig(
        level=logging.INFO, format='%(asctime)s :: %(name)s %(message)s')

    from datetime import datetime, timedelta, date, time

    today = datetime.combine( date.today(), time(0) )
    one_day = timedelta(days=1)

    from wiff.engine.parts import Series, Simulation

    Series('forecast',
        begin = today,
        # end = today + (7*one_day),
        interval = one_day,

        simulation_templates = (
            Simulation.template('simulation',

                stage_templates = (
                    Prepare.template('a'),
                    Compute.template('b', model='schism'),
                    Finalize.template('c'),
                )
            ),
        )
    ).simulate()

