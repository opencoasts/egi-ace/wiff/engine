import os.path
import shutil
from functools import wraps
from pathlib import Path

from wiff.core import utils
from wiff.core.traits import Registry


class FileHandler:
    """Provides file manipulation means to :mod:`wiff.engine.parts` classes

    This mixin class implements file manipulation functions to extend
    :mod:`engine.core.parts` classes and provide them with extra functionality.

    See Also
    --------
    engine.core.mixins.process_handler.ProcessHandler :
        similar mixin class to handle process execution

    Example
    -------
    .. code::

        class DoStuff(Stage, FileHandler):

            def execute(self):
                self.create_folder('foo')
    """

    # TODO: auto (on __init__) copy/link files, from a pre-defined list

    path = Registry.Input(default=Path.cwd)
    """default working path"""
    static_path = Registry.Input(default=Path.cwd)
    """default static files path"""
    file_overwrite = Registry.Input(default=False)
    """forces file overwriting on all (relevant) methods"""
    cache_path = Registry.Input(optional=True)
    """path used by :meth:`cache_files` decorator"""

    def __resolve_path(self, target, base_path=None):
        path = Path(target or '')

        if not path.is_absolute():
            path = (base_path or self.path) / target

        return path

    def __resolve_endpoint_paths(self, source, destination, base_path=None):
        if not source:
            raise ValueError('source must be defined!')

        source_path = self.__resolve_path(source, base_path)
        destination_path = self.__resolve_path(destination or source_path.name)

        return source_path, destination_path

    def link_file(self, source, destination=None, *, check=True, resolve=False,
                                                    relative=True, force=False):
        """creates a symbolic link `destination`, pointing to `source`

        Arguments
        ---------
        source : path-like
            path pointed by link, dirname defaults to :attr:`path`
        destination : path-like
            new link file, defaults to :attr:`path` and `source`'s name
        check : bool
            check if `source` exists, raises :exc:`FileNotFoundError` if not
        resolve : bool
            makes the `source` path absolute, resolving any symlinks and `..`
        relative : bool
            makes the link relative
        force : bool
            removes `destination` if it exists and is a link
        """

        source_path, destination_path = self.__resolve_endpoint_paths(source,
                                                                    destination)

        if check and not source_path.exists():
            raise FileNotFoundError(source_path)

        # should it be here, or before check or after relative?
        if resolve:
            source_path = source_path.resolve()

        if relative:
            source_path = Path(
                         os.path.relpath(source_path, destination_path.parent) )

        if (force or self.file_overwrite) and destination_path.is_symlink():
            self.remove_file(destination_path)

        destination_path.symlink_to(source_path)

        self.logger.info('linked file %s -> %s', destination_path, source_path)
        return destination_path

    def copy_file(self, source, destination=None, follow_symlinks=True,
                                             force=False, create_parents=False):
        """copies file (or folder)

        Arguments
        ---------
        source : path-like
            path to copy, dirname defaults to :attr:`path`
        destination : path-like
            new file, defaults to :attr:`path` and `source`'s name
        follow_symlinks : bool
            if true and `source` is a symlink, `destination` will be a
            copy the file `source` refers to, otherwise the symlink is copied
        force : bool
            removes `destination` if it exists
        """

        source_path, destination_path = self.__resolve_endpoint_paths(source,
                                                                    destination)

        if create_parents:
            destination_path.parent.mkdir(parents=True, exist_ok=True)

        # force = force or self.file_overwrite
        # real_dir = \
        #          destination_path.is_dir() and not destination_path.is_symlink()

        # if force and destination_path.exists() and not real_dir:
        #     self.remove_file(destination_path)
        if (force or self.file_overwrite) and destination_path.exists():
            self.remove_file(destination_path, allow_folders=True)

        new_file = shutil.copy(source_path, destination_path,
                                                follow_symlinks=follow_symlinks)

        self.logger.info('copied file %s >> %s', source_path, new_file)
        return Path(new_file)

    def move_file(self, source, destination, create_parents=False):
        """move/rename file (or folder)

        Arguments
        ---------
        source : path-like
            path to move, dirname defaults to :attr:`path`
        destination : path-like
            `source`'s new path, defaults to :attr:`path` and `source`'s name
        """

        source_path, destination_path = self.__resolve_endpoint_paths(source,
                                                                    destination)

        if create_parents:
            destination_path.parent.mkdir(parents=True, exist_ok=True)

        source_path.rename(destination_path)

        self.logger.info('renamed file %s > %s', source_path, destination_path)
        return destination_path

    def remove_file(self, target, allow_folders=False):
        """removes file (or folder)

        Arguments
        ---------
        target : path-like
            path to be removed
        allow_folders : bool
            allows folders to be removed
        """

        target_path = self.__resolve_path(target)

        if allow_folders and target_path.is_dir() and \
                                                   not target_path.is_symlink():
            shutil.rmtree(target_path)
        else:
            target_path.unlink()

        self.logger.info('removed file %s', target_path)
        return target_path

    # TODO: complete
    def create_folder(self, target, force=False, mode=0o775):
        """creates folder

        Arguments
        ---------
        target : path-like
            new folder's path, dirname defaults to :attr:`path`
        force : bool
            removes `target` if it exists
        mode : int
            file mode and access flags
        """

        target_path = self.__resolve_path(target)

        if force and target_path.exists():
            self.remove_file(target_path, allow_folders=True)

        elif target_path.is_dir():
            self.logger.debug("folder %s already exists", target_path)
            return

        target_path.mkdir(mode=mode, parents=True)

        self.logger.info("created folder %s (mode=%o)", target_path, mode)
        return target_path

    def read_text_file(self, target):
        """removes file

        Arguments
        ---------
        """

        path = self.__resolve_path(target)
        self.logger.debug('reading text file %s', path)
        return path.read_text()

    def create_text_file(self, target, text, **tags):
        """creates text file with optional formatting

        Arguments
        ---------
        """

        path = self.__resolve_path(target)
        self.logger.debug('writing text file %s', path)
        path.write_text(text.format_map(tags) if tags else text)
        self.logger.info('new text file %s', path)
        return path

    # def render_template_file(self, target, **render_tags):
    #     target_path = self.__resolve_path(target, self.static_path)
    #     return target_path.read_text().format_map(render_tags)

    create_local_static_link = True
    """(en/dis)able static folder linking in `self.path`"""
    static_files = dict()
    """mapping for static files, must be redefined by user classes"""

    _local_static_link_name = '_static'

    def link_static_file(self, key, base_dir=None, recreate_middle_dirs=False,
                                                                      **kwargs):
        """symlinks a static file from :attr:`static_path`

        Arguments
        ---------
        key : str
            :attr:`static_files` mapping key
        base_dir : str | Path
            base dir path, inserted between the root and the remaning path
        recreate_middle_dirs : bool
            create folders, if any, defined in the static file path
        kwargs
            :meth:`link_file` keyword arguments
        """

        if key not in self.static_files:
            raise KeyError(
                   f'{self} must define static_files mapping with {key!r} key!')

        local_static_link = self.path / self._local_static_link_name

        if self.create_local_static_link and not local_static_link.exists():
            self.static_path = self.link_file(self.static_path,
                                    local_static_link, relative=False, **kwargs)

        target_suffix_path = Path(self.static_files[key])
        source_path = self.static_path / (base_dir or '') / target_suffix_path

        if recreate_middle_dirs:

            middle_path = target_suffix_path.parent
            if not self.path.joinpath(middle_path).is_dir():
                self.create_folder(middle_path)

            return self.link_file(source_path, target_suffix_path, **kwargs)

        return self.link_file(source_path, **kwargs)

    def lookup_hotstart(self, current, interval, period, path_fn):
        restart_datetimes = ( current-delta
                    for delta in utils.timedelta_range(period, delta=interval) )

        for date_time in restart_datetimes:

            restart_path = Path( path_fn(date_time) )
            self.logger.debug('checking restart path from %s at %r',
                                                        date_time, restart_path)

            if restart_path.exists():
                return restart_path

    # def rename_local_file(self, dir_path, file_name, target_name):
    #     return self.rename_file(dir_path/file_name, dir_path/target_name)

    # def local_path_exists(self, name):
    #     file_path = self.path / name
    #     return file_path.exists()

    @classmethod
    def cache_files(cls, *names, check_method=None):
        """method decorator for caching files

        Arguments
        ---------
        names: str | Path
            sequence of file names to be cached
        check_method=None: method
            method to customize checking; accepts a Path argument and returns
            True if the check passes, False otherwise (TODO)

        Note
        ----
        Decorated methods return None if they reuse the cached files, so the
        code that makes use of them must be able to handle that.

        Tip
        ---
        Useful when rerunning stuff, like in testing and maintenance.
        May be used on methods fetching external data (like forcings) or
        performing long/heavy operations.
        """

        def decorator(func):

            if not names:
                raise ValueError(
                               f'cache_files@{func} requires at least one file')

            @wraps(func)
            def wrapper(self, *args, **kwargs):

                # only has effect if cache_path is defined
                if self.cache_path:
                    local_cache_link = self.link_file(self.cache_path, '_cache',
                                                        force=True, check=False)

                    # since cached files exist, those will be used
                    if all( local_cache_link.joinpath(name).exists()
                                                            for name in names ):
                        self.logger.info('%s files (%s) are cached',
                                           func.__qualname__, ', '.join(names) )

                        for name in names :
                            self.link_file(local_cache_link/name,
                                                                 self.path/name)

                        return

                    # otherwise, they'll be cached once produced
                    func_return = func(self, *args, **kwargs)
                    self.logger.debug('%s files will be cached',
                                                              func.__qualname__)

                    for name in names:

                        cached_file_path = self.move_file(name,
                                      self.cache_path/name, create_parents=True)
                        self.link_file(cached_file_path, name)

                    return func_return

            return wrapper

        return decorator
