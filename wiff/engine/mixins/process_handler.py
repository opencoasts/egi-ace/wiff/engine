from pathlib import Path
from subprocess import Popen, SubprocessError, TimeoutExpired, PIPE, STDOUT

from wiff.core.traits import Registry
from wiff.core.utils import QueuedExecution


class ProcessHandler:
    """Provides process execution means to :mod:`wiff.engine.parts`
    classes

    This mixin class implements process execution functions to extend
    :mod:`engine.core.parts` classes and provide them with extra functionality.

    See also
    --------
    engine.core.mixins.file_handler.FileHandler :
        similar mixin class to handle file manipulation

    Example
    -------
    .. code::

        class DoStuff(Stage, ProcessHandler):

            def execute(self):
                self.run_executable_mpi('foo', 'bar1', 'bar2')
    """

    path = Registry.Input(Path.cwd)
    """default working path"""
    bin_path = Registry.Input(Path.cwd)
    """default binary files path"""

    # TODO: rename to mpirun_exec_path
    mpirun_bin = Registry.Input('/lib64/openmpi/bin/mpirun')
    """mpirun executable path"""
    mpirun_args = Registry.Input(tuple)
    """mpirun default arguments"""

    xz_exec_path = Registry.Input('/usr/bin/xz')
    """xz (de)compressing tool binary path"""
    xz_threads = Registry.Input(1)
    """xz compressing thread count"""

    _default_stdout = PIPE
    _default_stderr = STDOUT

    def _popen(self, args, *, cwd=None, stdout=None, stderr=None, **kwargs):
        cwd = cwd or self.path
        args_str = tuple( map(str, args) )
        self.logger.debug("start executing '%s' @ %s", ' '.join(args_str), cwd)

        stdout = stdout or self._default_stdout
        stderr = stderr or self._default_stderr
        return Popen(args_str, cwd=cwd, stdout=stdout, stderr=stderr, **kwargs)

    # TODO: raise exception on error
    def _execute(self, *args, cwd=None, timeout=None, verbose=True,
                                        check=False, sync=True, **popen_kwargs):
        """low-level method to spawn a new process via :class:`subprocess.Popen`

        The high-level execution methods, :meth:`run_executable` and
        :meth:`run_executable_mpi` should be used whenever possible.

        Arguments
        ---------
        args : tuple or list
            executable arguments, including the program
        cwd : path-like
            execution working directory, defaults to :attr:`path` if None
        timeout : float
            limits the execution time, in seconds
        verbose : bool
            logs stdout and stderr
        check : bool
            verifies the return code
        sync : bool
            waits for the process to finish if True or else returns immediately
        popen_kwargs
            :class:`subprocess.Popen` keyword arguments
        
        Return
        ------
        :obj:`subprocess.Popen` instance
            Popen instance object

        See Also
        --------
        run_executable :
            higher-level method for spawning a process
        run_executable_mpi :
            higher-level method for spawning an MPI process
        """
        cwd = cwd or self.path

        args_str = ' '.join( map(str, args) )
        log_msg = lambda status: (f"{status} executing '{args_str}' @ %s", cwd)

        error_code = 0
        execution_error = False

        try:
            popen_obj = self._popen(args, cwd=cwd, **popen_kwargs)

            if not sync:
                return popen_obj

            error_code = popen_obj.wait(timeout)

        except SubprocessError as error:
            execution_error = True
            error_kind = ( 'timeout' if isinstance(error, TimeoutExpired)
                                                                  else 'error' )
            self.logger.exception( *log_msg(f'{error_kind} while') )

        except: # testing
            execution_error = True
            try:
                popen_obj.kill()
                print("\n >>>>>>>> Killed except!!!!!!!\n")
            except NameError:
                pass

            self.logger.exception( *log_msg(f'error while') )

        else:
            if check and error_code:
                execution_error = True
                self.logger.info( *log_msg('returned error') )
            else:
                self.logger.info( *log_msg('finish') )

        if verbose or execution_error:
            try:
                popen_stdout, _ = popen_obj.communicate()

            except NameError:
                pass

            else:
                quoted_stdout = \
                    str(popen_stdout, errors='replace').strip().replace(
                                                                  '\n', '\n \ ')
                self.logger.debug(f"execution output from '{args_str}' @ %s"
                                                 ":\n \ %s", cwd, quoted_stdout)

        return popen_obj

    def __resolve_exec_path(self, exec_name_or_path):
        exec_path = Path(exec_name_or_path)
        if not exec_path.is_absolute():
            exec_path = self.bin_path / exec_path
        return exec_path

    def run_executable(self, target, *args, **kwargs):
        """executes `target` in a new process

        Arguments
        ---------
        target : path-like
            executable path
        args
            executable arguments
        kwargs
            :meth:`_execute` keyword arguments
        """

        exec_path = self.__resolve_exec_path(target)
        return self._execute(exec_path, *args, **kwargs)

    mpi_queue = QueuedExecution('mpirun')
    """:class:`core.utils.QueuedExecution` decorator for MPI-based executions"""

    # TODO: add mpirun_append_args or similar to indicate if mpirun_args should
    # be merge with self.mpirun_args
    @mpi_queue
    def run_executable_mpi(self, target, *args, mpirun_exec_path=None,
                                                      mpirun_args=(), **kwargs):
        """executes `target` MPI in a new process using mpirun

        Arguments
        ---------
        target : path-like
            executable path
        args
            executable arguments
        mpirun_exec_path : path-like
            alternative mpirun executable path
        mpirun_args : tuple or list
            mpirun execution arguments
        kwargs
            :meth:`_execute` keyword arguments

        Notes
        -----
        This function is decorated with :attr:`mpi_queue`.
        """

        mpirun_exec = mpirun_exec_path or self.mpirun_bin
        mpirun_args = mpirun_args or self.mpirun_args
        target_exec = self.__resolve_exec_path(target)

        return self._execute(mpirun_exec, *mpirun_args, target_exec, *args,
                                                                       **kwargs)

    python_bin_map = dict(
        default = '/usr/bin/python',
        sys2 = '/usr/bin/python2',
        sys3 = '/usr/bin/python3',
    )

    def run_script_python(self, script_path, *args, py_bin='default', **kwargs):
        return self.run_executable(self.python_bin_map[py_bin], script_path,
                                                                *args, **kwargs)
