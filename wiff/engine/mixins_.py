from wiff.core import utils


# TODO: decide where to put it, here or in base_parts
class Filiation:
    '''Provides the filiation aspect'''

    def __init__(self, **kwargs):
        self.base_logger.debug( utils.class_mark(__class__) )

        self.__parent = None
        self.__childs = list()

        super().__init__(**kwargs)

    def parent(self):
        return self.__parent

    def childs(self):
        return self.__childs

    def set_parent(self, name, parent):

        if self.parent():
            raise AttributeError(f'{self!r} belongs to {self.parent!r}!')

        self.__parent = parent

        property_getter_fn = type(self).parent
        property_ = property(fget=property_getter_fn)
        setattr(type(self), name, property_)

        self.update_logger_hierarchy()

    def update_logger_hierarchy(self):
        parent_logger = self.parent().logger
        self.logger.debug('updating logger hierarchy, parent=%s',
                                                             parent_logger.name)
        self.set_parent_logger(parent_logger)

    # TODO: should we also add this aspect? would avoid
    # update_logger_hierarchy-like extending situations to deal with childs
    def add_child(self, parent):
        pass

    # lookup through the parent chain
    def lookup_attr(self, name):
        self.logger.debug( utils.class_mark(type(self), mark='lookup_attr') )

        attr = getattr(self, name, None)
        try:
            attr_ = self.parent().lookup_attr(name)
        except AttributeError:
            attr_ = None

        return attr_ or attr

    def lookup_conf(self, name):
        self.logger.debug( utils.class_mark(type(self), mark='lookup_conf') )

        try:
            conf = self.conf.get(name)
        except AttributeError:
            conf = None

        try:
            conf_ = self.parent().lookup_conf(name)
        except AttributeError:
            conf_ = None

        return conf_ or conf


class TimeSeries:
    '''Provides the time series aspect'''
    pass

class Workspace:
    pass

class GitMixin:
    pass
