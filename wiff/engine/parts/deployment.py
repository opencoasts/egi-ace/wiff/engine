from core import utils
from core.entities import BaseEntity

class Deployment(BaseEntity):
    '''Simulation deployment'''
    def __init__(self, identifier, root_path, series, *args, **kwargs):
        self.base_logger.info( utils.class_mark(__class__) )

        super().__init__(identifier, *args, **kwargs)

        self.__root_path = utils.unfold_path(root_path, strict=True)
        self.__series = series

    @property
    def root_path(self):
        return self.__root_path

    @property
    def series(self):
        return self.__series
