import inspect
import json
import pickle
import shutil
from collections import ChainMap
from datetime import datetime, date, time, timedelta
from pathlib import Path
from pprint import pformat

from wiff.core import utils
from .base import Part0
from .stage import Step0
from ..mixins_ import Filiation


class Simulation0(Part0, Filiation):
    '''Simulation unit'''

    __init__ws_dirname__ = '{period_hours}hours-%Y_%m_%dT%H_%M_%S_%f'

    def __init__(self, identifier, stages, period, *args, ws_dirname='',
                        check_stages_deps=True, run_log_settings=None, **kwargs):
        self.base_logger.info( utils.class_mark(__class__) )

        super().__init__(identifier, *args, **kwargs)

        if not isinstance(period, timedelta):
            raise TypeError('period argument must be a timedelta instance'
                                               f', not {type(period).__name__}')
        self.__period = period

        self._set_stages(stages, check_dependencies=check_stages_deps)

        self.check_stages_dependencies = check_stages_deps

        if ws_dirname and not isinstance(ws_dirname, str):
            raise TypeError('ws_dirname argument must be a str or None'
                                           f', not {type(ws_dirname).__name__}')
        ws_dirname_ = ws_dirname or __class__.__init__ws_dirname__
        self._ws_dirname = self.format_workspace_dirname(dirname=ws_dirname_)

        self._run_log_settings = run_log_settings or dict()

        self.logger.debug(self)

    def add_step(self, step, dependencies=None):
        pass

    def _set_stages(self, stages, check_dependencies=True):
        if check_dependencies:
            self._check_stages(stages)

        for step in stages:
            step.set_parent('simulation', self)

        self.__stages = tuple(stages)

    def _check_stages(self, stages):
        included_stages = list()
        for step in stages:
            if not isinstance(step, Step0):
                raise TypeError('stages argument must list only Step instances'
                                          f", can't have {type(step).__name__}")

            if not step.dependencies.issubset(included_stages):
                raise RuntimeError(f"{step!r}'s dependency(ies) not resolved!")

            included_stages.append(step)

    def update_logger_hierarchy(self):
        super().update_logger_hierarchy()

        for step in self.stages:
            step.update_logger_hierarchy()

    def __str__(self):
        return '; '.join( (
            repr(self),
            f'stages: {self.stages}',
            f'period: {self.period}',
            f'ws_dirname: {self._ws_dirname}',
            f'series: {self.parent}',
        ) )

    @property
    def period(self):
        return self.__period

    @property
    def period_hours(self):
        return int( utils.hours_in_(self.period) )

    @property
    def stages(self):
        return self.__stages

    @property
    def run_date_time_current(self):
        return datetime.combine( date.today(), time(0) )

    @property
    def run_path(self):
        return Path.cwd()

    def run(self, *, stages=None, date_time=None, path=None, ws_dirname='',
                         ws_replacing=False, interactive=False, **base_context):

        date_time_ = date_time or self.run_date_time_current
        path_ = path or self.run_path

        execution_datetime = datetime.now()
        workspace_dirname = self.format_workspace_dirname(execution_datetime,
                                                             dirname=ws_dirname)

        try:
            simulation_root_path = \
                        base_context['series_path'] / base_context['series_dir']
        except KeyError:
            simulation_root_path = path_

        workspace_path = path_

        if simulation_root_path == workspace_path:
            workspace_path /= workspace_dirname
            self._create_workspace(workspace_path, replace=ws_replacing)

        log_file = workspace_path.with_suffix('.log')
        self.logger.debug("simulation run log file: %s (settings: %s)",
                                               log_file, self._run_log_settings)
        file_logger = self.new_file_logger(log_file, **self._run_log_settings)

        check_dependencies = False if stages else self.check_stages_dependencies

        context = dict(
            date_time = date_time_,
            path = workspace_path,

            simulation_period = self.period,
            simulation_path = path_,

            check_dependencies = check_dependencies,
        )

        # should the update be performed before executing every step ?
        conflicting_keys = set( context.keys() ).intersection(
                                                           base_context.keys() )
        if conflicting_keys:
            self.logger.warning("context key(s): %s will be overwritten!",
                                                               conflicting_keys)
        context.update(base_context)

        ran_stages = list()
        _stages_failed = list()
        begin_timestamp = datetime.now()

        context.update(
            stages_ran = ran_stages,
            stages_failed = _stages_failed,
            simulation_begin = begin_timestamp,
        )

        stages_to_run = stages or self.stages
        self.logger.debug("simulation stages: %s", stages_to_run)

        self.logger.debug('registry context = %s', context)

        try:
            for step in stages_to_run:

                step_msg_ = lambda state: ( f"{self!r}'s %s {state}!",
                                                                    repr(step) )

                # if step in ran_stages:
                #     self.logger.info( *step_msg_('IGNORED') )
                #     continue

                finished_stages = set(ran_stages).difference(_stages_failed)
                dependencies_resolved = step.dependencies.issubset(
                                                                 finished_stages)

                if check_dependencies and not dependencies_resolved:
                    self.logger.info( *step_msg_('SKIPPED') )
                    continue

                if interactive:
                    user_input = input(
                        "\n"
                        f" >>> Do you want to run {step!r}?\n"
                        " Press [ENTER] to run it"
                        ", type 'n' and press [ENTER] to skip it"
                        " or press [CTRL+C] to abort the whole simulation!\n"
                    )
                    if user_input == 'n':
                        continue

                ran_stages.append(step)
                # context.update(
                #     stages_ran = ran_stages,
                #     stages_failed = _stages_failed,
                # )

                step._inject_from(context=context)
                self.logger.debug('step inputs = %s', step._get_inputs() )

                self.logger.info( *step_msg_('STARTED') )
                try:
                    step.execute()

                except:
                    _stages_failed.append(step)
                    check_dependencies = True
                    self.logger.exception( *step_msg_('FAILED') )

                else:
                    self.logger.info( *step_msg_('FINISHED') )

                step_outputs = step._get_outputs()
                self.logger.debug('step outputs = %s', step_outputs)
                context.update(step_outputs)

        except:
            self.logger.exception('error')

        finally:

            self.logger.info("ran stages: %s", ran_stages)
            self.logger.info("_stages_failed: %s", _stages_failed)

            skipped_stages = set(stages_to_run).difference(ran_stages)
            self.logger.info("skipped stages: %s", skipped_stages)

            end_timestamp = datetime.now()
            execution_time = end_timestamp - begin_timestamp
            self.logger.info("execution duration: %s", execution_time)

            context.update(
                stages_skipped = skipped_stages,
                simulation_end = end_timestamp,
                simulation_duration = execution_time
            )

            self._dump_context(context, workspace_path)

            self.close_file_logger(file_logger)
            self.logger.debug('simulation run log file closed: %s', log_file)

            return context

    # TODO
    def setup(self):
        pass

    # TODO: rework (as workspace mixin?)
    def format_workspace_dirname(self, date_time=None, *, dirname='',
                                              extra_scope_dict=None, safe=True):
        dirname_ = getattr(self, '_ws_dirname', None)

        if dirname:
            scope_ = dict( inspect.getmembers(self) )
            if extra_scope_dict:
                scope_.update(extra_scope_dict)

            dirname_ = dirname.format_map(scope_)

        if date_time:
            dirname_ = date_time.strftime(dirname_)

        if safe:
            dirname_ = ( dirname_.
                            replace(':', '').
                            replace(':', '').
                            replace(' ', '_') )

        return dirname_

    # _create_workspace_dirmode = 0o775 # o=rwx,g=rwx,a=rx

    def _create_workspace(self, path, replace=False, dirmode=0o775):

        replaced = False
        if path.exists():
            if replace:
                self._backup_workspace(path)
                replaced = True
            else:
                return False

        self.logger.info("creating workspace folder '%s' (mode=%o)", path,
                                                                    dirmode)
        path.mkdir(mode=dirmode, parents=True)

        return replaced

    def _backup_workspace(self, path, remove=True):

        backup_path = Path(f'{path}-backup')

        self.logger.info("moving existing workspace '%s' to '%s'!", path,
                                                                    backup_path)

        removed = False
        if remove and backup_path.exists():

            self.logger.debug("removing existing workspace backup '%s'!",
                                                                    backup_path)

            try:
                shutil.rmtree(backup_path)
            except OSError:
                backup_path.unlink()

            removed = True

        path.rename(backup_path)

        return removed

    def _remaining_stages(self, all_stages, unsuccessful_step_ids):

        stages_to_rerun = list(all_stages)

        for step in all_stages:
            if step.id in unsuccessful_step_ids:
                break
            else:
                stages_to_rerun.remove(step)

        return stages_to_rerun

    def _dump_context(self, context, path):

        step_ids_ = lambda stages: tuple( step.id for step in stages )
        safe_item_ = lambda k, v: step_ids_(v) if k.startswith('stages_') else v

        safe_context = { key: safe_item_(key, value)
                                             for key, value in context.items() }

        pickle_path = path.with_suffix('.pickle')
        self.logger.info("dumping pickle context into %r", pickle_path)
        pickle_path.write_bytes( pickle.dumps(safe_context) )

        json_context = json.dumps(safe_context, default=str, indent=4,
                                                                 sort_keys=True)
        self.logger.debug("context:\n%s", json_context)
        json_path = path.with_suffix('.json')
        self.logger.info("dumping json context into %r", json_path)
        json_path.write_text(json_context)


class Simulation1(Part0):
    """Manages the execution of its stages

    The purpose of this class is to spawn and execute its stages and layers
    (from `stage_templates` definitions).
    When testing, `interactive` and `check_stage_dependencies` may be useful,
    but should NOT be used in production systems.
    The former may be used to require user confirmation before starting the
    execution of each step, and the later allows a stage to be included even if
    not all of its inputs are guaranteed to be provided.

    The order of execution follows the `stage_templates` definition.
    All stages are spawned and executed unless an exception is caught.
    Only layers that completed its execution are finalized (in reverse order).

    Example
    -------
    .. code::

        Simulation1('simulation',
            stage_templates = (
                Stage.template('prepare'),
                Stage.template('compute'),
                Stage.template('finalize'),
            ),
            stage_context = (
                period = timedelta(hours=48),
            ),
            interactive = True,
        ).run()
    """

    stage_templates = Part0.Input()
    """sequence of stage templates"""

    stage_context = Part0.Input(dict)
    """common base context for all stages"""

    interactive = Part0.Input(False)
    """requires confirmation before executing each stage"""

    check_stage_dependencies = Part0.Input(True)
    """verify if all stage inputs will be fed"""

    # default_date_time = lambda: datetime.combine( date.today(), time(0) )
    # date_time = Part0.Input(default_date_time) # convinience

    # default_period = timedelta(hours=24)
    # period = Part0.Input(default_period) # convinience

    _series = Part0.Input(optional=True)

    _stages_done = Part0.Output(dict)
    _stages_failed = Part0.Output(dict)

    #default_logger_grouping = True


    def __init__(self, name, **kwargs):
        # self.base_logger.debug( utils.class_mark(__class__) )
        super().__init__(name, **kwargs)

        # TODO: implement dependencies checking
        if self.check_stage_dependencies:
            pass

        self._run_outputs_list = list()
        self._series_outputs = self._try_series_outputs()
        self._series_simulation_context = self._try_series_simulation_context()

    def _try_series_outputs(self):
        try:
            return self._series._get_outputs()
        except AttributeError:
            return dict()

    def _try_series_simulation_context(self):
        try:
            return self._series.simulation_context
        except AttributeError:
            return dict()

    def run(self):
        """spawns and executes all stages, unless an exception is caught"""

        context = ChainMap(
            *reversed(self._run_outputs_list),
            dict(
                _simulation = self,
                _series = self._series,
            ),
            self.stage_context,
            self._series_simulation_context,
            self._get_outputs(),
            self._series_outputs,
        )

        data_items = lambda data_map, indicator: \
                   indicator + pformat(data_map).replace('\n', f'\n{indicator}')
        # data_items = lambda data_map, indicator: '\n'.join( f'{indicator}{item}'
        #                              for item in pformat(data_map).split('\n') )

        try:
            for template in self.stage_templates:

                stage = template.render(context, _logger_root=self.logger)

                logger_args_ = lambda state: ('#> %s %s <#', state, stage)

                self.logger.debug( *logger_args_('STARTING') )
                self.logger.debug('inputs:\n%s',
                                        data_items(stage._get_inputs(), ' > ') )

                if self.interactive:
                    input(f'\n Press ENTER to execute {stage}')

                try:
                    stage.execute()
                except:
                    self.logger.exception( *logger_args_('FAILED') )
                    self._stages_failed[stage.id] = stage
                    break

                self.logger.info( *logger_args_('FINISHED') )
                self.logger.debug('outputs:\n%s',
                                       data_items(stage._get_outputs(), ' < ') )

                self._stages_done[template.name] = stage

                stage_outputs = stage._get_outputs()
                self._run_outputs_list.append(stage_outputs)
                context = context.new_child(stage_outputs)

        finally:
            for layer in reversed( tuple( self._stages_done.values() ) ):
                if hasattr(layer, 'finalize'):

                    if self.interactive:
                        input(f'\n Press ENTER to finalize {layer}')

                    try:
                        layer.finalize()
                    except:
                        self.logger.exception("%s finalize FAILED!", self)

        self.logger.debug('context:\n%s', pformat(context) )
        return context

    def get_state(self):
        return dict(
            name = self.id,
            done_stage_names = (
                *getattr(self, '_previously_done_stage_names', tuple() ),
                *self._stages_done.keys(),
            ),
            run_outputs_list = self._run_outputs_list,
            series_outputs = self._series_outputs,
            series_simulation_context = self._series_simulation_context,
        )

    def set_state(self, content, force=False):
        name = content['name']
        if force and name != self.id:
            raise ValueError('name mismatch!')

        self._previously_done_stage_names = content['done_stage_names']
        self.stage_templates = tuple(
            t for t in self.stage_templates
                if t.kind.transient or
                                 t.name not in self._previously_done_stage_names
        )

        self._run_outputs_list = content['run_outputs_list']
        self._series_outputs = content['series_outputs']
        self._series_simulation_context = content['series_simulation_context']

        return self
