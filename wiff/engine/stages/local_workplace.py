import pickle
from datetime import datetime
from pathlib import Path
from uuid import uuid4

from ..parts import Layer
from ..mixins.file_handler import FileHandler


class LocalWorkplace0(Layer, FileHandler):

    transient = True

    root_path = Layer.Input(Path.cwd, output=True)
    date_time = Layer.Input(optional=True)

    cache_enabled = Layer.Input(True)
    initial_files = Layer.Input(tuple)

    _series = Layer.Input(optional=True)
    _simulation = Layer.Input(optional=True)

    _stages_failed = Layer.Input(optional=True)

    workplace_dir_template = Layer.Input('%Y_%m_%dT%H_%M_%S')
    series_dir_template = Layer.Input('%Y/%m/%d/%H')

    path = Layer.Input(optional=True, output=True)
    latest_run_link = Layer.Input('_latest', output=True)

    # _series_root_path = Layer.Input(optional=True, output=True)
    # _series_dir = Layer.Input(optional=True, output=True)
    # _simulation_root_dir = Layer.Input(optional=True, output=True)

    uuid4 = Layer.Output(uuid4)
    begin_timestamp = Layer.Output(datetime.now)
    simulation_series_path_template = Layer.Output()
    series_root = Layer.Output()
    cache_path = Layer.Output()

    def _series_root_dir(self):
        if self._series:
            return self._series.id
        return ''

    def _series_dir(self):
        if self._series and self.date_time:
            return self.date_time.strftime(self.series_dir_template)
        return ''

    def _simulation_series_root_dir(self):
        if self._simulation:
            return self._simulation.id
        return ''

    def _simulation_dir(self):
        return '{prefix}-{uuid}'.format(
            prefix = self.begin_timestamp.strftime(self.workplace_dir_template),
            uuid = self.uuid4.hex,
        )

    def execute(self):
        if not self.path:
            # base_path = self.root_path

            # if series_root_dir:
            #     # self._series_root_dir = self._series.id
            #     base_path /= series_root_dir

            #     if series_dir:
            #         # self._series_dir = \
            #         #          self.date_time.strftime(self.series_dir_template)
            #         base_path /= series_dir

            # if simulation_series_root_dir:
            #     # self._simulation_series_root_dir = self._simulation.id
            #     base_path /= simulation_series_root_dir

            # # self._simulation_dir = '{prefix}-{uuid}'.format(
            # #     prefix =
            # #          self.begin_timestamp.strftime(self.workplace_dir_template),
            # #     uuid = self.uuid4.hex,
            # # )
            # self.path = base_path / simulation_dir
            simulation_root_path = Path(self.root_path, self._series_root_dir(),
                        self._series_dir(), self._simulation_series_root_dir() )

            if self.cache_enabled:
                self.cache_path = simulation_root_path / '_cache'

            self.path = simulation_root_path / self._simulation_dir()
            self.create_folder(self.path)

        self.logger.info("workspace dir: %s", self.path)

        for file in self.initial_files:
            try:
                target, name = file
            except ValueError:
                target = file
                name = ''

            target_path = Path(target)
            self.link_file(target_path, name,
                             relative=not target_path.is_absolute(), force=True)

        if self._simulation:
            self.log_path = self.path / f'_log.txt'
            self.logger.info("log file: %s", self.log_path)
            self.file_logger = self._simulation.new_file_logger(self.log_path)

            if self._series:
                self.series_root = self.root_path / self._series_root_dir()
                self.simulation_series_path_template = str( self.series_root /
                 self.series_dir_template / self._simulation_series_root_dir() )

    def finalize(self):
        if self._simulation:
            execution_time = datetime.now() - self.begin_timestamp
            self.logger.info("execution time: %s", execution_time)

            state_path = self.path / '_state.pickle'
            state = self._simulation.get_state()

            self.logger.debug("dumping state into %s", state_path)
            state_path.write_bytes( pickle.dumps(state) )

            self._simulation.close_file_logger(self.file_logger)
            self.logger.debug("closing log file %s", self.log_path)

        if not self._stages_failed:
            self._update_latest()

    def _update_latest(self):
        base_path = self.root_path

        series_root_dir = self._series_root_dir()
        series_dir = self._series_dir()

        if series_root_dir and series_dir:
            series_root = base_path / series_root_dir
            series_path = series_root / series_dir
            #series_path = base_path = series_root / series_dir
            latest_simulation_series_link = series_root / self.latest_run_link

            base_path = self.link_file(series_path,
                                      latest_simulation_series_link, force=True)

        simulation_series_root_path = \
                        base_path.resolve() / self._simulation_series_root_dir()

        if simulation_series_root_path.is_dir():
            latest_simulation_link = \
                              simulation_series_root_path / self.latest_run_link

            self.link_file(self.path, latest_simulation_link, force=True)

