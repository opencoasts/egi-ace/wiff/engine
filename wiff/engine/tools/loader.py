import json
from collections.abc import Sequence, Mapping
from datetime import datetime, timedelta
from importlib import import_module
from operator import attrgetter
from pathlib import Path
from pprint import pformat
from string import Template
from typing import Sequence


class JSONTemplateLoader:

    def __init__(self, **context):
        self.context = context

    def parse(self, content:str):
        return self._parse_part(json.loads(content), 'root')

    def parse_from_file(self, path):
        return self.parse( Path(path).read_text() )

    _part_key = '@part'
    _name_key = 'name'

    def _parse_part(self, definition, origin):
        part = self._replace_references(definition.pop(self._part_key))
        name = self._replace_references(definition.pop(self._name_key))

        print(f"> part '{part} # {name}' @ {origin}\n{pformat(definition, depth=1)}")

        part_cls = self._import_attr(part)
        arguments = dict(self._parse_mapping(definition, origin))

        return part_cls.template(name, **arguments)
        
    _context_key_prefix = '#'

    def _replace_references(self, value):
        if value.startswith(self._context_key_prefix):
            return self.context[value[1:]]

        return Template(value).substitute(self.context)

    _attr_separator = ' from '

    def _import_attr(self, definition):
        print(f"> import attr '{definition}'")

        attr_definition, indicator, module_name = \
                                      definition.partition(self._attr_separator)
        if not indicator:
            raise ValueError(f"invalid '{definition}' definition"
                           ", MUST be like '[[...]container.]attr from module'")

        parent = import_module(module_name)

        # container_name, has_container, name = attr_definition.rpartition('.')
        # if has_container:
        #     parent = getattr(parent, container_name)

        # attr = getattr(parent, name)

        attrgetter_from_ = attrgetter(attr_definition)
        attr = attrgetter_from_(parent)

        print(f"> attr '{definition}' = {attr}")
        return attr

    _hidden_key_prefix = '.'

    def _parse_mapping(self, definition, origin):
        for key, raw_value in definition.items():
            if key.startswith(self._hidden_key_prefix):
                continue

            name, kind_parser = self._parse_key(key, origin)

            if kind_parser:
                kind_value = kind_parser(self._parse_value(raw_value, name))
                print(f"> raw value = {raw_value}, kind value = {kind_value}")
                yield name, kind_value

            else:
                yield name, self._parse_value(raw_value, name)

    _key_kind_separator = '|'

    def _parse_key(self, definition, origin):
        name, has_kind, kind = definition.partition(self._key_kind_separator)

        if has_kind:
            kind_parser = getattr(self, f'_parse_kind_{kind}', None)
            if not kind_parser:
                raise ValueError(f"unknown '{kind}' kind in {origin}!")

            return name, kind_parser

        return name, None

    _attr_value_separator = ' from '

    def _parse_kind_attr(self, value):
        return self._import_attr(value)

    def _parse_kind_datetime(self, value):
        if isinstance(value, datetime):
            return value
        return datetime.strptime(value, '%Y-%m-%d %H:%M%S')

    def _parse_kind_days(self, value):
        return timedelta(days=int(value))

    def _parse_kind_hours(self, value):
        return timedelta(hours=int(value))

    def _parse_kind_path(self, value):
        return Path(value)

    def _parse_kind_folder(self, value):
        path = self._parse_kind_path(value)
        if not path.is_dir():
            raise ValueError("path '{path}' is not a folder!")
        return path

    def _parse_kind_file(self, value):
        path = self._parse_kind_path(value)
        if not path.is_file():
            raise ValueError("path '{path}' is not a file!")
        return path

    _reference_prefix = '$'
    #_reference_regex = r'.* [^\$] \$ \{ (\w+) \} .*'

    def _parse_value(self, definition, origin):

        if isinstance(definition, str):
            value = self._replace_references(definition)
            print(f"> string '{origin}:{definition}' value = {value}")
            return value

        elif isinstance(definition, Mapping):
            if self._is_part(definition):
                return self._parse_part(definition, origin)

            print(f"> mapping '{origin}' keys: {', '.join(definition.keys())}")
            return dict(self._parse_mapping(definition, origin))

        elif isinstance(definition, Sequence):
            print(f"> sequence '{origin}' count: {len(definition)}")
            return tuple( self._parse_value(entry, f'{origin}[{index}]')
                                     for index, entry in enumerate(definition) )

        print(f"> value '{origin}' = {definition}")
        return definition

    def _is_part(self, definition):
        return isinstance(definition, Mapping) \
            and self._part_key in definition \
            and self._name_key in definition


def main(json_path):
    from datetime import datetime
    loader = JSONTemplateLoader(
        begin_datetime = datetime(2022, 1, 1),
        end_datetime = datetime(2022, 1, 7),
        simulation_period_hours = "48",
        simulation_interval_hours = 24,
        run_root_path = '/root',
        bin_path = '/bin',
        static_path = '/static',
        albufeira_static_path = '/albufeira',
    )

    series_template = loader.parse_from_file(json_path)
    print(type(series_template))

if __name__ == '__main__':
    from sys import argv
    main(*argv[1:])
