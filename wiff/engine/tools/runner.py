from argparse import ArgumentParser, SUPPRESS
from configparser import ConfigParser, ExtendedInterpolation
from datetime import datetime, timedelta, date, time
from pathlib import Path
from pprint import pformat

from .loader import JSONTemplateLoader
from ..parts import Series, Simulation

import f90nml
from wiff.models.schism.data.param import ParamNml


class _WithCLI:

    def __init__(self, *args, **kwargs):
        parser = self._cli_parser_setup()
        self.__cli_args = self._cli_args_handler(parser)

        kwargs.update(self._cli_args_init())
        super().__init__(*args, **kwargs)

    @property
    def cli_args(self):
        return self.__cli_args

    def _cli_parser_setup(self, **kwargs):
        return ArgumentParser(**kwargs)

    def _cli_args_handler(self, parser):
        return vars(parser.parse_args())

    def _cli_args_init(self):
        return self.cli_args


class _WithConfig:

    def __init__(self, *args, conf_path, **kwargs):
        parser = self._config_parser_setup()
        self.__config = self._config_handler(parser, conf_path)

        kwargs.update(self._config_args_init())
        super().__init__(*args, **kwargs)

    @property
    def config(self):
        return self.__config

    def _config_parser_setup(self, **kwargs):
        return ConfigParser(**kwargs)

    def _config_handler(self, parser, conf_path):
        parser.read(conf_path)
        return dict(parser.items())

    def _config_args_init(self):
        return dict()


class _Base:

    def __init__(self, *, template_path, **initial_context):
        print(template_path, pformat(initial_context))
        self.__tamplate_path = Path(template_path)
        self.__context = self._initial_context_handler(initial_context)
        print(pformat(self.context))

    @property
    def context(self):
        return self.__context

    @context.setter
    def context(self, extra_context):
        self.__context.update(extra_context)

    # def set_context_default(self, key, value):
    #     context_value = self.context.get(key)
    #     if context_value is None:
    #         self.context[key] = value

    def _initial_context_handler(self, context):
        return dict(context)

    class TemplateLoader(JSONTemplateLoader):

        def _parse_kind_pnml(self, value):
            kwargs = dict(value)
            return ParamNml.from_file(kwargs.pop('_file'), **kwargs)

        def _parse_kind_wnml(self, value):
            sections = dict(value)
            namelist = f90nml.read(sections.pop('_file'))
            namelist.patch(sections)
            return namelist

    def load_template(self, **extra_context):
        context = dict(self.context, **extra_context)
        loader = self.TemplateLoader(**context)
        return loader.parse_from_file(self.__tamplate_path)


class SeriesRunner(_WithCLI, _WithConfig, _Base):

    # defaults = dict(
    #     conf_path = Path('runner.conf'),
    #     template_path = Path('template.json'),
    #     initial_context = dict(),
    # )

    default_conf_name = 'runner.conf'
    default_template_name = 'template.json'

    cli_parser_init_kwargs = dict(
        description = 'WIFF engine runner',
    )

    default_date_time_fmt = '%Y-%m-%d_%H'

    def __init__(self, conf_path=None, template_path=None, initial_context={}):
        super().__init__(**initial_context)

    def _cli_parser_setup(self, **kwargs):
        kwargs.update(self.cli_parser_init_kwargs)
        parser = super()._cli_parser_setup(**kwargs)

        default_dt_fmt = self.default_date_time_fmt.replace('%', '')

        parser.add_argument('-v', '--verbose',
            action='store_true', help='increases output verbosity level')
    
        parser.add_argument('-c', '--config', metavar='PATH',
            default=self.default_conf_name, type=Path, dest='conf_path',
            help=f'configuration file path (default: {self.default_conf_name})')
        parser.add_argument('-t', '--template', metavar='PATH',
            default=self.default_template_name, type=Path, dest='template_path',
            help=f'template file path (default: {self.default_template_name})')

        begin_megroup = parser.add_mutually_exclusive_group()

        begin_megroup.add_argument('-b', '--begin', metavar='DATE_TIME',
            dest='begin__datetime', 
            help=f'series begin date/time (default format: {default_dt_fmt})')
        begin_megroup.add_argument('-o', '--offset', metavar='DAYS',
            default=0, dest='begin_offset__days',
            help='offset relative to begin (specified in days)')

        end_megroup = parser.add_mutually_exclusive_group()

        end_megroup.add_argument('-e', '--end', metavar='DATE_TIME',
            dest='end__datetime', 
            help=f'series end date/time (default format: {default_dt_fmt})')
        end_megroup.add_argument('-d', '--duration', metavar='DAYS',
            default=0, dest='duration__days',
            help='duration relative to begin (specified in days)')

        return parser

    def _config_parser_setup(self, **kwargs):
        kwargs['interpolation'] = ExtendedInterpolation()
        return super()._config_parser_setup(**kwargs)

    _config_default_sections = ('context', 'logging', 'formatting')

    def _config_handler(self, parser, conf_path):
        config = super()._config_handler(parser, conf_path)
        return { section: config.get(section) or dict()
                                 for section in self._config_default_sections }

    def _config_args_init(self):
        return self.config.get('context') or dict()

    def _initial_context_handler(self, context):
        date_time_fmt = self.config['formatting'].get('date_time') or \
                                                      self.default_date_time_fmt

        def converted_args():
            for name, value in context.items():
                new_name, needs_conversion, kind = name.rpartition('__')

                if needs_conversion:
                    name = new_name

                    if value is not None:

                        if kind == 'path':
                            value = Path(value)

                        elif kind == 'datetime':
                            value = datetime.strptime(value, date_time_fmt)

                        elif kind in ('hours', 'days'):
                            value = timedelta(**{kind: float(value)})

                yield name, value

        return dict(converted_args())

    def execute_system(self):
        # begin is today's midnight by default
        begin = self.context['begin'] or datetime.combine(date.today(), time())
        begin += self.context.pop('begin_offset')

        end = self.context['end'] or begin + self.context.pop('duration')

        self.load_template(begin=begin, end=end).render().simulate()


class CLI:

    parser_class = ArgumentParser

    def __init__(self, **kwargs):
        self.parser = self.parser_class(**kwargs)

    def __get__(self, obj, owner):
        print(f'{type(self).__qualname__}.__get__ self={self} obj={obj} owner={owner.__qualname__}')
        if not obj:
            return self
        return self.parser

    # def __set__(self, obj, value):
    #     print(f'{type(self).__qualname__}.__set__ self={self} obj={obj} value={value}')
    #     self.

    def with_argument(self, *args, **kwargs):
        self.parser.add_argument(*args, **kwargs)
        return self


class Config:

    parser_class = ConfigParser

    def __init__(self, **kwargs):
        self.__parser = self.parser_class(**kwargs)

    def load_sections(self, config, *names):
        self.parser.read_string(config)
        self.__sections = dict(self._parse_sections(names))

    def _parse_sections(self, names):
        sections = dict(self.parser.items())
        for name in names:
            yield name, self._parse_section(sections.get(name))

    def _parse_section(self, items):
        pass

    @property
    def parser(self):
        return self.__parser

    @property
    def sections(self):
        return self.__sections


class SeriesLauncher(_Base):

    default_conf_path = Path('runner.conf')
    default_template_path = Path('template.json')

    defaults = dict(
        conf_path = Path('runner.conf'),
        template_path = Path('template.json'),
        initial_context = dict(),
    )

    cli = CLI(
        description = 'WIFF engine runner',
    ).with_argument(
        '-c', '--config', default=SUPPRESS, dest='config_path', type=Path
    ).with_argument(
        '-t', '--template', default=SUPPRESS, dest='template_path', type=Path
    )

    def __init__(self, **initial_context):
        self._cli_setup().parse_args()

        super().__init__(
            template_path = self.default_template_path,
            initial_context = initial_context,
        )

    def _cli_setup(self):
        return CLI(
            description = 'WIFF engine runner',
        ).with_argument(
            '-c', '--config', default=SUPPRESS, dest='config_path', type=Path
        ).with_argument(
            '-t', '--template', default=SUPPRESS, dest='template_path', type=Path
        )

    def execute_system(self):
        pass


# class SeriesRunnerXP(SeriesRunner):

#     SeriesRunner.cli


if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.DEBUG)

    SeriesRunner().execute_system()
