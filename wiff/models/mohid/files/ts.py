import logging
from datetime import datetime, timedelta
from enum import Enum
from pathlib import Path
from typing import Generator, Iterable, Mapping, Sequence, Union

_logger = logging.getLogger(__name__)


class TimeSeries:

    logger = _logger.getChild(__qualname__)

    class TimeUnit(Enum):
        SECONDS = timedelta(seconds=1)
        MINUTES = timedelta(minutes=1)
        HOURS = timedelta(hours=1)
        DAYS = timedelta(days=1)

    def __init__(self, initial_data:datetime, time_unit:TimeUnit,
            columns:Sequence[str], series:Union[Sequence,None]=None):
        self.initial_data = initial_data
        self.time_unit = time_unit
        self.columns = tuple(columns)
        self.set_series(series or list())

    def get_series(self, *columns:str) -> Generator[Sequence, None, None]:
        if columns and not self.columns:
            raise ValueError()

        indexes = tuple( map(self.columns.index, columns) )
        values_in_ = lambda data: \
            tuple( map(data.__getitem__, indexes) if columns else data )

        for step, data in self.series:
            yield self.initial_data+step, values_in_(data)

    def set_series(self, series:Sequence):
        self.series = tuple( (step, tuple(values)) for step, values in series )
        return self

    @classmethod
    def from_file(cls, path:Path):
        '''creates new instance from the file defined in `path`'''
        def file_lines():
            with Path(path).open() as file:
                for line in file:
                    yield line
        return cls.from_lines(file_lines())

    @classmethod
    def from_lines(cls, lines:Union[Sequence[str],Iterable[str]]):
        return cls( **cls._parse_lines(iter(lines)) )

    @classmethod
    def _parse_lines(cls, lines:Iterable[str]) -> Mapping:
        '''parses every line in `self.lines`'''
        init_kwargs = dict()

        for line in lines:
            data = cls._parse_data(line)
            if not data:
                continue

            keyword_part, keyword_separator, value_part = data.partition(':')
            keyword = keyword_part.strip().upper()

            if keyword_separator:
                value = value_part.strip()
                cls.logger.debug("keyword=%s value=%s", keyword, value)

                if keyword == 'SERIE_INITIAL_DATA':
                    init_kwargs['initial_data'] = \
                                           cls._parse_series_initial_data(value)

                elif keyword == 'TIME_UNITS':
                    init_kwargs['time_unit'] = cls.TimeUnit[value]

                # elif keyword == 'TIME_CYCLE':
                #     pass

                else:
                    cls.logger.debug("unknown keyword '%s'!", keyword)

            elif data == '<BeginTimeSerie>':
                cls.logger.debug("series")
                init_kwargs['series'] = tuple(
                    cls._parse_series_lines(lines, init_kwargs['time_unit'],
                                                               '<EndTimeSerie>')
                )

            elif data == '<BeginResidual>':
                cls.logger.debug("residual")
                tuple(cls._parse_series_lines(lines, init_kwargs['time_unit'],
                                                               '<EndResidual>'))

            elif 'initial_data' in init_kwargs and 'series' not in init_kwargs:
                cls.logger.debug("columns=%s", data)
                init_kwargs['columns'] = data.split()[1:]

            else:
                cls.logger.debug(f"title=%s", data)

        return init_kwargs

    @classmethod
    def _parse_data(cls, line:str) -> str:
        data, indicator, comment = line.partition('!')
        return data.strip()

    @classmethod
    def _parse_series_initial_data(cls, value:str) -> datetime:
        def split_decimal_(str_number:str):
            whole_part, dot, decimal_part = str_number.partition('.')
            return int(whole_part), int(decimal_part) if decimal_part else 0
        whole_part_of_ = lambda str_number: split_decimal_(str_number)[0]

        *date_time_parts, seconds_part = value.split()
        year, month, day, hour, minute = map(whole_part_of_, date_time_parts)
        second, microsecond = split_decimal_(seconds_part)

        return datetime(year, month, day, hour, minute, second, microsecond)

    @classmethod
    def _parse_series_lines(cls, lines:Iterable[str], time_unit:TimeUnit,
                                                    break_tag:str) -> Generator:
        for line in lines:
            data = cls._parse_data(line)
            if data == break_tag:
                break

            step, *values = data.split()
            yield float(step)*time_unit.value, map(float, values)

    def __str__(self) -> str:
        return f"Time series: {self.initial_data} ({self.time_unit})\n" \
            f"  columns: {', '.join(self.columns)}"

    def __repr__(self) -> str:
        return f"{self}\n" \
            f"  series: {'|'.join( str(step) for step, _ in self.series )}"


if __name__ == '__main__':
    logging.basicConfig(level='INFO')

    from sys import argv
    file_path, = argv[1:]

    mohid_ts = TimeSeries.from_file(file_path)
    _logger.info("%r", mohid_ts)
    _logger.info("%s", tuple(mohid_ts.get_series('YY')) )
