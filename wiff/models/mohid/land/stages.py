from datetime import timedelta
from pathlib import Path

from wiff.engine.parts import Stage
from wiff.engine.mixins.file_handler import FileHandler
from wiff.engine.mixins.process_handler import ProcessHandler


class MOHIDStage0(Stage, FileHandler):

    date_time = Stage.Input()
    period = Stage.Input()

    _confs_folder = 'confs'

    _inputs_folder = 'inputs'
    _meteo_folder = f'{_inputs_folder}/boundary_conditions/meteo'
    
    _outputs_folder = 'outputs'
    _output_state_folder = f'{_outputs_folder}/state'
    _latest_run_link = '_latest'


class Prepare0(MOHIDStage0):

    force_coldstart = MOHIDStage0.Input(False)

    series_interval = MOHIDStage0.Input(optional=True)
    simulation_series_path_template = MOHIDStage0.Input(optional=True)

    def execute(self):
        self._make_folders()
        self._link_static()
        is_continuous = self._resolve_hotstart()
        self._render_templates(is_continuous)

    _input_state_folder = f'{MOHIDStage0._inputs_folder}/state'

    _folders_to_create = (
        MOHIDStage0._confs_folder,
        MOHIDStage0._meteo_folder,
    )

    def _make_folders(self):
        for name in self._folders_to_create:
            self.create_folder(name)

    static_files = dict(
        atmosphere = f'{MOHIDStage0._confs_folder}/atmosphere.dat',
        basin = f'{MOHIDStage0._confs_folder}/basin.dat.template',
        basin_geometry = f'{MOHIDStage0._confs_folder}/basin_geometry.dat',
        drainage_network =
                   f'{MOHIDStage0._confs_folder}/drainage_network.dat.template',
        geometry = f'{MOHIDStage0._confs_folder}/geometry.dat',
        model = f'{MOHIDStage0._confs_folder}/model.dat.template',
        main = f'{MOHIDStage0._confs_folder}/nomfich.dat',
        porous_media = f'{MOHIDStage0._confs_folder}/porous_media.dat.template',
        run_off = f'{MOHIDStage0._confs_folder}/run_off.dat.template',
        timeseries = f'{MOHIDStage0._confs_folder}/timeseries_nodes.dat',
        vegetation = f'{MOHIDStage0._confs_folder}/vegetation.dat.template',

        digital_terrain = f'{MOHIDStage0._inputs_folder}/digital_terrain',
        initial_conditions = f'{MOHIDStage0._inputs_folder}/initial_conditions',
        other = f'{MOHIDStage0._inputs_folder}/other'
    )

    def _link_static(self):
        for name in self.static_files.keys():
            self.link_static_file(name, recreate_middle_dirs=True)

    # _hotstart_files = (
    #     'basin',
    #     'drainage_network',
    #     'porous_media',
    #     'run_off',
    #     'vegetation',
    # )

    def _resolve_hotstart(self):
        coldstart_msg = "COLDSTARTED!!! [reason: %s]"

        if self.force_coldstart:
            self.logger.warning(coldstart_msg, 'forced')
            return False

        if not (self.series_interval and self.simulation_series_path_template):
            self.logger.warning(coldstart_msg, 'no series')
            return False

        path_fn = lambda date_time: Path(
            date_time.strftime(self.simulation_series_path_template),
            self._latest_run_link,
            self._output_state_folder,
        )

        file_path = self.lookup_hotstart(self.date_time,
                                     self.series_interval, self.period, path_fn)

        if not file_path:
            self.logger.warning(coldstart_msg, 'no hotstart available')
            return False

        self.link_file(file_path, self._input_state_folder)
        self.logger.info("HOTSTARTED!!! [%s]", file_path)
        return True

    def _render_templates(self, continuous=False):

        def render_template(static_name, **tags):
            path = Path(self.static_files[static_name])
            text = self.read_text_file(path)
            self.create_text_file(path.with_suffix(''), text, **tags)

        render_template('model',
            START = self.date_time,
            END = self.date_time + self.period,
        )

        for name in ('basin', 'drainage_network', 'porous_media', 'run_off',
                                                                  'vegetation'):
            render_template(name,
                CONTINUOUS = int(continuous)
            )


from .. import meteo


class Forcings0(MOHIDStage0):

    bin_path = MOHIDStage0.Input()

    maretec_dataset = MOHIDStage0.Input()
    maretec_user = MOHIDStage0.Input()
    maretec_password = MOHIDStage0.Input()

    # _hdf5_time_field = 'time'
    # _hdf5_grid_fields = dict(
    #     lat = 'lat',
    #     long = 'lon'
    # )
    # _hdf5_fields = dict(
    #     rain = dict(
    #         name = 'precipitation',
    #         units = 'mm',
    #         description = 'WRF precipitation',
    #         dim = 2,
    #     ),
    #     temp2 = dict(
    #         name = 'air temperature',
    #         units = 'C',
    #         description = 'WRF temperature',
    #         dim = 2,
    #     ),
    #     rh2 = dict(
    #         name = 'relative humidity',
    #         units = 'fraction',
    #         description = 'WRF relative humidity',
    #         dim = 2,
    #     ),
    #     lwdown = dict(
    #         name = 'solar radiation',
    #         units = 'W/m2',
    #         description = 'WRF downward long wave flux',
    #         dim = 2,
    #     ),
    #     u10 = dict(
    #         name = 'wind velocity X',
    #         units = 'm/s',
    #         description = 'WRF x-component of wind',
    #         dim = 2,
    #     ),
    #     v10 = dict(
    #         name = 'wind velocity Y',
    #         units = 'm/s',
    #         description = 'RF y-component of wind',
    #         dim = 2,
    #     ),
    #     ws10 = dict(
    #         name = 'wind modulus',
    #         units = 'm/s',
    #         description = 'WRF wind modulus',
    #         dim = 2,
    #     ),
    # )

    #@MOHIDStage0.cache_files(f'{MOHIDStage0._meteo_folder}/wrf.hdf5')
    def execute(self):
        atmos = meteo.MARETEC(
            self.maretec_dataset,

            date_time = self.date_time,
            period = self.period,
            extent = None,

            user = self.maretec_user,
            password = self.maretec_password,

            work_path = self.path,
            templates_dir = self.static_path,
            tools_dir = self.bin_path,

            logger_root = self.logger,
        )
        try:
            atmos.create_file()
        finally:
            atmos.close()


class Compute0(MOHIDStage0, ProcessHandler):

    mohid_land_bin = MOHIDStage0.Input('MohidLand')
    config_file = MOHIDStage0.Input(f'{MOHIDStage0._confs_folder}/nomfich.dat')

    def execute(self):
        self._run_mohid()

    #@MOHIDStage0.cache_files(MOHIDStage0._outputs_folder)
    def _run_mohid(self):
        self.create_folder(self._output_state_folder)
        self.create_folder(f'{self._outputs_folder}/run')

        self.run_executable('/bin/bash', f'{self.bin_path}/shim.sh',
            f'{self.bin_path}/{self.mohid_land_bin}',
            '--config', f'{self.config_file}',
        )


from math import ceil


class Outputs0(MOHIDStage0, ProcessHandler):

    bin_path = MOHIDStage0.Input()
    outputs_series_period = MOHIDStage0.Input(timedelta(days=5))

    root_path = MOHIDStage0.Input(optional=True)
    root_path_ncml_replacement = MOHIDStage0.Input(optional=True)
    simulation_series_path_template = MOHIDStage0.Input(optional=True)

    def execute(self):
        self._convert_outputs()
        self._build_outputs_series()

    _convert_to_netcdf_bin = 'Convert2netcdf'
    _convert_outputs_template = 'convert_outputs.dat.template'

    _outputs_to_convert = {
        'porous_media': (
            'Results/relative water content',
        ),
        'run_off': (
            'Results/velocity modulus',
            'Results/water column',
        )
    }

    def _convert_outputs(self):
        template = self.read_text_file(
                                self.static_path/self._convert_outputs_template)
        config_path = self.path / f'{self._convert_to_netcdf_bin}.dat'

        for kind, groups in self._outputs_to_convert.items():
            kind_config_path = self.create_text_file(f'{config_path}.{kind}',
                template,
                KIND = kind,
                YEAR = self.date_time.year,
                REFERENCE_DATE = self.date_time,
                GROUPS = '\n    '.join(groups),
            )

            print(kind_config_path, config_path)
            self.link_file(kind_config_path, config_path, force=True)
            self.run_executable('/bin/bash', f'{self.bin_path}/shim.sh',
                f'{self.bin_path}/{self._convert_to_netcdf_bin}'
            )

    # TODO: move ncml specific stuff for its own class and make it reusable
    _series_ncml_aggregation_lines = (
        '<netcdf xmlns="http://www.unidata.ucar.edu/namespaces/netcdf/ncml-2.2">',
        '  <variable name="time">',
        '    <attribute name="units" value="seconds since {DATETIME:%Y-%m-%d %H:%M:%S}"/>',
        '    <values start="3600" increment="3600" />',
        '  </variable>',
        '',
        '  <aggregation dimName="time" type="joinExisting">',
        '    {NETCDFS}',
        '  </aggregation>',
        '</netcdf>',
    )
    _series_ncml_aggregation_netcdf = \
        '<netcdf location="file:{}" ncoords="24"/>'

    def _build_outputs_series(self):
        if not self.simulation_series_path_template:
            return

        series_output_path_for_ = lambda run_path, name: \
                                  run_path / self._outputs_folder / f'{name}.nc'
        ncml_output_strpath = lambda run_path: (
            str(run_path).replace(str(self.root_path),
                                           str(self.root_path_ncml_replacement))
                if self.root_path and self.root_path_ncml_replacement
                                                              else str(run_path)
        )

        series_size = ceil(self.outputs_series_period / self.period)

        # NOTE: this generator changes reference_datetime, but the effects only
        # apply after it is rendered
        def netcdfs_xml_elements(output_kind):
            run_output_path = series_output_path_for_(self.path, output_kind)
            yield self.date_time, self._series_ncml_aggregation_netcdf.format(
                                           ncml_output_strpath(run_output_path))

            for index in range(1, series_size):
                run_date_time = self.date_time - (index * self.period)
                run_path = Path(run_date_time.strftime(
                              self.simulation_series_path_template)) / '_latest'

                run_output_path = series_output_path_for_(run_path, output_kind)
                if run_output_path.is_file():
                    yield run_date_time, \
                        self._series_ncml_aggregation_netcdf.format(
                                           ncml_output_strpath(run_output_path))
                else:
                    break

        series_path = self.create_folder('_series')

        for output_kind in self._outputs_to_convert.keys():
            datetimes, netcdfs = zip(*netcdfs_xml_elements(output_kind))
            self.create_text_file(series_path / f'{output_kind}.ncml',
                '\n'.join(self._series_ncml_aggregation_lines),
                DATETIME = min(datetimes),
                NETCDFS = '\n    '.join(reversed(netcdfs))
            )
