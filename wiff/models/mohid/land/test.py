from datetime import datetime, date, time, timedelta
from pathlib import Path

from wiff.engine.parts import Simulation, Series
from wiff.engine.stages.local_workplace import LocalWorkplace0

from .stages import Prepare0, Forcings0, Compute0

def main(run_date, dist_root, run_root):

    Series('series',
        begin = run_date,
        interval = timedelta(hours=24),
        simulation_context = dict(),

        simulation_templates = (
            Simulation.template('simulation',
                stage_context = dict(
                    bin_path = Path(dist_root, 'bin/mohid/20.10'),
                    static_path = Path(dist_root, 'static/mohid'),
                    period = timedelta(hours=24),
                ),

                stage_templates = (
                    LocalWorkplace0.template('workplace',
                        root_path = Path(run_root, 'test'),
                        # initial_files = (
                        # ),
                    ),
                    Prepare0.template('prepare',
                        static_path =
                                 Path(dist_root, 'static/mohid/land/albufeira'),
                    ),
                    Forcings0.template('forcings',
                        maretec_dataset = 'sinergea_fc_albufeira',
                        maretec_user = 'sinergea',
                        maretec_password = 'T6p7xBPxgwG2u88V',
                    ),
                    Compute0.template('compute',
                    ),
                )
            ),
        )
    ).simulate()

if __name__ == '__main__':
    import sys
    import logging

    logging.basicConfig(level=logging.DEBUG)

    dist_root, run_root, offset = sys.argv[1:]
    run_date = \
        datetime.combine(date.today(), time(0) ) - timedelta( days=int(offset) )

    main(run_date, dist_root, run_root)
