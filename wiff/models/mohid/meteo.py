from abc import abstractmethod
from pathlib import Path
from subprocess import run, PIPE, STDOUT, CalledProcessError

from wiff.forcings.targets import Target


class Meteo0(Target):

    def __init__(self, *args, templates_dir='.', tools_dir='.', **kwargs):
        self.templates_dir = Path(templates_dir)
        self.tools_dir = Path(tools_dir)
        super().__init__(*args, **kwargs)

    # TODO: duplicated code, it should be somewhere central
    def _run_bin(self, bin_path, *args, **kwargs):
        run_kwargs = dict(check=True, stdout=PIPE, stderr=STDOUT,
                                              encoding='utf8', errors='replace')
        run_kwargs.update(kwargs)

        args_str = tuple( map(str, args) )
        self.logger.info('%s args=%s kwargs=%s', bin_path, args_str, run_kwargs)

        run_args = ('/bin/bash', f'{self.tools_dir}/shim.sh',
                                                       str(bin_path), *args_str)

        try:
            run_exec = run(run_args, **run_kwargs)
        except CalledProcessError as error:
            self.logger.warning('%s stdout:\n%s', bin_path, error.stdout)
        else:
            self.logger.debug('%s stdout:\n%s', bin_path, run_exec.stdout)
            return run_exec

    @abstractmethod
    def _meteo_netcdf_path(self):
        pass

    @abstractmethod
    def _meteo_date_time(self):
        pass

    @abstractmethod
    def _meteo_period(self):
        pass

    @abstractmethod
    def _meteo_work_path(self):
        pass

    _meteo_convert_to_hdf5_tool = 'ConvertToHDF5'

    def _meteo_convert_to_hdf5(self, config_name, template_name, **tags):
        template_path = self.templates_dir / template_name
        config = template_path.read_text().format_map(tags)

        config_path = self._meteo_work_path() / config_name
        config_path.write_text(config)

        self._run_bin(self.tools_dir/self._meteo_convert_to_hdf5_tool,
                          '--config', config_name, cwd=self._meteo_work_path() )

    _meteo_convert_config = 'convert_meteo.dat'
    _meteo_convert_config_template = f'{_meteo_convert_config}.template'

    def _meteo_convert(self):
        self._meteo_convert_to_hdf5(self._meteo_convert_config,
            self._meteo_convert_config_template,

            INPUT_FILES = self._meteo_netcdf_path()
        )

    _meteo_interpolate_config = 'interpolate_meteo.dat'
    _meteo_interpolate_config_template = f'{_meteo_interpolate_config}.template'

    def _meteo_interpolate(self):
        self._meteo_convert_to_hdf5(self._meteo_interpolate_config,
            self._meteo_interpolate_config_template,

            START = self._meteo_date_time(),
            END = self._meteo_date_time() + self._meteo_period(),
        )

    def create_file(self):
        self._meteo_convert()
        self._meteo_interpolate()


from wiff.forcings.providers.tecnico.maretec import Base


class MARETEC(Base, Meteo0):

    def _meteo_netcdf_path(self):
        return Path(self._get_netcdf().filepath())

    def _meteo_date_time(self):
        return self.date_time

    def _meteo_period(self):
        return self.period

    def _meteo_work_path(self):
        return self.work_path
