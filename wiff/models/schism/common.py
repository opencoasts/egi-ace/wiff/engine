from enum import Enum, IntEnum
from itertools import chain


class CommonName:

    PATH = 'path'
    DATE_TIME = 'date_time'
    SIMULATION_KIND = 'simulation_kind'

    FORCINGS = 'forcings'

    class ForcingKind:

        OCEAN_ELEV = 'ocean_elev'
        OCEAN_SALT_TEMP = 'ocean_salt_temp'
        ATMOSPHERIC = 'atmospheric'

cn = CommonName


class BoundaryKind:

    OCEAN = 'ocean'
    RIVER = 'river'
    ATMOSPHERIC = 'atmospheric'


# consider define new class ForcingProvider with SOURCE_MAP
class ForcingKind:

    CIRCULATION = 'Circulation'
    ELEVATION = 'Elevation'
    VELOCITY = 'Velocity'
    WAVES = 'Waves'
    FLOW = 'Flow'

    TEMPERATURE = 'Temperature'
    SALINITY = 'Salinity'

    OCEAN_TEMPERATURE = 'Temperature'
    OCEAN_SALINITY = 'Salinity'
    RIVER_TEMPERATURE = 'Temperature'
    RIVER_SALINITY = 'Salinity'

    E_COLI = 'Escherichia Coli'
    ENTEROCOCCUS = 'Enterococcus'

    AIR = 'Air'
    OPTIONAL_AIR = 'Optional Air' # makes sense to exist?
    RADIATION = 'Radiation'


class OceanForcingKind:

    CIRCULATION = 'Circulation'
    ELEVATION = 'Elevation'
    VELOCITY = 'Velocity'

    TEMPERATURE = 'Temperature'
    SALINITY = 'Salinity'

    WAVES = 'Waves'

    E_COLI = 'Escherichia Coli'
    ENTEROCOCCUS = 'Enterococcus'


class RiverForcingKind:

    FLOW = 'Flow'

    TEMPERATURE = 'Temperature'
    SALINITY = 'Salinity'

    E_COLI = 'Escherichia Coli'
    ENTEROCOCCUS = 'Enterococcus'


class AtmosphericForcingKind:

    AIR = 'Air'
    OPTIONAL_AIR = 'Optional Air' # makes sense to exist?
    RADIATION = 'Radiation'


class ConfigurationKind:

    PARAM_IN = 'param_in'
    PARAM_NML = 'param_nml'
    WWMINPUT_NML = 'wwminput_nml'


class SimulationKind:

    BAROTROPIC = 'barotropic'
    BAROTROPIC_WAVES = 'barotropic_waves'
    BAROTROPIC_MICROBIOLOGY = 'barotropic_microbiology'
    BAROTROPIC_WAVES_MICROBIOLOGY = 'barotropic_waves_microbiology'

    BAROCLINIC = 'baroclinic'
    BAROCLINIC_WAVES = 'baroclinic_waves'
    BAROCLINIC_MICROBIOLOGY = 'baroclinic_microbiology'
    BAROCLINIC_WAVES_MICROBIOLOGY = 'baroclinic_waves_microbiology'

    _bk = BoundaryKind
    _fk = ForcingKind

    FORCINGS_MAP = {
        BAROTROPIC: {
            _bk.OCEAN: (_fk.ELEVATION,),
            _bk.RIVER: (_fk.FLOW,),
            _bk.ATMOSPHERIC: (_fk.OPTIONAL_AIR,),
        },
        BAROTROPIC_WAVES: {
            _bk.OCEAN: (_fk.ELEVATION, _fk.WAVES),
            _bk.RIVER: (_fk.FLOW,),
            _bk.ATMOSPHERIC: (_fk.OPTIONAL_AIR,),
        },
        BAROTROPIC_MICROBIOLOGY: {
            _bk.OCEAN: (_fk.ELEVATION, _fk.E_COLI, _fk.ENTEROCOCCUS),
            _bk.RIVER: (_fk.FLOW, _fk.E_COLI, _fk.ENTEROCOCCUS),
            _bk.ATMOSPHERIC: (_fk.OPTIONAL_AIR,),
        },
        BAROTROPIC_WAVES_MICROBIOLOGY: {
            _bk.OCEAN: (_fk.ELEVATION, _fk.WAVES, _fk.E_COLI, _fk.ENTEROCOCCUS),
            _bk.RIVER: (_fk.FLOW, _fk.E_COLI, _fk.ENTEROCOCCUS),
            _bk.ATMOSPHERIC: (_fk.OPTIONAL_AIR,),
        },

        BAROCLINIC: {
            _bk.OCEAN: (_fk.ELEVATION, _fk.TEMPERATURE, _fk.SALINITY),
            _bk.RIVER: (_fk.FLOW, _fk.TEMPERATURE, _fk.SALINITY), # reversed
            _bk.ATMOSPHERIC: (_fk.AIR, _fk.RADIATION),
        },
        BAROCLINIC_WAVES: {
            _bk.OCEAN: (_fk.ELEVATION, _fk.TEMPERATURE, _fk.SALINITY,
                                                                     _fk.WAVES),
            _bk.RIVER: (_fk.FLOW, _fk.TEMPERATURE, _fk.SALINITY),
            _bk.ATMOSPHERIC: (_fk.AIR, _fk.RADIATION),
        },
        BAROCLINIC_MICROBIOLOGY: {
            _bk.OCEAN: (_fk.ELEVATION, _fk.TEMPERATURE, _fk.SALINITY,
                                                  _fk.E_COLI, _fk.ENTEROCOCCUS),
            _bk.RIVER: (_fk.FLOW, _fk.TEMPERATURE, _fk.SALINITY, _fk.E_COLI,
                                                              _fk.ENTEROCOCCUS),
            _bk.ATMOSPHERIC: (_fk.AIR, _fk.RADIATION),
        },
        BAROCLINIC_WAVES_MICROBIOLOGY: {
            _bk.OCEAN: (_fk.ELEVATION, _fk.TEMPERATURE, _fk.SALINITY, _fk.WAVES,
                                                  _fk.E_COLI, _fk.ENTEROCOCCUS),
            _bk.RIVER: (_fk.FLOW, _fk.TEMPERATURE, _fk.SALINITY, _fk.E_COLI,
                                                              _fk.ENTEROCOCCUS),
            _bk.ATMOSPHERIC: (_fk.AIR, _fk.RADIATION),
        },
    }

    _ck = ConfigurationKind

    CONFIGURATION_MAP = {
        BAROTROPIC: (_ck.PARAM_IN,),
        BAROTROPIC_WAVES: (_ck.PARAM_IN, _ck.WWMINPUT_NML),
        BAROCLINIC: (_ck.PARAM_IN,),
        BAROCLINIC_WAVES: (_ck.PARAM_IN, _ck.WWMINPUT_NML),
        BAROCLINIC_MICROBIOLOGY: (_ck.PARAM_IN,),
        BAROCLINIC_WAVES_MICROBIOLOGY: (_ck.PARAM_IN, _ck.WWMINPUT_NML),
    }

    BAROCLINIC_KINDS = (
        BAROCLINIC,
        BAROCLINIC_WAVES,
        BAROCLINIC_MICROBIOLOGY,
        BAROCLINIC_WAVES_MICROBIOLOGY,
    )
    WAVES_KINDS = (
        BAROTROPIC_WAVES,
        BAROCLINIC_WAVES,
        BAROTROPIC_WAVES_MICROBIOLOGY,
        BAROCLINIC_WAVES_MICROBIOLOGY,
    )
    MICROBIOLOGY_KINDS = (
        BAROTROPIC_MICROBIOLOGY,
        BAROCLINIC_MICROBIOLOGY,
        BAROTROPIC_WAVES_MICROBIOLOGY,
        BAROCLINIC_WAVES_MICROBIOLOGY
    )


class SimulationKind2:

    class Option:

        BAROCLINIC = 'Baroclinic'
        WAVES = 'Waves'
        MICROBIOLOGY = 'Microbiology'

    def __init__(self, *options):
        self.__options = options

    @property
    def options(self):
        return self.__options

    def has(self, option:str):
        return option in self.options

    def is_baroclinic(self):
        return self.has(self.Option.BAROCLINIC)

    def has_waves(self):
        return self.has(self.Option.WAVES)

    def has_microbiology(self):
        return self.has(self.Option.MICROBIOLOGY)

    _base_forcings_map = {
        BoundaryKind.OCEAN: (
            ForcingKind.ELEVATION,
        ),
        BoundaryKind.RIVER: (
            ForcingKind.FLOW,
        ),
        BoundaryKind.ATMOSPHERIC: (
            ForcingKind.OPTIONAL_AIR,
        ),
    }

    _forcings_map = {
        Option.BAROCLINIC: {
            BoundaryKind.OCEAN: (
                ForcingKind.TEMPERATURE,
                ForcingKind.SALINITY,
            ),
            BoundaryKind.RIVER: (
                ForcingKind.TEMPERATURE,
                ForcingKind.SALINITY
            ),
            BoundaryKind.ATMOSPHERIC: (
                ForcingKind.AIR,
            ),
        },
        Option.WAVES: {
            BoundaryKind.OCEAN: (
                ForcingKind.WAVES,
            ),
        },
        Option.MICROBIOLOGY: {
            BoundaryKind.OCEAN: (
                ForcingKind.E_COLI,
                ForcingKind.ENTEROCOCCUS,
            ),
            BoundaryKind.RIVER: (),
        }
    }

    @property
    def forcings(self):
        def forcings_of_(boundary_kind):
            boundary_forcings_of_ = lambda option: \
                self._forcings_map[option].get(boundary_kind) or ()

            return (
                *self._base_forcings_map[boundary_kind],
                *chain.from_iterable( map(boundary_forcings_of_, self.options) )
            )

        return {
            boundary_kind: forcings_of_(boundary_kind)
                for boundary_kind in self._base_forcings_map.keys()
        }


class ForcingProviders:

    class Provider:

        NONE = 'none'
        USER_INPUT = 'user_input'

        PRISM_2017 = 'prism_2017'
        FES_2014 = 'fes_2014'
        CMEMS_GLOBAL = 'cmems_global'
        CMEMS_IBI = 'cmems_ibi'

        WAVE_WATCH_3 = 'ww3'

        MARETEC_SINERGIA_REGIONAL = 'maretec_sinergia_regional'
        MARETEC_SINERGIA_ALBUFEIRA = 'maretec_sinergia_albufeira'
        METEOFRANCE_ARGPEGE_EUROPE = 'meteofrance_arpege_europe'
        METEOFRANCE_ARGPEGE_GLOBAL = 'meteofrance_arpege_global'
        METEOGALICIA_WRF_WEST_EUROPE = 'meteogalicia_wrf_west_europe'
        METEOGALICIA_WRF_IBERIA_BISCAY = 'meteogalicia_wrf_iberia_biscay'
        METEOGALICIA_WRF_GALICIA = 'meteogalicia_wrf_galicia'
        NOAA_GFS_0P25 = 'noaa_gfs_0p25'
        NOAA_NAM_CONUS = 'noaa_nam_conus'
        NOAA_NAM = 'noaa_nam'
    
    _air_common = (
        Provider.MARETEC_SINERGIA_REGIONAL,
        Provider.MARETEC_SINERGIA_ALBUFEIRA,
        Provider.METEOGALICIA_WRF_WEST_EUROPE,
        Provider.METEOGALICIA_WRF_IBERIA_BISCAY,
        Provider.METEOGALICIA_WRF_GALICIA,
        Provider.NOAA_GFS_0P25,
        Provider.NOAA_NAM_CONUS,
        Provider.NOAA_NAM,
    )

    _providers_map = {
        BoundaryKind.OCEAN: {

            ForcingKind.ELEVATION: (
                Provider.PRISM_2017,
                Provider.FES_2014,
                Provider.CMEMS_GLOBAL,
                Provider.CMEMS_IBI,
            ),
            ForcingKind.TEMPERATURE: (
                Provider.CMEMS_GLOBAL,
                Provider.CMEMS_IBI,
            ),
            ForcingKind.SALINITY: (
                Provider.CMEMS_GLOBAL,
                Provider.CMEMS_IBI,
            ),
            ForcingKind.WAVES: (
                Provider.WAVE_WATCH_3,
            ),

        },
        BoundaryKind.RIVER: {

            ForcingKind.FLOW: (
                Provider.USER_INPUT,
            ),
            ForcingKind.TEMPERATURE: (
                Provider.USER_INPUT,
            ),
            ForcingKind.SALINITY: (
                Provider.USER_INPUT,
            ),

        },
        BoundaryKind.ATMOSPHERIC: {

            ForcingKind.AIR: (
                *_air_common,
                Provider.METEOFRANCE_ARGPEGE_EUROPE,
                Provider.METEOFRANCE_ARGPEGE_GLOBAL,
            ),
            ForcingKind.OPTIONAL_AIR: (
                *_air_common,
                Provider.METEOFRANCE_ARGPEGE_EUROPE,
                Provider.METEOFRANCE_ARGPEGE_GLOBAL,
                Provider.NONE,
            ),
            ForcingKind.RADIATION: _air_common,

        },
    }

    @classmethod
    def providers(self, boundary_kind, forcing_kind):
        return self._providers_map[boundary_kind][forcing_kind]

    @classmethod
    def ocean_providers(self, forcing_kind):
        return self.providers(BoundaryKind.OCEAN, forcing_kind)

    @classmethod
    def river_providers(self, forcing_kind):
        return self.providers(BoundaryKind.RIVER, forcing_kind)

    @classmethod
    def atmospheric_providers(self, forcing_kind):
        return self.providers(BoundaryKind.ATMOSPHERIC, forcing_kind)


class ForcingSource:

    NO_FORCING = 'no_forcing'
    INPUT_VALUE = 'input_value'

    PRISM_2017 = 'prism_2017'
    FES_2014 = 'fes_2014'
    CMEMS_GLOBAL = 'cmems_global'
    CMEMS_IBI = 'cmems_ibi'

    WAVE_WATCH_3 = 'ww3'

    MARETEC_SINERGIA_REGIONAL = 'maretec_sinergia_regional'
    MARETEC_SINERGIA_ALBUFEIRA = 'maretec_sinergia_albufeira'
    METEOFRANCE_ARGPEGE_EUROPE = 'meteofrance_arpege_europe'
    METEOFRANCE_ARGPEGE_GLOBAL = 'meteofrance_arpege_global'
    METEOGALICIA_WRF_WEST_EUROPE = 'meteogalicia_wrf_west_europe'
    METEOGALICIA_WRF_IBERIA_BISCAY = 'meteogalicia_wrf_iberia_biscay'
    METEOGALICIA_WRF_GALICIA = 'meteogalicia_wrf_galicia'
    NOAA_GFS_0P25 = 'noaa_gfs_0p25'
    NOAA_NAM = 'noaa_nam'
    NOAA_NAM_CONUS = 'noaa_nam_conus'

    _bk = BoundaryKind
    _fk = ForcingKind

    _air_common = (
        MARETEC_SINERGIA_REGIONAL,
        MARETEC_SINERGIA_ALBUFEIRA,
        METEOFRANCE_ARGPEGE_EUROPE,
        METEOFRANCE_ARGPEGE_GLOBAL,
        METEOGALICIA_WRF_WEST_EUROPE,
        METEOGALICIA_WRF_IBERIA_BISCAY,
        METEOGALICIA_WRF_GALICIA,
        NOAA_GFS_0P25,
        NOAA_NAM,
        NOAA_NAM_CONUS,
    )

    KIND_MAP = {
        _fk.ELEVATION: (PRISM_2017, FES_2014, CMEMS_GLOBAL, CMEMS_IBI),
        _fk.OCEAN_TEMPERATURE: (CMEMS_GLOBAL, CMEMS_IBI),
        _fk.OCEAN_SALINITY: (CMEMS_GLOBAL, CMEMS_IBI),
        _fk.WAVES: (WAVE_WATCH_3,),

        _fk.FLOW: (INPUT_VALUE,),
        _fk.RIVER_TEMPERATURE: (INPUT_VALUE,),
        _fk.RIVER_SALINITY: (INPUT_VALUE,),

        _fk.AIR: _air_common,
        _fk.OPTIONAL_AIR: (NO_FORCING, *_air_common),
        _fk.RADIATION: _air_common,
    }


class ForcingSeriesKind:

    ANNUAL = 'annual'
    RATIO = 'ratio'
    TH_SERVICE_URL = 'th_service_url'


class StationInOutputKind(IntEnum):

    ELEVATION = 1
    AIR_PRESSURE = 2
    WIND_U = 3
    WIND_V = 4
    TEMPERATURE = 5
    SALINITY = 6
    VELOCITY_U = 7
    VELOCITY_V = 8
    W = 9
