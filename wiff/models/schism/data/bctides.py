from functools import wraps
from itertools import chain
from pathlib import Path

# TODO: indentation should be parent's responsability

# helper functions
def repeat(count, callable, *args, **kwargs):
    for _ in range(count):
        yield callable(*args, **kwargs)

def next_line_from_(iter, split=False, tag=None):
    values, separator, comments = next(iter).partition('!')
    if tag is not None:
        print(f"[{tag}] {values}")
    return tuple( values.split() ) if split else values.strip()

def next_lines_from_(iter, count, split=False, tag=None):
    return tuple( repeat(count, next_line_from_, iter, split, tag) )

def tuple_of_(kind, iterable):
    return tuple( map(kind, iterable) )

def float_tuple_from_(iterable):
    return tuple_of_(float, iterable)

def join(iterable, separator=' ', indent=''):
    return indent + separator.join( map(str, iterable) )

def indent(prefix, iterable):
    return ( f"{prefix}{line}" for line in iterable )

DEBUG = False

def echo(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        # self may be an instance or a class (from a classmethod)
        kind = self if type(self) is type(object) else type(self)
        if DEBUG:
            print(f'{kind.__name__}.{func.__name__}({args} {kwargs})')

        return func(self, *args, **kwargs)

    return wrapper


class _Part:

    @echo
    def __init__(self):
        pass

    def check(self, **kwargs):
        raise NotImplementedError(
            "should check the correctness and return self")

    _NO_INDENT = ''
    _INDENT_1 = '  '
    _INDENT_2 = '    '
    _INDENT_3 = '      '

    def render(self):
        raise NotImplementedError("should produce an interable of strings")

    @classmethod
    def parse(self, lines_iter, **kwargs):
        raise NotImplementedError("should produce a mapping to call __init__")

    @classmethod
    def build(self, iter, *, init_kwargs=dict(), **kwargs):
        return self(**self.parse(iter, **kwargs), **init_kwargs)

    def __repr__(self):
        return f'{type(self).__name__}'

    def __str__(self):
        return '\n'.join( self.render() )


class _Values(_Part):

    @echo
    def __init__(self, lines):
        # self.lines = tuple( tuple_of_(float, line) for line in lines )
        self.lines = tuple(lines)

    def render(self):
        return (
            *( join(line, '  ', self._INDENT_2) for line in self.lines ),
        )

    class SizeMismatch(Exception):

        def __init__(self, expected_kinds, actual_count, line_no):
            expected_str = ', '.join( kind.__name__ for kind in expected_kinds )
            super().__init__(f"expecting {len(expected_kinds)} [{expected_str}]"
                        f" values but found {actual_count} in line #{line_no}!")

    _parse_line_count = 1
    _parse_values_kind = None

    @classmethod
    def parse(self, iter, line_count=None, values_kind=None, **kwargs):
        line_count = line_count or self._parse_line_count
        values_kind = values_kind or self._parse_values_kind

        def parse_lines():
            for line in range(line_count):
                values = next_line_from_(iter, split=True)

                if values_kind and len(values_kind) != len(values):
                    raise self.SizeMismatch(expected_kinds=values_kind,
                                       actual_count=len(values), line_no=line+1)

                if values_kind:
                    values = tuple( kind(value) 
                        for kind, value in zip(values_kind, values) if kind )

                yield values

        return dict(
            lines = tuple( parse_lines() ),
        )

    def __repr__(self):
        return f'{super().__repr__()} line_count={len(self.lines)}'


# class TidalConstituent(_Part):
#     '''Tidal Constituent part
    
#     # for tidal constituents:
#         constituent name
#         tidal values
#     '''

#     @echo
#     def __init__(self, name:str, values:tuple):
#         self.name = str(name)
#         self.values = tuple_of_(float, values)

#     def render(self):
#         return (
#             f'{self._INDENT_1}{self.name}',
#             join(self.values, indent=self._INDENT_2),
#         )

#     @classmethod
#     def parse(self, iter, **kwargs):
#         return dict(
#             name = next_line_of_(iter),
#             values = next_line_of_(iter, split=True),
#         )


class TidalConstituent(_Part):
    '''Tidal Constituent part
    
    # for tidal constituents:
        constituent name
        <values.render>
    '''

    @echo
    def __init__(self, name, definition:_Values):
        self.name = str(name)
        self.definition = definition

    def render(self):
        return (
            f'{self._INDENT_1}{self.name}',
            *self.definition.render(),
        )

    @classmethod
    def parse(self, iter, **kwargs):
        return dict(
            name = next_line_from_(iter),
            definition = _Values.build(iter, **kwargs),
        )

class _Condition(_Part):

    flag = None

    def render(self):
        return ()

    @classmethod
    def parse(self, iter, **kwargs):
        return dict()


class Nothing(_Condition):

    flag = 0


class TimeVarying(_Condition):

    flag = 1


class Constant(_Condition):

    flag = 2

    @echo
    def __init__(self, value, **kwargs):
        self.constant_value = float(value)
        super().__init__(**kwargs)

    def render(self):
        return (
            f'{self._INDENT_1}{self.constant_value}',
            *super().render(),
        )

    @classmethod
    def parse(self, iter, **kwargs):
        return dict(
            value = next_line_from_(iter),
            **super().parse(iter, **kwargs),
        )


# class SegmentNodes(Values):
#     '''Boundary segment nodes part

#     # for segment nodes:
#         node values
#     '''

#     @echo
#     def __init__(self, *, nodes):
#         super().__init__(nodes)

#     @classmethod
#     def parse(self, iter, node_count, **kwargs):
#         return dict(
#             nodes = 
#                   super().parse(iter, line_count=node_count, **kwargs)['lines'],
#         )


# class Tidal(_Condition):
#     '''Tidal constituents condition

#     # for tidal constituents:
#         constituent name
#         # for segment nodes:
#             SegmentNodes
#     '''

#     flag = 3

#     @echo
#     def __init__(self, constituents:dict):
#         self.constituents = constituents

#     def render(self):
#         return chain.from_iterable(
#             ( f'{self._INDENT_2}{name}', *nodes.render() )
#                 for name, nodes in self.constituents.items()
#         )

#     @classmethod
#     def parse(self, iter, *, constituent_count, **kwargs):
#         return dict(
#             constituents = dict(
#                 # name: segment nodes
#                 ( next_line_of_(iter), SegmentNodes.build(iter, **kwargs) )
#                     for _ in range(constituent_count)
#             ),
#         )


class Tidal(_Condition):
    '''Tidal constituents condition

    # for tidal constituents:
        constituent name
        # for segment nodes:
            SegmentNodes
    '''

    flag = 3

    @echo
    def __init__(self, constituents):
        self.constituents = tuple(constituents)

    def render(self):
        return chain.from_iterable( constituent.render()
                                          for constituent in self.constituents )

    @classmethod
    def parse(self, iter, *, constituent_count, node_count, vector=False,
                                                                      **kwargs):
        values_kind = (float, float, float, float) if vector else (float, float)
        return dict(
            constituents = (
                repeat(constituent_count, TidalConstituent.build, iter,
                       line_count=node_count, values_kind=values_kind, **kwargs)
            )
        )


class SpaceTimeVarying(_Condition):

    flag = 4


class TidalAndSpaceTimeVarying(Tidal):

    flag = 5


class Flanther(_Condition):

    flag = -1


class _InfluxNudging(_Condition):

    @echo
    def __init__(self, *, inflow_factor):
        self.inflow_factor = float(inflow_factor)

    def render(self):
        return (
            f'{self._INDENT_2}{self.inflow_factor}',
        )

    @classmethod
    def parse(self, iter, **kwargs):
        return dict(
            inflow_factor = next_line_from_(iter),
        )


class NudgedConstant(Constant, _InfluxNudging):
    pass


class FullNudgedSpaceTimeVarying(_Condition):

    flag = -4

    @echo
    def __init__(self, *, inflow_factor, outflow_factor):
        self.inflow_factor = float(inflow_factor)
        self.outflow_factor = float(outflow_factor)

    def render(self):
        return (
            f'{self._INDENT_2}{self.inflow_factor} {self.outflow_factor}',
        )

    @classmethod
    def parse(self, iter, **kwargs):
        inflow, outflow = next_line_from_(iter, split=True)
        return dict(
            inflow_factor = inflow,
            outflow_factor = outflow,
        )



class NudgedTimeVarying(_InfluxNudging):

    flag = 1


# TODO: consider moving to prepare_fib
class NudgedMicrobiologyConstants(_InfluxNudging):

    flag = 2

    @echo
    def __init__(self, e_coli, enterococcus, **kwargs):
        self.e_coli = float(e_coli)
        self.enterococcus = float(enterococcus)
        super().__init__(**kwargs)

    def render(self):
        return (
            f'{self._INDENT_1}{self.e_coli} {self.enterococcus}',
            *super().render(),
        )

    @classmethod
    def parse(self, iter, **kwargs):
        e_coli, enterococcus = next_line_from_(iter, split=True)
        return dict(
            e_coli = e_coli,
            enterococcus = enterococcus,
            **super().parse(iter, **kwargs),
        )


class NudgedInitialProfile(_InfluxNudging):

    flag = 3


class NudgedSpaceTimeVarying(_InfluxNudging):

    flag = 4


class OpenBoundary(_Part):

    elevation_conditions = (
        Nothing, # velocity must be specified
        TimeVarying,
        Constant,
        Tidal,
        SpaceTimeVarying,
        TidalAndSpaceTimeVarying,
    )

    velocity_conditions = (
        Nothing,
        TimeVarying,
        Constant,
        Tidal,
        SpaceTimeVarying,
        TidalAndSpaceTimeVarying,
        Flanther,
        FullNudgedSpaceTimeVarying,
    )

    temperature_conditions = salinity_conditions = (
        Nothing,
        NudgedTimeVarying,
        NudgedConstant,
        NudgedInitialProfile,
        NudgedSpaceTimeVarying
    )

    tracer_conditions = (
        *temperature_conditions,
        NudgedMicrobiologyConstants,
    )

    @echo
    def __init__(self, *,
        node_count : int,
        elevation : _Condition = Nothing(),
        velocity : _Condition = Nothing(),
        temperature : _Condition = Nothing(),
        salinity : _Condition = Nothing(),
        tracers : tuple = (),
    ):
        self.node_count = node_count
        self.elevation = elevation
        self.velocity = velocity
        self.temperature = temperature
        self.salinity = salinity
        self.tracers = tuple(tracers)

        self.check()

    def check(self, *, elevation=None, velocity=None, temperature=None,
                                                    salinity=None, tracers=() ):

        names_of_ = lambda conditions: ', '.join( condition.__name__
                                                   for condition in conditions )

        elevation_condition = type(elevation or self.elevation)
        if elevation_condition not in self.elevation_conditions:
            raise ValueError(
                "elevation condition can't be {used}, only {allowed}!".format(
                    used = elevation_condition.__name__,
                    allowed = names_of_(self.elevation_conditions)
                )
            )

        velocity_condition = type(velocity or self.velocity)
        if velocity_condition not in self.velocity_conditions:
            raise ValueError(
                "velocity condition can't be {used}, only {allowed}!".format(
                    used = velocity_condition.__name__,
                    allowed = names_of_(self.velocity_conditions)
                )
            )

        if elevation_condition == Nothing and velocity_condition == Nothing:
            raise ValueError(
                      "elevation and velocity conditions can't be both Nothing")

        temperature_condition = type(temperature or self.temperature)
        if temperature_condition not in self.temperature_conditions:
            raise ValueError(
                "temperature condition can't be {used}, only {allowed}!".format(
                    used = temperature_condition.__name__,
                    allowed = names_of_(self.temperature_conditions)
                )
            )

        salinity_condition = type(salinity or self.salinity)
        if salinity_condition not in self.salinity_conditions:
            raise ValueError(
                "salinity condition can't be {used}, only {allowed}!".format(
                    used = salinity_condition.__name__,
                    allowed = names_of_(self.salinity_conditions)
                )
            )

        for index, tracer in enumerate(tracers, start=1):

            tracer_condition = type(tracer or self.tracers)
            if tracer_condition not in self.tracer_conditions:
                raise ValueError(
                    "tracer #{index} condition can't be {used}"
                                                     ", only {allowed}!".format(
                        index = index,
                        used = tracer_condition.__name__,
                        allowed = names_of_(self.tracer_conditions)
                    )
                )

    def render(self):
        conditions = (self.elevation, self.velocity, self.temperature,
                                                   self.salinity, *self.tracers)

        first_line_elements = ( self.node_count, *( condition.flag
                                                 for condition in conditions ) )

        return indent(self._INDENT_1, (
            join(first_line_elements),
            *chain.from_iterable( condition.render()
                                                  for condition in conditions ),
        ) )

    def _flag_map_of_(conditions):
        return { condition.flag: condition for condition in conditions }

    _elevation_condition_flag_map = _flag_map_of_(elevation_conditions)
    _velocity_condition_flag_map = _flag_map_of_(velocity_conditions)
    _temperature_condition_flag_map = _flag_map_of_(temperature_conditions)
    _salinity_condition_flag_map = _flag_map_of_(salinity_conditions)
    _tracer_condition_flag_map = _flag_map_of_(tracer_conditions)

    @classmethod
    def parse(self, iter, **kwargs):
        node_count, elevation_flag, velocity_flag, temperature_flag, \
                salinity_flag, *tracer_flags = next_line_from_(iter, split=True)

        build_ = lambda map, flag, **_kwargs: \
            map[int(flag)].build(iter, node_count=int(node_count), **kwargs,
                                                                      **_kwargs)

        return dict(
            node_count = node_count,
            elevation = build_(self._elevation_condition_flag_map,
                                                                elevation_flag),
            velocity = build_(self._velocity_condition_flag_map, velocity_flag,
                                                                   vector=True),
            temperature = build_(self._temperature_condition_flag_map,
                                                              temperature_flag),
            salinity = build_(self._salinity_condition_flag_map, salinity_flag),
            tracers = tuple(
                build_(self._tracer_condition_flag_map, flag)
                                                     for flag in tracer_flags ),
        )


class BCTidesIn:

    def __init__(self, *,
        header : str = 'bctides',
        tidal_potential : tuple = (),
        tp_cut_off_depth : float = .0,
        tidal_forcing : tuple = (),
        open_boundaries : tuple = ()
    ):

        self.header = str(header)
        self.tp_cut_off_depth = float(tp_cut_off_depth)
        self.tidal_potential = tuple(tidal_potential)
        self.tidal_forcing = tuple(tidal_forcing)
        self.open_boundaries = tuple(open_boundaries)

    @classmethod
    def from_text(self, text:str):
        return self.parse( iter( text.splitlines() ) )

    @classmethod
    def from_file(self, path):
        return self.parse( iter( Path(path).read_text().splitlines() ) )

    def __str__(self):
        return '\n'.join( self.render() )

    def __repr__(self):
        return f"BCTidesIn: header={self.header!r}"

    def render(self):
        render_ = lambda sequence: chain.from_iterable( entry.render()
                                                         for entry in sequence )
        return (
            self.header,
            f"{len(self.tidal_potential)} {self.tp_cut_off_depth}",
            *render_(self.tidal_potential),
            f"{len(self.tidal_forcing)}",
            *render_(self.tidal_forcing),
            f"{len(self.open_boundaries)}",
            *render_(self.open_boundaries),
        )

    def to_file(self, dir_path, name='bctides.in'):
        Path(dir_path, name).write_text( str(self) )

    @classmethod
    def parse(self, iter):
        '''
        header
        tidal potential count, tidal potential cut-off depth
        # for tidal potential constituents:
            TidalConstituent(name, tidal species, amplitude constants,
                      angular frequency, nodal factor, earth equilibrium (deg) )
        tidal forcing count
        # for tidal forcing constituents:
            TidalConstituent(name, angular frequency (rad/s), nodal factor,
                                                       earth equilibrium (deg) )
        open boundary segments count
        # for open boundary segments:
            segment node count, B.C. flags for elevation, velocity, temperature,
                salinity, and (optionally) for each tracer module invoked
                (order: GEN, AGE, SED3D, EcoSim, ICM, CoSiNE, FIB, and TIMOR)

            # case ELEVATION flag:
            # = 0:
                # VELOCITY MUST BE SPECIFIED!
            # = 2:
                constant value
            # = 3 or 5:
                # for tidal forcing constituents:
                    constituent name
                    # for segment nodes:
                        node amplitude, and phase

            # case VELOCITY flag:
            # = 2:
                constant value
            # = 3 or 5:
                # for tidal forcing constituents:
                    constituent name
                    # for segment nodes:
                        node amplitude, and phase vectors (u,v)
            # = -1:
                mean elevation below
                # for segment nodes:
                    node mean elevation
                mean normal velocity
                # for segment nodes:
                    node mean normal velocity (at all levels)
            # = -4:
                inflow, and outflow nudging factor

            # case TEMPERATURE, SALINITY or TRACER(S) flag:
            # = 1, 3 or 4:
                nudging factor for inflow
            # = 2:
                constant value
                nudging factor for inflow
        '''

        build_ = lambda part, count, **kwargs: tuple(
            part.build(iter, **kwargs) for _ in range( int(count) ) )

        # build_ = lambda part, count, **kwargs: tuple(
        #                         repeat(int(count), part.build, iter, **kwargs) )

        header = next_line_from_(iter)

        tp_count, tp_cut_off_depth = next_line_from_(iter, split=True)
        tidal_potential = build_(TidalConstituent, tp_count,
                                 values_kind=(int, float, float, float, float) )

        tf_count = next_line_from_(iter)
        tidal_forcing = build_(TidalConstituent, tf_count,
                                             values_kind=(float, float, float) )

        ob_count = next_line_from_(iter)
        open_boundaries = build_(OpenBoundary, ob_count,
                                               constituent_count=int(tf_count) )

        return self(
            header = header,
            tidal_potential = tidal_potential,
            tp_cut_off_depth = tp_cut_off_depth,
            tidal_forcing = tidal_forcing,
            open_boundaries = open_boundaries,
        )



if __name__ == '__main__':
    from sys import argv
    path, = argv[1:]

    base = BCTidesIn.from_file(path)
    for bnd in base.open_boundaries:
        bnd.tracers = (
            NudgedMicrobiologyConstants(.001, .001, inflow_factor=.33),
        )
    
    print(base)
