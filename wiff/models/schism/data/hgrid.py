from io import StringIO
from itertools import chain
from functools import partial


# TODO: lazy execution
class HorizontalGrid:

    def __init__(self, header='', nodes=(), elements=(),
                           open_boundaries=(), land_boundaries=(), islands=() ):
        self.nodes = tuple(nodes)
        self.elements = tuple(elements)
        self.open_boundaries = tuple(open_boundaries)
        self.land_boundaries = tuple(land_boundaries)
        self.islands = tuple(islands)
        self.header = header

    # TODO: implement checks
    @classmethod
    def parse_file(self, obj):
        read_line_ = lambda: obj.readline().strip()
        read_n_split_line_ = lambda n=None: read_line_().split()[:n]

        parse_int_line_ = lambda: int( read_n_split_line_(n=1)[0] )
        parse_ints_line_ = lambda n=None: map(int, read_n_split_line_(n) )

        iter_tuple_ = lambda fn, it: tuple( map(fn, it) )
        size_tuple_ = lambda fn, sz: tuple( fn() for _ in range(sz) )
        bnd_nodes_tuple_ = lambda sz: size_tuple_(parse_int_line_, sz)

        def parse_node():
            node_index, *node_coords = read_n_split_line_(n=4)
            return iter_tuple_(float, node_coords)

        def parse_element():
            elem_index, elem_type, *elem_nodes = read_n_split_line_()
            return iter_tuple_(int, elem_nodes)

        def parse_open_boundary():
            node_count = parse_int_line_()
            return bnd_nodes_tuple_(node_count)

        def parse_land_n_island_boundary():
            node_count, island_flag = parse_ints_line_(n=2)
            bnd_list = island_boundary_list if island_flag \
                                                         else land_boundary_list
            bnd_list.append( bnd_nodes_tuple_(node_count) )

        header = read_line_()
        #print(header)

        element_count, node_count = parse_ints_line_(n=2)
        #print(element_count, node_count)

        nodes = size_tuple_(parse_node, node_count)
        #print(len(nodes))

        elements = size_tuple_(parse_element, element_count)
        #print(len(elements))

        open_boundary_count = parse_int_line_()
        #print(open_boundary_count)
        total_open_bnd_node_count = parse_int_line_()
        #print(total_open_bnd_node_count)

        open_boundaries = size_tuple_(parse_open_boundary, open_boundary_count)
        #print(len(open_boundaries))

        land_island_boundary_count = parse_int_line_()
        #print(land_island_boundary_count)
        total_land_island_boundary_node_count = parse_int_line_()
        #print(total_land_island_boundary_node_count)

        land_boundary_list = list()
        island_boundary_list = list()
        for land_island_bnd_idx in range(land_island_boundary_count):
            parse_land_n_island_boundary()
        #print(len(land_boundary_list))
        #print(len(island_boundary_list))

        return self(header, nodes, elements, open_boundaries,
                                       land_boundary_list, island_boundary_list)

    @classmethod
    def parse(self, text):
        return self.parse_file( StringIO(text) )

    @classmethod
    def open(self, path):
        with open(path) as obj:
            return self.parse_file(obj)

    def boundary_nodes(self, index):
        return tuple( (node, self.nodes[node-1])
                                       for node in self.open_boundaries[index] )

    _wwm_flag = 2

    def wwm_boundary_per_nodes(self, indexes):
        wwm_flag_dim = [0] * len(self.nodes)
        for node_index in indexes:
            wwm_flag_dim[node_index-1] = self._wwm_flag

        return HorizontalGrid(
            nodes = tuple( (x, y, wwm)
                          for (x, y, _), wwm in zip(self.nodes, wwm_flag_dim) ),
            header = "WWM boundary grid",
        )

    def wwm_boundary(self, indexes):
        return self.wwm_boundary_per_nodes(
            chain.from_iterable( self.open_boundaries[index-1]
                                                          for index in indexes )
        )

    def constant_grid(self, value):
        return HorizontalGrid(
            nodes = tuple( (x, y, value) for x, y, _ in self.nodes ),
            header = "Constant value grid",
        )

    def save(self, path):
        with open(path, mode='w') as hgrid_file:

            write_line_ = lambda ln: hgrid_file.write(f'{ln}\n')
            write_list_ = lambda ls, sep=' ': write_line_(
                                                       sep.join(map(str, ls) ) )
            write_list_line_ = lambda *lsn: write_list_(lsn)
            write_lines_ = lambda lns: write_list_(lns, sep='\n')

            nest_len = lambda *nls: sum( map(len, chain.from_iterable(nls) ) )

            def write_indexed_section_(entries):
                for index, entry in enumerate(entries, start=1):
                    write_list_line_(index, *entry)

            def write_indexed_len_section_(entries):
                for index, entry in enumerate(entries, start=1):
                    write_list_line_(index, len(entry), *entry)

            def write_boundary_(node_indexes, is_island=None):
                node_count = len(node_indexes)
                write_header_line_ = partial(write_list_line_, node_count)

                if is_island is None:
                    write_header_line_()
                else:
                    write_header_line_( int(is_island) )
                write_lines_(node_indexes)

            def write_boundary_section_(entries, is_island=None):
                for entry in entries:
                    write_boundary_(entry, is_island)

            write_line_(self.header)
            write_list_line_(len(self.elements), len(self.nodes) )

            write_indexed_section_(self.nodes)
            write_indexed_len_section_(self.elements)

            open_boundary_count = len(self.open_boundaries)

            if open_boundary_count:
                write_line_(open_boundary_count)
                write_line_( nest_len(self.open_boundaries) )
                write_boundary_section_(self.open_boundaries)

            land_boundary_count = len(self.land_boundaries)+len(self.islands)

            if land_boundary_count:
                write_line_(land_boundary_count)
                write_line_(
                         nest_len(self.land_boundaries)+nest_len(self.islands) )
                write_boundary_section_(self.land_boundaries, is_island=False)
                write_boundary_section_(self.islands, is_island=True)


def _main(in_path, out_path):

    grid = HorizontalGrid.open(in_path)
    print( *zip( *grid.boundary_nodes(0) ) )
    # grid.wwm_boundary( (1,) )
    # grid.save(out_path)

if __name__ == '__main__':
    from sys import argv
    _main(*argv[1:])
