import re
from datetime import timedelta
from pathlib import Path


class ParamIn:

    class Parameter:

        TIMESTEP = 'dt'
        HOTSTART_FLAG = 'ihot'


    PARAM_REGEX_TEMPLATE = (
         r'(?P<pre_value>^\s* \{param\} \s*=\s* )'
         r'(?P<value>.+?)'
         r'(?P<post_value> \s*(?:!.*)?$)'
    )

    def __init__(self, base, *, timestep=None, hotstarted=None, params=None):
        self._base = base
        self._param_in = base
        self._params = params or dict()

        if timestep:
            self.timestep = timestep

        if hotstarted != None:
            self.hotstarted = hotstarted

    @classmethod
    def from_file(cls, path, **kwargs):
        base = Path(path).read_text()
        return cls(base, **kwargs)

    @property
    def base(self):
        return self._base

    @property
    def timestep(self):
        try:
            return self._timestep
        except AttributeError:
            timestep_secs = int( self.get_param(self.Parameter.TIMESTEP) )
            return timedelta(seconds=timestep_secs)

    @timestep.setter
    def set_timestep(self, timedelta):
        self.set_param(self.Parameter.TIMESTEP, timedelta.total_seconds() )
        # self.set_param(Parameter., ) # remaining dependencies
        self._timestep = timedelta

    @property
    def hotstarted(self):
        try:
            return self._hotstarted
        except AttributeError:
            return int( self.get_param(self.Parameter.HOTSTART_FLAG) )

    @hotstarted.setter
    def set_hotstarted(self, is_hotstarted):
        self.set_param(self.Parameter.HOTSTART_FLAG, 2 if hotstart else 0)
        self._hotstarted = is_hotstarted

    def _change_param_in(self, param, value):
        param_re = self.PARAM_REGEX_TEMPLATE.format(param=param)
        replace = rf'\g<pre_value>{value}\g<post_value>'
        self._param_in, changes = re.subn(param_re, replace, self._param_in,
                           flags=re.ASCII|re.IGNORECASE|re.MULTILINE|re.VERBOSE)

        return changes

    def get_param(self, param):
        return self._params.get(param) or self.base_param(param)

    def set_param(self, param, value):
        changes = self._change_param_in(param, value)
        if changes:
            self._params[param] = value

        return changes

    def base_param(self, param):
        param_re = self.PARAM_REGEX_TEMPLATE.format(param=param)
        re_matches = re.search(param_re, self.base,
                           flags=re.ASCII|re.IGNORECASE|re.MULTILINE|re.VERBOSE)

        return re_matches['value']

    def as_string(self):
        return self._param_in

    def save_as_file(self, path):
        Path(path).write_text(self._param_in)
        print(f"param.in saved as {path}")

    @classmethod
    def test(cls, base_path, param_in_path):
        params = dict(
        )
        param_in = cls.from_file(Path(base_path), **params)

        def get_n_print(param):
            print( param_in.base_param(param) )

        #get_n_print('ics')
        #param_in.save_as_file( Path(param_in_path) )
        param_in.set_param('ics', 3)
        param_in.set_param('ipre', 1)
        print(param_in.as_string())


from enum import IntEnum
from datetime import datetime, time, timezone
from f90nml import Namelist, Parser
from functools import wraps
from pprint import pformat


class ParamNml:

    def __init__(self, text, *, core=dict(), optional=dict(), output=dict(),
                                                                   update=True):
        self._namelist = Parser().reads(text)
        self._namelist.patch( Namelist(CORE=core, OPT=optional, SCHOUT=output) )

        self._update = update

    @classmethod
    def from_file(self, path, **kwargs):
        return self(Path(path).read_text(), **kwargs)

    def __str__(self) -> str:
        return str(self.namelist)

    def __repr__(self) -> str:
        return pformat(self.namelist, compact=True)

    def to_file(self, dir_path, name='param.nml', force=False, sort=False):
        file_path = Path(dir_path, name)
        self.namelist.write(file_path, force=force, sort=sort)

    @property
    def namelist(self) -> Namelist:
        return self._namelist

    @property
    def core(self) -> Namelist:
        return self.namelist['CORE']

    @property
    def optional(self) -> Namelist:
        return self.namelist['OPT']

    @property
    def output(self) -> Namelist:
        return self.namelist['SCHOUT']

    def _update_properties(*names):

        def decorator(function):

            @wraps(function)
            def wrapper(self, value):
                if not self._update:
                    return function(self, value)

                values = list()
                for name in names:
                    try:
                        values.append( getattr(self, name) )
                    except KeyError:
                        pass

                function_return = function(self, value)

                for name, value in zip(names, values):
                    setattr(self, name, value)

                return function_return

            return wrapper

        return decorator

    # def _interval(self, value, unit):
    #     return value if isinstance(value, timedelta) \
    #                                               else timedelta(**{unit:value})

    # def _steps(self, value, unit):
    #     self._timedelta(value, unit) / timedelta(**{unit:1})

    @property
    def period(self) -> timedelta:
        return timedelta(days=self.core['rnday'])

    @period.setter
    def period(self, value:timedelta):
        self.core['rnday'] = value / timedelta(days=1)

    @property
    def timestep(self) -> timedelta:
        return timedelta(seconds=self.core['dt'])

    @timestep.setter
    @_update_properties('spool_timestep', 'spool_write_timestep',
                                      'hotstart_write_timestep', 'wwm_timestep')
    def timestep(self, value:timedelta):
        self.core['dt'] = value.total_seconds()

    @property
    def start_datetime(self) -> datetime:
        start_ = lambda part: int(self.optional[f'start_{part}'])
        start_hours_ = lambda kind: float(self.optional[kind])

        start_timezone = timezone(timedelta(hours=-start_hours_('utc_start') ) )
        start_date = datetime(start_('year'), start_('month'), start_('day'),
                                                          tzinfo=start_timezone)
        offset_time = timedelta(hours=start_hours_('start_hour') )

        return start_date + offset_time

    @start_datetime.setter
    def start_datetime(self, value:datetime):
        # TODO: handle timezone/utc_start relation
        in_hours_ = lambda interval: interval / timedelta(hours=1)

        start_hour = in_hours_(
                        value - datetime.combine(value, time(0), value.tzinfo) )
        # utc_start_interval = value.astimezone(timezone.utc) -

        self.optional.update(
            start_year = value.year,
            start_month = value.month,
            start_day = value.day,
            start_hour = start_hour,
            utc_start = start_hour,
#            utc_start = in_hours_(utc_start_interval)
        )

    # @property
    # def spool_timestep(self) -> timedelta:
    #     return self.timestep * int(self.core['nspool'])

    # @spool_timestep.setter
    # def spool_timestep(self, value:timedelta):
    #     steps = value / self.timestep

    #     if not steps.is_integer():
    #         raise ValueError(f"spool_timestep ({value}) MUST BE multiple of"
    #                                             f" timestep ({self.timestep})!")

    #     self.core['nspool'] = int(steps)

    # @property
    # def spool_write_timestep(self) -> timedelta:
    #     return self.timestep * int(self.core['ihfskip'])

    # @spool_write_timestep.setter
    # def spool_write_timestep(self, value:timedelta):
    #     if not (value/self.spool_timestep).is_integer():
    #         raise ValueError(f"spool_write_timestep ({value}) MUST BE multiple"
    #                              f" of spool_timestep ({self.spool_timestep})!")

    #     self.core['ihfskip'] = int(value/self.timestep)

    def _timestep_getter(self, setting):
        return self.timestep * int(setting)

    def _timestep_setter(self, section, name, value, divisor):
        if not (value/divisor).is_integer():
            raise ValueError(f"{name}={value} MUST BE multiple of {divisor}!")

        section[name] = int(value/self.timestep)

    @property
    def spool_timestep(self) -> timedelta:
        return self._timestep_getter(self.core['nspool'])

    @spool_timestep.setter
    def spool_timestep(self, value:timedelta):
        self._timestep_setter(self.core, 'nspool', value, self.timestep)

    @property
    def spool_write_timestep(self) -> timedelta:
        return self._timestep_getter(self.core['ihfskip'])

    @spool_write_timestep.setter
    def spool_write_timestep(self, value:timedelta):
        self._timestep_setter(self.core, 'ihfskip', value, self.spool_timestep)

    @property
    def hotstart_write(self) -> bool:
        return bool(self.output['nhot'])

    @hotstart_write.setter
    def hotstart_write(self, value:bool):
        self.output['nhot'] = int(value)

    @property
    def hotstart_write_timestep(self) -> timedelta:
        return self._timestep_getter(self.output['nhot_write'])

    @hotstart_write_timestep.setter
    def hotstart_write_timestep(self, value:timedelta):
        self._timestep_setter(self.output, 'nhot_write', value,
                                                      self.spool_write_timestep)
        self.hotstart_write = True

    # @property
    # def is_coldstarted(self):
    #     return int(self.optional['ihot']) == 0

    # @is_coldstarted.setter
    # def is_coldstarted(self, value:bool):
    #     if not value:
    #         raise ValueError(f"value must be true")

    #     self.optional['ihot'] = 0

    # @property
    # def is_hotstarted_time_reset(self):
    #     return int(self.optional['ihot']) == 0

    # @is_hotstarted_time_reset.setter
    # def is_hotstarted_time_reset(self, value:bool):
    #     if not value:
    #         raise ValueError(f"value must be true")

    #     self.optional['ihot'] = 1

    # @property
    # def is_hotstarted_time_resume(self):
    #     return int(self.optional['ihot']) == 0

    # @is_hotstarted_time_resume.setter
    # def is_hotstarted_time_resume(self, value:bool):
    #     if not value:
    #         raise ValueError(f"value must be true")

    #     self.optional['ihot'] = 2


    class CoordinateSystemOption(IntEnum):

        CARTESIAN = 1
        GEOGRAPHIC = 2
        LAT_LON = 2


    @property
    def coordinate_system(self):
        return self.CoordinateSystemOption( int(self.optional['ics']) )

    @coordinate_system.setter
    def coordinate_system(self, option:CoordinateSystemOption):
        self.optional['ics'] = option.value


    class HotstartOption(IntEnum):

        COLDSTART = 0
        DISABLED = 0
        WITH_TIME_RESET = 1
        WITH_TIME_RESUME = 2


    @property
    def hotstart(self):
        return self.HotstartOption( int(self.optional['ihot']) )

    @hotstart.setter
    def hotstart(self, option:HotstartOption):
        self.optional['ihot'] = option.value

    @property
    def wwm_step(self) -> int:
        return int(self.optional['nstep_wwm'])

    @wwm_step.setter
    def wwm_step(self, value:int):
        if isinstance(value, int) or \
                             (isinstance(value, float) and value.is_integer() ):
            self.optional['nstep_wwm'] = int(value)

        else:
            raise TypeError(f"wwm_step ({value}) MUST BE interger!")

    @property
    def wwm_timestep(self) -> timedelta:
        return self._timestep_getter(self.wwm_step)

    @wwm_timestep.setter
    def wwm_timestep(self, value:timedelta):
        self._timestep_setter(self.optional, 'nstep_wwm', value, self.timestep)


if __name__ == '__main__':
    from sys import argv

    nml_base, nml_dir = argv[1:]
    nml = ParamNml.from_file(nml_base,
        core = dict(
            dt=30.,
        ),
        optional = dict(),
        output = dict(),
    )
    print(nml.timestep)
    nml.period = timedelta(hours=48)
    print(nml.period)
    nml.start_datetime = datetime(2021, 4, 5, 0)
    print(nml.start_datetime)
    nml.spool_timestep = timedelta(hours=1)
    print(nml.spool_timestep, nml.core['nspool'])
    nml.spool_write_timestep = timedelta(days=1)
    print(nml.spool_write_timestep, nml.core['ihfskip'])
    nml.timestep = timedelta(seconds=100)
    nml.spool_timestep = timedelta(hours=1)
    print(nml.spool_timestep, nml.core['nspool'])
    nml.spool_write_timestep = timedelta(days=1)
    print(nml.spool_write_timestep, nml.core['ihfskip'])
    nml.hotstart_write_timestep = timedelta(days=1)
    print(nml.hotstart_write_timestep)
    nml.coordinate_system = nml.CoordinateSystemOption.GEOGRAPHIC
    print(nml.coordinate_system)
