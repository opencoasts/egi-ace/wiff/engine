import netCDF4

from .hgrid import HorizontalGrid

def convert_to_ncwms(in_path, out_path, hgrid_path=None):

    hgrid_x_var, hgrid_y_var = None, None
    if hgrid_path:
        hgrid = HorizontalGrid.open(hgrid_path)
        hgrid_x_var, hgrid_y_var = tuple( zip(*hgrid.nodes) )[:2]

    with netCDF4.Dataset(in_path) as in_ds, \
             netCDF4.Dataset(out_path, 'w', format='NETCDF4_CLASSIC') as out_ds:

        # Global Attributes
        out_ds.Conventions = 'CF-1.4, UGRID-0.9'

        # Create Dimensions
        out_ds.createDimension('time', in_ds.dimensions['time'].size)
        out_ds.createDimension('nodes',
                                    in_ds.dimensions['nSCHISM_hgrid_node'].size)
        out_ds.createDimension('elements',
                                    in_ds.dimensions['nSCHISM_hgrid_face'].size)
        out_ds.createDimension('element_size', 3)

        levels_dim = out_ds.createDimension('levels',
                                  in_ds.dimensions['nSCHISM_vgrid_layers'].size)

        def new_variable(name, dims, values, np_type=netCDF4.numpy.float32,
                  attrs=dict(), zlib=True, fill_value=-999., least_sig_digit=3):

            var = out_ds.createVariable(name, np_type, dims, zlib=zlib,
                 fill_value=fill_value, least_significant_digit=least_sig_digit)
            var.setncatts(attrs)

            if values is not None:
                var[:] = values

            return var

        new_variable('time', ('time',), in_ds.variables['time'][:],
            attrs = dict(
                standard_name = 'time',
                long_name = 'Time',
                units = in_ds.variables['time'].units,
                base_date = in_ds.variables['time'].base_date,
            )
        )
        new_variable('mesh', (), None, netCDF4.numpy.int32,
            attrs = dict(
                long_name = 'Topology data of 2D unstructured mesh',
                cf_role = 'mesh_topology',
                face_node_connectivity = 'element_nodes',
                node_coordinates = 'node_lon node_lat',
                topology_dimension = 2,
            )
        )
        new_variable('element_nodes', ('elements', 'element_size'),
            in_ds.variables['SCHISM_hgrid_face_nodes'][:,:3],
            netCDF4.numpy.int32,
            attrs = dict(
                long_name = 'Maps every face to its corner nodes',
                cf_role = 'face_node_connectivity',
                units = 'non-dimensional',
                start_index = 1,
            )
        )
        new_variable('node_lon', ('nodes',),
            hgrid_x_var or in_ds.variables['SCHISM_hgrid_node_x'][:],
            least_sig_digit = 6,
            attrs = dict(
                standard_name = 'longitude',
                long_name = 'Longitude of 2D mesh nodes',
                units = 'degrees_east',
                mesh = 'mesh',
            )
        )
        new_variable('node_lat', ('nodes',),
            hgrid_y_var or in_ds.variables['SCHISM_hgrid_node_y'][:],
            least_sig_digit = 6,
            attrs = dict(
                standard_name = 'latitude',
                long_name = 'Latitude of 2D mesh nodes',
                units = 'degrees_north',
                mesh = 'mesh',
            )
        )
        new_variable('levels', ('levels',),
            netCDF4.numpy.arange(levels_dim.size),
            attrs=dict(
                standard_name = 'sigma',
                long_name = 'Whole levels',
                units = 'non-dimensional',
                positive = 'up',
            )
        )

        data_var_dims = ('time', 'nodes', 'levels')
        data_geom_atts = dict(
            mesh = 'mesh',
            location = 'node',
            coordinates = 'node_lon node_lat',
        )
        def copy_data_variable(name, target=None, slice=...,
                    dims=data_var_dims, attrs=dict(), optional=False, **kwargs):

            if not optional or name in in_ds.variables:
                new_variable(target or name, dims, in_ds.variables[name][slice],
                                  attrs=dict(attrs, **data_geom_atts), **kwargs)

        copy_data_variable('depth', dims=('nodes',),
            attrs = dict(
                standard_name = 'sea_floor_depth_below_sea_level',
                long_name = 'Bathymetry',
                positive = 'down',
                units = 'meters',
            )
        )
        copy_data_variable('elev', dims=('time', 'nodes'),
            attrs=dict(
                standard_name = 'sea_surface_height_above_sea_level',
                long_name = 'Sea surface elevation',
                positive = 'up',
                units = 'meters',
            )
        )
        copy_data_variable('hvel', 'hvel_x', slice=(..., 0),
            attrs=dict(
                standard_name = 'eastward_sea_water_velocity',
                long_name = 'Eastward water velocity',
            )
        )
        copy_data_variable('hvel', 'hvel_y', slice=(..., 1),
            attrs=dict(
                standard_name = 'northward_sea_water_velocity',
                long_name = 'Northward water velocity',
            )
        )
        copy_data_variable('temp', optional=True,
            attrs=dict(
                long_name = 'Water temperature',
                standard_name = 'sea_water_temperature',
            )
        )
        copy_data_variable('salt', optional=True,
            attrs=dict(
                standard_name = 'sea_water_practical_salinity',
                long_name = 'Water practical salinity',
            )
        )
        copy_data_variable('zcor', optional=True,
            attrs=dict(
                standard_name = 'z_coordinates',
                long_name = 'Z coordinates at whole levels',
                positive = 'up',
                units = 'meters',
            )
        )

def adapt_to_ncwms(netcdf_path, hgrid_ll_path=None, fib_factor_map=dict() ):

    with netCDF4.Dataset(netcdf_path, mode='a') as dataset:

        time_var = dataset.variables['time']
        if not time_var:
            return

        date_times = netCDF4.num2date(time_var, time_var.units)
        new_time_unit = 'hours since 2000-01-01 00:00:00'

        time_var[:] = netCDF4.date2num(date_times, new_time_unit)
        time_var.units = new_time_unit

        if hgrid_ll_path:
            hgrid = HorizontalGrid.open(hgrid_ll_path)

            node_x_var = dataset.variables['SCHISM_hgrid_node_x']
            node_y_var = dataset.variables['SCHISM_hgrid_node_y']
            node_x_var[:], node_y_var[:] = tuple( zip(*hgrid.nodes) )[:2]

            node_x_var.units = 'degrees_east'
            node_y_var.units = 'degrees_north'

            dataset.variables['SCHISM_hgrid_face_x'].units = 'degrees_east'
            dataset.variables['SCHISM_hgrid_face_y'].units = 'degrees_north'
            dataset.variables['SCHISM_hgrid_edge_x'].units = 'degrees_east'
            dataset.variables['SCHISM_hgrid_edge_y'].units = 'degrees_north'

        # gets variable if it exists or creates it otherwise
        var_ = lambda name, *args, **kwargs: dataset.variables.get(name) or \
                                   dataset.createVariable(name, *args, **kwargs)

        triangle_dim_name = 'nSCHISM_hgrid_triangle'
        if triangle_dim_name not in dataset.dimensions:
            dataset.createDimension(triangle_dim_name, 3)

        triangles = var_('SCHISM_hgrid_triangles', 'i',
                    ('nSCHISM_hgrid_face', 'nSCHISM_hgrid_triangle'), zlib=True)

        faces = dataset.variables['SCHISM_hgrid_face_nodes']

        triangles.setncatts({
            key: faces.getncattr(key)
                for key in faces.ncattrs() if not key.startswith('_')
        })
        triangles[...] = faces[:, :3]

        dataset.variables['SCHISM_hgrid'].setncattr('face_node_connectivity',
                                                       'SCHISM_hgrid_triangles')

        vlevels = var_('nSCHISM_vgrid_layers', 'i1', ('nSCHISM_vgrid_layers',) )
        vlevels[:] = netCDF4.numpy.arange(
                                dataset.dimensions['nSCHISM_vgrid_layers'].size)
        vlevels.setncatts(
            dict(
                units = 'level',
                positive = 'up',
            )
        )

        def create_var(name, dims, std_name, zlib=True, **attrs):
            var = var_(name, 'f', dims, zlib=zlib)
            var.setncatts(
                dict(
                    standard_name = std_name,
                    mesh = "SCHISM_hgrid",
                    location = 'node',
                    coordinates = 'SCHISM_hgrid_node_x SCHISM_hgrid_node_y',
                )
            )
            var.setncatts(attrs)

            return var

        hvel_dims = ('time', 'nSCHISM_hgrid_node', 'nSCHISM_vgrid_layers')

        hvel_x = create_var('hvel_x', hvel_dims, 'eastward_sea_water_velocity')
        hvel_y = create_var('hvel_y', hvel_dims, 'northward_sea_water_velocity')

        hvel = dataset.variables['hvel']
        hvel_x[...] = hvel[..., 0]
        hvel_y[...] = hvel[..., 1]

        wwm_dims = ('time', 'nSCHISM_hgrid_node')

        wsh_mag = dataset.variables.get('WWM_1')
        wsh_deg = dataset.variables.get('WWM_9')

        if wsh_mag and wsh_deg:

            wsh_x = create_var('wsh_x', wwm_dims, 
                      'eastward_sea_surface_wave_significant_height', units='m')
            wsh_y = create_var('wsh_y', wwm_dims,
                     'northward_sea_surface_wave_significant_height', units='m')

            wsh_rad = netCDF4.numpy.deg2rad(wsh_deg[:]+180.)
            wsh_x[:] = wsh_mag * netCDF4.numpy.sin(wsh_rad)
            wsh_y[:] = wsh_mag * netCDF4.numpy.cos(wsh_rad)

        tp = dataset.variables.get('WWM_3')
        if tp:
            tp_ = create_var('tp', wwm_dims, 'sea_surface_wind_wave_period',
                                                                      units='s')
            tp_[:] = tp[:]

        for name, factor in fib_factor_map.items():
            dataset.variables[name][:] *= factor

if __name__ == '__main__':
    from sys import argv
    adapt_to_ncwms(*argv[1:])
