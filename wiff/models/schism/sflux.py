from functools import partial
from pathlib import Path


# add logging trait
class Sflux:

    class Dataset:

        def __init__(self, provider_class, dataset_name, **kwargs):
            self._provider_class = provider_class
            self._dataset_name = dataset_name

            self.dataset = provider_class.datasets[dataset_name]
            self.kwargs = kwargs

        def updated_kwargs(self, **map):
            return type(self)(self._provider_class, self._dataset_name, 
                                                    **dict(self.kwargs, **map) )

        @property
        def partial(self):
            return partial(self._provider_class, self._dataset_name, 
                                                                  **self.kwargs)

        @property
        def timestep(self):
            return self.dataset.time_resolution

        def __str__(self) -> str:
            return f'{self._provider_class.__name__}${self._dataset_name}' \
                                                               f'#{self.kwargs}'


    def __init__(self, first:Dataset, *, date_time, period, extent, path,
                                              second:Dataset=None, logger=None):

        # self.logger =

        # self._first_pd = first
        # self._second_pd = second

        self._common = dict(
            date_time = date_time,
            extent = extent,
            work_path = Path(path),
            logger_root = logger,
        )

        self._first_obj = first.partial(
            **self._common,
            period = period + 2*first.timestep,
            create_work_path = True,
        )

        self._second_obj = second.partial(
            **self._common,
            period = period + 2*second.timestep,
        ) if second else None

    def create_air(self, index='0001'):
        self._first_obj.create_sflux_air(name=f'sflux_air_1.{index}.nc')
        if self._second_obj:
            self._second_obj.create_sflux_air(name=f'sflux_air_2.{index}.nc')

    def create_radiation(self, index='0001'):
        self._first_obj.create_sflux_rad(name=f'sflux_rad_1.{index}.nc')
        if self._second_obj:
            self._second_obj.create_sflux_rad(name=f'sflux_rad_2.{index}.nc')

    def create_precipitation(self):
        return NotImplemented

    def create_inputs(self, first_details={}, second_details={}):
        self._common['work_path'].joinpath(
            'sflux_inputs.txt'
        ).write_text(
            '\n'.join( (
                "&sflux_inputs",
                "/",
                "",
            ) )
        )

    def create_legacy_inputs(self):
        self._common['work_path'].joinpath('sflux_inputs.txt').write_text(
            self._common['date_time'].strftime(
                '\n'.join( (
                    "&sflux_inputs",
                    "  start_year  = %Y,",
                    "  start_month = %m,",
                    "  start_day   = %d,",
                    "  start_hour  = %H.,",
                    "  utc_start   = 00.,",
                    "/",
                    "",
                ) )
            )
        )

    def close(self):
        self._first_obj.close()
        if self._second_obj:
            self._second_obj.close()


if __name__ == '__main__':

    from datetime import datetime, date, time, timedelta
    from sys import argv

    from wiff.forcings.providers.tecnico.maretec import MARETEC

    first = Sflux.Dataset(MARETEC, 'sinergea_fc_regional',
                                   user='sinergea', password='T6p7xBPxgwG2u88V')
    second = Sflux.Dataset(MARETEC, 'sinergea_fc_albufeira',
                                   user='sinergea', password='T6p7xBPxgwG2u88V')

    path, year, month, day, hours = argv[1:]

    sflux = Sflux(
        first = Sflux.Dataset(MARETEC, 'sinergea_fc_albufeira',
                                  user='sinergea', password='T6p7xBPxgwG2u88V'),
        second = Sflux.Dataset(MARETEC, 'sinergea_fc_regional',
                                  user='sinergea', password='T6p7xBPxgwG2u88V'),
        date_time = datetime.combine( date( int(year), int(month), int(day) ),
                                                                      time(0) ),
        period = timedelta( hours=int(hours) ),
        extent = dict(
            lon = (-180., 180.),
            lat = (-90., 90.),
        ),
        path = path,
    )
    sflux.create_air()
    sflux.create_inputs()
