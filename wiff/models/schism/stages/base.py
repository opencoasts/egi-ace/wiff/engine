from datetime import datetime, date, time, timedelta
from pathlib import Path

from wiff.engine.parts import Stage
from wiff.engine.mixins.file_handler import FileHandler
from wiff.engine.mixins.process_handler import ProcessHandler


class SCHISMStage(Stage, FileHandler, ProcessHandler):

    simulation_kind = Stage.Input()

    datetime_today = lambda: datetime.combine(date.today(), time() )
    date_time = Stage.Input(datetime_today)

    default_period = timedelta(hours=24)
    period = Stage.Input(default_period)

    series_interval = Stage.Input(optional=True)
