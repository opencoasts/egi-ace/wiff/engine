from itertools import chain

from .base import SCHISMStage


class Cleanup0(SCHISMStage):

    xz_bin = SCHISMStage.Input('/usr/bin/xz')
    tar_bin = SCHISMStage.Input('/usr/bin/tar')

    do_remove = SCHISMStage.Input(True)
    do_compress = SCHISMStage.Input(True)
    do_archive = SCHISMStage.Input(False)

    remove_archived_files = SCHISMStage.Input(True)

    extra_remove_list = SCHISMStage.Input(tuple)
    extra_compress_list = SCHISMStage.Input(tuple)
    extra_archive_map = SCHISMStage.Input(dict)

    _info_files = (
        '*.bin',
        'fort.33',
        'fort.5002',
    )
    _input_files = (
        'hotstart.in',
        'wwm_hotstart.nc',
        'inputs.tar.gz',
    )
    _output_files = (
        'outputs/?_00??_*.6?',
        'outputs/*_00??_hotstart',
        # 'outputs/?_*.6?',
        'outputs/?_????.nc',
        'outputs/?_wwm_?.nc',
        'outputs.tar.gz',
    )
    _output_info_files = (
        'outputs/local_to_global_00??',
        'outputs/global_to_local.prop',
        'outputs/nonfatal_00??',
        'outputs/maxdahv_00??',
        'outputs/maxelev_00??',
        'outputs/windbg*_00??',
        'outputs/wwmdbg*_00??',
        'outputs/wwmstat*_00??',
        'outputs/combine_output.in',
        'outputs/fort.98',
    )
    remove_list = (
        *_info_files,
        *_input_files,
        *_output_files,
        *_output_info_files,
    )

    compress_list = (
        'outputs/?_*.6?',
        'outputs/*_hotstart.in',
        'outputs/wwm_hotstart.nc',
    )
    compress_xz_args = '--compress --threads=1 --verbose'.split()

    archive_map = {
        'logs.tar.xz': ('fort.11', 'mirror.out', 'outputs/mirror.out',
                                                         'outputs/fatal.error'),
        'confs.tar.xz': ('param.in', 'wwminput.nml'),
        'forcings.tar.xz': ('bctides.in', '*D.th', 'sflux/', 'ww3_spec.nc')
    }
    archive_tar_args = \
                      '--create --auto-compress --dereference --verbose'.split()

    def execute(self):
        if self.do_remove:
            self.remove()

        if self.do_compress:
            self.compress()

        if self.do_archive:
            self.archive()

    def remove(self):

        def is_recursive_(file_pattern):
            is_recursive = '**' in file_pattern

            if is_recursive:
                self.logger.warning(
                    '** recursive patterns are no allowed and will be skipped!')

            return is_recursive

        final_remove_list = (*self.remove_list, *self.extra_remove_list)

        files_to_remove = filter(
            lambda file: not file.is_symlink(),
                   chain.from_iterable( map(self.path.glob, final_remove_list) )
        )

        return tuple( map(self.remove_file, files_to_remove) )

    def compress(self):
        final_compress_list = (*self.compress_list, *self.extra_compress_list)

        for compress_pattern in final_compress_list:
            files_to_compress = tuple( self.path.glob(compress_pattern) )

            if not files_to_compress:
                self.logger.debug('nothing to compress for %s',
                                                               compress_pattern)

            else:
                self.logger.info('compressing %s', compress_pattern)
                self.run_executable(self.xz_bin, *self.compress_xz_args,
                                                             *files_to_compress)

    def archive(self):
        self.logger.debug("ARCHIVE MAP: %s", self.archive_map)
        final_archive_map = dict(self.archive_map, **self.extra_archive_map)

        for archive_file, archive_patterns in final_archive_map.items():
            files_to_archive = tuple( 
                chain.from_iterable(map(self.path.glob, archive_patterns))
            )
            relative_files_to_archive = tuple( file_path.relative_to(self.path)
                                             for file_path in files_to_archive )

            if not relative_files_to_archive:
                self.logger.debug('nothing to archive for %s', archive_patterns)

            else:
                self.logger.info('archiving %s into %s', archive_patterns,
                                                                   archive_file)
                self.run_executable(self.tar_bin, *self.archive_tar_args,
                           f'--file={archive_file}', *relative_files_to_archive)


class Cleanup0v58(Cleanup0):

    do_compress_netcdf = SCHISMStage.Input(True)

    extra_compress_netcdf_list = SCHISMStage.Input(tuple)

    nccopy_executable = SCHISMStage.Input('/usr/bin/nccopy')

    _input_files = (
        'hotstart.nc',
        'wwm_hotstart.nc',
        'inputs.tar.gz',
    )
    _output_part_files = (
        'outputs/hotstart_0???_*.nc',
        'outputs/schout_0???_*.nc',
        'outputs.tar.gz',
    )
    _output_info_files = (
        'outputs/coriolis.out',
        'outputs/flux.out',
        'outputs/global_to_local.prop',
        'outputs/JCG.out',
        'outputs/local_to_global_0???',
        'outputs/maxdahv_00??',
        'outputs/maxelev_00??',
        'outputs/nonfatal_00??',
        'outputs/subcycling.out',
        'outputs/total*.out',
        'outputs/windbg*_00??',
        'outputs/wwmdbg*_00??',
        'outputs/wwmstat*_00??',
    )
    remove_list = (
        *Cleanup0._info_files,
        *_input_files,
        *_output_part_files,
        *_output_info_files,
    )

    compress_netcdf_list = (
        #'outputs/hotstart_it=*.nc',
        #'outputs/wwm_hotstart.nc',
        #'outputs/schouts_*.nc',
    )
    compress_nccopy_args = ('-s', '-d1')

    compress_list = ()

    archive_map = {
        'logs.tar.xz': ('outputs/fatal.error', 'outputs/mirror.out'),
        'confs.tar.xz': ('param.nml', 'wwminput.nml', 'outputs/param.out.nml'),
        'forcings.tar.xz': ('bctides.in', 'sflux/', 'ww3_spec.nc', '*D.th.nc'),
    }

    def compress(self):
        self.compress_netcdf()

        super().compress()

    def compress_netcdf(self):
        final_compress_list = (*self.compress_netcdf_list,
                                               *self.extra_compress_netcdf_list)

        for compress_pattern in final_compress_list:
            files_to_compress = tuple( self.path.glob(compress_pattern) )

            if not files_to_compress:
                self.logger.debug('nothing to compress for %s',
                                                               compress_pattern)
                continue

            for file in files_to_compress:
                self.logger.info('compressing netcdf %s', file)
                self.run_executable(self.nccopy_executable,
                         *self.compress_nccopy_args, file, f'{file}-compressed')
                self.move_file(f'{file}-compressed', file)


if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.DEBUG)

    from sys import argv
    path, = argv[1:]

    from pathlib import Path
    Cleanup0('test', path=Path(path)).execute()
