from itertools import chain
from pathlib import Path

from wiff.data import netcdf_ugrid

#engine.core.parts import Stage
#engine.mixins import FileHandler, ProcessHandler

from .base import SCHISMStage
from ..common import SimulationKind


class Combine0(SCHISMStage):

    tracer_count = SCHISMStage.Input(optional=True)
    simulation_path = SCHISMStage.Input(optional=True)
    series_path = SCHISMStage.Input(optional=True)

    output_files = SCHISMStage.Output()
    sixties_paths = SCHISMStage.Output()
    netcdf_paths = SCHISMStage.Output()
    netcdf_ugrid_paths = SCHISMStage.Output()
    simulation_latest_run_path = SCHISMStage.Output()
    series_latest_run_path = SCHISMStage.Output()

    output_files_map = {
        SimulationKind.BAROTROPIC: ('elev.61', 'hvel.64'),
        SimulationKind.BAROCLINIC: ('elev.61', 'temp.63', 'salt.63', 'zcor.63',
                                                                     'hvel.64'),
        SimulationKind.BAROTROPIC_WAVES: ('elev.61', 'hvel.64', 'wwm_1.61',
                                                        'wwm_7.61', 'wwm_9.61'),
    }

    def execute(self):
        self.combine_hotstarts()
        self.combine_outputs()
        self.produce_netcdfs()
        self.produce_ugrid_netcdfs()
        self._link_latest_run()

    # def _thread_pool(self, ):
    #     with multiprocessing.pool.ThreadPool(processes=pool_size) as pool:

    #         for deployment in deployment_set:

    #             args = (deployment, run_date)

    #             logger.debug(' > apply_async(%s, %s, %s)',
    #                            self.launch_deployment_simulations, args, kwargs)
    #             pool.apply_async(self.launch_deployment_simulations, args,
    #                                                                      kwargs)

    #             #pool.wait(self.concurrent_wait.total_seconds() )
    #             if concurrent:
    #                 sleep(self.concurrent_wait.total_seconds() )

    #         pool.close()
    #         pool.join()

    combine_hotstart_exec = 'combine_hotstart6'

    # TODO: implement check argument
    def combine_hotstarts(self, check=True):
        outputs_path = self.path / 'outputs'
        hotstart_in_path = outputs_path / 'hotstart.in'

        for first_part_path in outputs_path.glob('*_0000_hotstart'):

            part_index, _ = first_part_path.name.split('_0000_hotstart')
            part_paths = tuple(
                              outputs_path.glob(f'{part_index}_????_hotstart') )

            optional_ = lambda name, value: \
                                      (name, value) if value is not None else ()

            args = (
                '--iteration', part_index,
                '--nproc', len(part_paths),

                *optional_('--ntracer', self.tracer_count),
            )
            self.run_executable(self.combine_hotstart_exec, *args,
                                                 cwd=outputs_path, verbose=True)

            iteration_path = outputs_path / f'{part_index}_hotstart.in'
            hotstart_in_path.rename(iteration_path)

            combined_names = ', '.join(
                             sorted( f'{path.name!r}' for path in part_paths ) )
            self.logger.debug("combined %s into %r @ '%s'", combined_names,
                                              iteration_path.name, outputs_path)

    combine_output_exec = 'combine_output7'
    empty_threshold_size = 0

    def combine_outputs(self):
        outputs_path = self.path / 'outputs'

        def part_indexes(output_file):
            for path in outputs_path.glob(f'*_0000_{output_file}'):
                if path.stat().st_size > self.empty_threshold_size:
                    yield path.name.split('_')[0]

        self.output_files = self.output_files_map[self.simulation_kind]

        def combine_parts(output_file):
            self.logger.debug("combining %s output parts...", output_file)

            combine_output_in_text = '\n'.join( (
                '{file_name}',
                '1 {last_part} ! first, last part',
                '{tracer_count} ! tracer count',
            ) ).format(
                file_name = output_file,
                last_part = max( part_indexes(output_file) ),
                tracer_count = self.tracer_count or 0,
            )

            self.create_text_file('outputs/combine_output.in',
                                                         combine_output_in_text)
            self.run_executable(self.combine_output_exec, cwd=outputs_path,
                                                                   verbose=True)

            combined_parts = tuple( outputs_path.glob(f'?_{output_file}') )
            self.logger.debug("combined output parts:\n * %s",
                                       '\n * '.join(map(str, combined_parts) ) )
            return combined_parts

        self.sixties_paths = tuple( chain.from_iterable(
                                       map(combine_parts, self.output_files) ) )
        
        sixties_names = ', '.join( path.name for path in self.sixties_paths )
        self.logger.info("produced %s @ %s", sixties_names, outputs_path)

    selfe2netcdf_bin = 'selfe2netcdf'

    def produce_netcdfs(self):

        # bin_path = self.bin_path / 'selfe2netcdf'

        def convert_to_netcdf(sixty_path):

            self.run_executable(self.selfe2netcdf_bin, sixty_path.name,
                                            cwd=sixty_path.parent, verbose=True)

            netcdf_path = sixty_path.with_suffix('.nc')
            self.logger.info("produced netcdf file %s", netcdf_path)

            return netcdf_path

        self.netcdf_paths = tuple( map(convert_to_netcdf, self.sixties_paths) )

    def produce_ugrid_netcdfs(self, sources_suffix='elev.nc',
                           outputs_suffix='all-ugrid.nc', remove_sources=False):

        elev_source_nc_paths = tuple(
            path for path in map(str, self.netcdf_paths)
                                                if path.endswith(sources_suffix)
        )

        def convert_to_ugrid(nc_path):

            nc_ugrid_path = nc_path.replace(sources_suffix, outputs_suffix)
            hgrid_geographic = self.path / 'hgrid.ll'
            netcdf_ugrid.convert(nc_path, nc_ugrid_path, hgrid_geographic,
                                                                 remove_sources)

            extra_info = " (source .nc files removed)" if remove_sources else ""
            info_msg = f"produced ugrid compliant netcdf file %s{extra_info}"
            self.logger.info(info_msg, repr(nc_ugrid_path) )

            return Path(nc_ugrid_path)

        self.netcdf_ugrid_paths = tuple(
                                   map(convert_to_ugrid, elev_source_nc_paths) )

    def _link_latest_run(self, force=True):

        if self.simulation_path:
            self.simulation_latest_run_path = \
                                     self.simulation_path / self.latest_run_link
            self.link_file(self.path, self.simulation_latest_run_path,
                                                                    force=force)

        if self.series_path:
            self.series_latest_run_path = \
                                         self.series_path / self.latest_run_link
            self.link_file(self.path, self.series_latest_run_path, force=force)


import re


class Combine1(SCHISMStage):

    series_path = SCHISMStage.Input(optional=True)
    simulation_path = SCHISMStage.Input(optional=True)

    wet_dry_output_option = SCHISMStage.Input(0)
    output_variables = SCHISMStage.Input(tuple)

    hotstart_executable = SCHISMStage.Input('combine_hotstart',
                                            alias='combine_hotstart_executable')
    output_executable = SCHISMStage.Input('combine_output',
                                              alias='combine_output_executable')

    output_files = SCHISMStage.Output()
    hotstart_files = SCHISMStage.Output()

    def execute(self):
        self._outputs_path = self.path / 'outputs'

        self.set_output('hotstart_files', self._combine_hotstarts() )
        self.logger.info('hotstarts combined')

        self.set_output('output_files', self._combine_outputs() )
        self.logger.info('outputs combined')

    _hotstart_part_prefix = 'hotstart_0000'
    _hotstart_prefix_name = 'hotstart_it='

    def _combine_hotstarts(self):
        part_indexes = self._parts_indexes(self._hotstart_part_prefix)

        for index in part_indexes:
            self.run_executable(
                self.hotstart_executable, 
                f'--iteration={index}',
                cwd=self._outputs_path, verbose=True
            )
        
        combined_paths = tuple(
                self._outputs_path.glob(f'{self._hotstart_prefix_name}*.nc') )
        self.logger.debug("combined hotstart files:\n * %s",
                                       '\n * '.join(map(str, combined_paths) ) )
        return combined_paths

    _output_part_prefix = 'schout_0000'
    _output_prefix_name = 'schouts'

    def _combine_outputs(self):
        part_indexes = self._parts_indexes(self._output_part_prefix)

        if part_indexes:
            self.run_executable(
                self.output_executable, 
                f'--begin={min(part_indexes)}',
                f'--end={max(part_indexes)}',
                f'--wetdry={self.wet_dry_output_option}',
                f"--vars={','.join(self.output_variables)}",
                f'--output={self._output_prefix_name}',
                cwd=self._outputs_path, verbose=True
            )
        
            combined_paths = tuple(
                   self._outputs_path.glob(f'{self._output_prefix_name}_*.nc') )
            self.logger.debug("combined output files:\n * %s",
                                       '\n * '.join(map(str, combined_paths) ) )
            return combined_paths

    def _parts_indexes(self, prefix):
        parts_glob_pattern = f'{prefix}_*.nc'
        parts = self._outputs_path.glob(parts_glob_pattern)

        parts_regex_pattern = parts_glob_pattern.replace('*', r'(?P<index>\d+)')
        parts_regex_match = lambda path: \
            int( re.match(parts_regex_pattern, path.name).groupdict()['index'] )

        return tuple( map(parts_regex_match, parts) )
