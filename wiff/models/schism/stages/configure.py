from .base import SCHISMStage
from ..data.bctides import BCTidesIn


class Configure0(SCHISMStage):

    param_in = SCHISMStage.Input(dict)
    bctides_in = SCHISMStage.Input(BCTidesIn)

    def execute(self):
        self._format_param_in()
        self.bctides_in.to_file(self.path)
        self.logger.info('created %s/bctides.in', self.path)

    def _format_param_in(self):
        template_path = self.path / 'param.in.template'
        formatted = template_path.read_text().format_map(self.param_in)

        self.create_text_file('param.in', formatted)
        self.logger.info('formatted param.in from %s', template_path)


from ..data.param import ParamNml


class Configure1(SCHISMStage):

    param_nml = SCHISMStage.Input(ParamNml)
    bctides_in = SCHISMStage.Input(BCTidesIn)

    def execute(self):
        self.param_nml.to_file(self.path)
        self.logger.info('created %s/param.nml', self.path)

        self.bctides_in.to_file(self.path)
        self.logger.info('created %s/bctides.in', self.path)
