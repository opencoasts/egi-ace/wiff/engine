import json
from datetime import timedelta
from functools import partial

from wiff.core import utils
from wiff.forcings.providers.copernicus.cmems import \
    CMEMSPhysics2D as copernicus_CMEMSPhysics2D, \
    CMEMSPhysics3D as copernicus_CMEMSPhysics3D
from wiff.forcings.providers.meteofrance.dcpc import DCPC as meteofrance_DCPC
from wiff.forcings.providers.meteogalicia.mandeo import Mandeo as \
                                                             meteogalicia_Mandeo
from wiff.forcings.providers.noaa.nomads import NOMADS as noaa_NOMADS
from wiff.forcings.providers.tecnico.maretec import Sflux as tecnico_MARETEC

from .base import SCHISMStage
from ..common import CommonName, SimulationKind, BoundaryKind, ForcingSource, \
                                                                     ForcingKind
from ..sflux import Sflux
from ..data import bctides


class Forcings0(SCHISMStage):

    bbox = SCHISMStage.Input()
    forcings = SCHISMStage.Input()
    boundaries = SCHISMStage.Input()

    param_in = SCHISMStage.Input(dict, output=True)
    vertical_levels_count = SCHISMStage.Input(optional=True)

    fes2014_exec = SCHISMStage.Input(default='fes2014.sh')
    motuclient_exec = SCHISMStage.Input(default='motuclient')
    motuclient_config = SCHISMStage.Input(optional=True)
    maretec_ftp_user = SCHISMStage.Input(optional=True)
    maretec_ftp_password = SCHISMStage.Input(optional=True)

    #ocean_boundaries_files = SCHISMStage.Output()
    bctides_in = SCHISMStage.Output()

    def execute(self):
        self.timestep = timedelta(seconds=self.param_in['dt'])
        self.is_baroclinic = \
                         self.simulation_kind in SimulationKind.BAROCLINIC_KINDS

        self.resolve_ocean_boundary_elev_forcings()
        if self.is_baroclinic:
            self.resolve_ocean_boundary_salt_temp_forcings()

        self._resolve_atmospheric_forcings()

        # self._format_bctides_in()
        self._create_bctides_in()


    class CMEMSDomain:

        GLOBAL = 'global'
        IBI = 'ibi'


    # TODO: add explicit no_forcings and disallow KeyError exception
    def resolve_ocean_boundary_elev_forcings(self):
        # TODO: make it reusable
        forcing_resolver_map = {
            ForcingSource.PRISM_2017:
                              self._resolve_ocean_elev_forcings_using_prism,
            ForcingSource.FES_2014:
                            self._resolve_ocean_boundary_forcings_using_fes2014,
            ForcingSource.CMEMS_GLOBAL: partial(
                self._resolve_ocean_boundary_elev_forcings_using_cmems,
                                                       self.CMEMSDomain.GLOBAL),
            ForcingSource.CMEMS_IBI: partial(
                self._resolve_ocean_boundary_elev_forcings_using_cmems,
                                                          self.CMEMSDomain.IBI),
        }

        try:
            forcing = self.forcings[CommonName.ForcingKind.OCEAN_ELEV]
        except KeyError:
            self.logger.info('No ocean elevation forcing defined!')
        else:
            forcing_resolver_map[forcing]()

    # TODO: add explicit no_forcings and disallow KeyError exception
    def resolve_ocean_boundary_salt_temp_forcings(self):
        forcing_resolver_map = {
            ForcingSource.CMEMS_GLOBAL: partial(
                self._resolve_ocean_boundary_salt_temp_forcings_using_cmems,
                                                       self.CMEMSDomain.GLOBAL),
            ForcingSource.CMEMS_IBI: partial(
                self._resolve_ocean_boundary_salt_temp_forcings_using_cmems,
                                                          self.CMEMSDomain.IBI),
        }

        try:
            forcing = self.forcings[CommonName.ForcingKind.OCEAN_SALT_TEMP]
        except KeyError:
            pass
        else:
            forcing_resolver_map[forcing]()

    prism_dir = 'prism'

    @SCHISMStage.cache_files('elev2D.th')
    def _resolve_ocean_elev_forcings_using_prism(self):

        current_date = self.date_time.date()
        previous_date = current_date - timedelta(days=1)

        prism_file = lambda date, filename: f'{date:%Y-%j}/run/{filename}'
        prism_nesting_files = (
            ( '1_elev.61', prism_file(previous_date, 'outputs/1_elev.61') ),
            ( '2_elev.61', prism_file(current_date, 'outputs/1_elev.61') ),
            ( '3_elev.61', prism_file(current_date, 'outputs/2_elev.61') ),
            ( 'bg.gr3', prism_file(current_date, 'hgrid.gr3') ),
        )

        for link_name, suffix_path in prism_nesting_files:

            prism_file_path = self.static_path / self.prism_dir / suffix_path
            self.link_file(prism_file_path, link_name)

        # the remaining funcionality was handled by genBCschism.sh script

        enumerated_boundaries = enumerate(self.boundaries, start=1)
        is_ocean_ = lambda boundary: boundary['kind'] == BoundaryKind.OCEAN

        boundaries_index, boundaries_node_count = zip( *(
            (index, boundary['node_count']) for index, boundary
                                 in enumerated_boundaries if is_ocean_(boundary)
        ) )

        boundary_count = len(boundaries_index)
        boundaries_indexes_listing = utils.listing(boundaries_index)

        gen_fg_in = utils.format_lines(
            '{boundary_count}'
                  ' !total # of open boundary segments to be included in fg.bp',
            '{boundaries_index} !list of open boundary segment IDs',

            boundary_count = boundary_count,
            boundaries_index = boundaries_indexes_listing,
        )
        self.create_text_file(self.path/'gen_fg.in', gen_fg_in)
        self.run_executable('gen_fg')

        self.link_file('hgrid.ll', 'fg.gr3')
        self.link_file('vgrid.in', 'vgrid.fg')

        period_days = utils.days_in_(self.period)
        interval_days = utils.days_in_(self.series_interval)
        period_extended_days = period_days + interval_days

        interpolate_variables_in_file_type_dict = dict(
            elev2D = 1,
            salt3D = 2,
            temp3D = 2,
            uv3D = 3,
        )
        interpolate_variables_in = utils.format_lines(
            '{file_type} {length_days} ! file_type: {file_type_dict}; days',
            '{boundary_count} {boundaries_index}'
                                          ' ! boundary count; boundary indexes',
            '{debug:0} ! debug: 0=off, 1=on',

            length_days = period_extended_days,
            file_type = interpolate_variables_in_file_type_dict['elev2D'],
            file_type_dict = interpolate_variables_in_file_type_dict,
            boundary_count = boundary_count,
            boundaries_index = boundaries_indexes_listing,
            debug = False,
        )
        self.create_text_file(self.path/'interpolate_variables.in',
                                                       interpolate_variables_in)
        self.run_executable('interpolate_variables6')

        self.move_file('elev2D.th', 'th.old')

        header_template = utils.format_lines(
            '{{length_days}} 1 ! length days, number of vertical levels',
            '{boundary_count} {boundaries_node_count}'
                              ' ! ocean boundaries, node count in each of them',

            boundary_count = boundary_count,
            boundaries_node_count = utils.listing(boundaries_node_count),
        )

        slash_th_in = utils.format_lines(
            header_template.format(length_days=period_extended_days),
            '{interval_days} !number of days to slash from the beginning',

            interval_days = interval_days
        )
        self.create_text_file(self.path/'slash_th.in', slash_th_in)

        self.run_executable('slash_th')

        timeint_in = utils.format_lines(
            header_template.format(length_days=period_days),
            '{timestep} ! timestep',
            # '{vertical_offset} ! PRISM vertical offset',

            timestep = int( self.timestep.total_seconds() ),
            # vertical_offset = context.get('vertical_offset') or 0.
        )
        self.create_text_file(self.path/'timeint.in', timeint_in)

        #self.rename_local_file(path, 'th.new', 'th.old')

        self.run_executable('timeint_th', 'th.new', 'elev2D.th')

        #self.rename_local_file(path, 'th.new', 'elev2D.th')

        #self.ocean_boundaries_files = prism_nesting_files

    _bctides_fes_name ='bctides.in-fes'

    @SCHISMStage.cache_files(_bctides_fes_name, 'uv3D.th')
    def _resolve_ocean_boundary_forcings_using_fes2014(self):
        self._execute_fes2014()

    _fes2014_date_time_fmt = '%Y-%m-%dT%H:%M:%SZ'

    def _execute_fes2014(self, *extra_args):

        format_ = lambda d_t: d_t.strftime(self._fes2014_date_time_fmt)

        def formatted_boundaries():
            for boundary in self.boundaries:
                flow_details = "" if boundary['kind'] == BoundaryKind.OCEAN \
                                           else f":{boundary[ForcingKind.FLOW]}"
                yield f"{boundary['kind']}{flow_details}"

        self.run_executable(self.fes2014_exec,
            #'-conf', self.static_path / 'slev_fes2014.ini',
            '--tstart', format_(self.date_time),
            '--tend', format_(self.date_time+self.period),
            #'--deltaT', self.timestep.total_seconds(),
            '--type', 'gr3',
            '--bnd', 'hgrid.ll',
            '--bnds_specs', *formatted_boundaries(),
            '--levels', str(self.vertical_levels_count or 2),
            '--velocities',
            '--out', self._bctides_fes_name,
            '--json',
            *extra_args,
        )

    queued_cmems = utils.QueuedExecution('cmems')

    @SCHISMStage.cache_files('elev2D.th')
    @queued_cmems
    def _resolve_ocean_boundary_elev_forcings_using_cmems(self, domain):
        cmems = \
               self._cmems_forcings_partial(copernicus_CMEMSPhysics2D, domain)()
        try:
            cmems.create_elev2D_th()
        finally:
            cmems.close()

    @SCHISMStage.cache_files('SAL_3D.th', 'TEM_3D.th')
    @queued_cmems
    def _resolve_ocean_boundary_salt_temp_forcings_using_cmems(self, domain):
        cmems = self._cmems_forcings_partial(copernicus_CMEMSPhysics3D, domain)(
                                        depth_levels=self.vertical_levels_count)
        try:
            cmems.create_SAL_3D_th()
            cmems.create_TEM_3D_th()
        finally:
            cmems.close()

    _cmems_domain_dataset_map = {
        CMEMSDomain.GLOBAL: 'global_af',
        CMEMSDomain.IBI: 'ibi_af',
    }

    # _cmems_domain_dataset_map = {
    #     CMEMSDomain.GLOBAL:  {
    #         CommonName.ForcingKind.OCEAN_ELEV: 'global_af',
    #         CommonName.ForcingKind.OCEAN_SALT_TEMP: 'global_af'
    #     },
    #     CMEMSDomain.IBI: {
    #         CommonName.ForcingKind.OCEAN_ELEV: 'ibi_ap',
    #         CommonName.ForcingKind.OCEAN_SALT_TEMP: 'ibi_af'
    #     },
    # }

    def _cmems_forcings_partial(self, provider, domain):
        return partial(provider,
            self._cmems_domain_dataset_map[domain],

            date_time = self.date_time,
            period = self.period,
            extent = self.bbox,

            boundaries = self.boundaries,
            timestep = self.timestep,

            work_path = self.path,
            utils_dir = self.bin_path,
            templates_dir = self.static_path,
            elev_offset_nc = self.static_path / 'GLO-MFC_001_024_mdt.nc',
            motuclient_path = self.motuclient_exec,
            motuclient_config_path = self.motuclient_config,

            logger_root = self.logger,
        )

    _base_atmospheric_param_in_settings = dict(
        inv_atm_bnd = int(False),
        nws = 0, # no atmospheric forcings
        wtiminc = 0,
    )

    _inverse_barometer_forcings = {
        ForcingSource.FES_2014,
    }

    def _resolve_atmospheric_forcings(self):
        self.param_in.update(self._base_atmospheric_param_in_settings)

        atmos = self.forcings.get(CommonName.ForcingKind.ATMOSPHERIC)
        if not atmos or atmos == ForcingSource.NO_FORCING:
            return

        fst_dataset, snd_dataset = self._atmospheric_forcing_datasets(atmos)
        self._create_sflux(fst_dataset, snd_dataset)

        # resolver = self._atmospheric_forcing_handler(atmos)

        # resolver_partial = partial(resolver,
        #     date_time = self.date_time,
        #     period = self.period + resolver.timestep(),
        #     extent = self.bbox,
        #     work_path = self.path / 'sflux',

        #     logger_root = self.logger,
        # )
        # self._create_sflux(resolver_partial)

        ocean_elev_forcing = \
                            self.forcings.get(CommonName.ForcingKind.OCEAN_ELEV)
        is_inverse_barometer = \
                          ocean_elev_forcing in self._inverse_barometer_forcings

        self.param_in.update(
            inv_atm_bnd = int(is_inverse_barometer),
            nws = 2, # sflux usage
            wtiminc = fst_dataset.timestep.total_seconds(),
        )

    # TODO: make it reusable
    _atmospheric_forcings_map = {
        ForcingSource.MARETEC_SINERGIA_REGIONAL: 
                                      (tecnico_MARETEC, 'sinergea_fc_regional'),
        ForcingSource.MARETEC_SINERGIA_ALBUFEIRA:
                                     (tecnico_MARETEC, 'sinergea_fc_albufeira'),

        ForcingSource.METEOFRANCE_ARGPEGE_EUROPE:
                                            (meteofrance_DCPC, 'arpege_europe'),
        ForcingSource.METEOFRANCE_ARGPEGE_GLOBAL:
                                            (meteofrance_DCPC, 'arpege_global'),

        ForcingSource.METEOGALICIA_WRF_WEST_EUROPE:
                                           (meteogalicia_Mandeo, 'west_europe'),
        ForcingSource.METEOGALICIA_WRF_IBERIA_BISCAY:
                                         (meteogalicia_Mandeo, 'iberia_biscay'),
        ForcingSource.METEOGALICIA_WRF_GALICIA:
                                               (meteogalicia_Mandeo, 'galicia'),

        ForcingSource.NOAA_GFS_0P25: (noaa_NOMADS, 'gfs_0p25_1hr'),
        ForcingSource.NOAA_NAM: (noaa_NOMADS, 'nam'),
        ForcingSource.NOAA_NAM_CONUS: (noaa_NOMADS, 'nam_conus'),
    }

    # def _atmospheric_forcing_handler(self, name):
    #     provider, dataset = self._atmospheric_forcings_map[name]
    #     provider_partial = partial(provider, dataset)

    #     if name == ForcingSource.MARETEC_SINERGIA_REGIONAL or \
    #                             name == ForcingSource.MARETEC_SINERGIA_ALBUFEIRA:
    #         provider_partial = partial(provider_partial,
    #                user=self.maretec_ftp_user, password=self.maretec_ftp_password)

    #     return provider_partial, provider.datasets[dataset].time_resolution

    # temporary solution until Sflux.Dataset objects are provided as inputs
    def _atmospheric_forcing_datasets(self, name):
        first_dataset = Sflux.Dataset(*self._atmospheric_forcings_map[name])
        second_dataset = None

        fs_sinergia_regional = ForcingSource.MARETEC_SINERGIA_REGIONAL
        fs_sinergia_albufeira = ForcingSource.MARETEC_SINERGIA_ALBUFEIRA

        if name in (fs_sinergia_regional, fs_sinergia_albufeira):
            first_dataset.kwargs.update(
                user = self.maretec_ftp_user,
                password = self.maretec_ftp_password,
            )

            if name == fs_sinergia_regional:
                second_dataset = Sflux.Dataset(
                    *self._atmospheric_forcings_map[fs_sinergia_albufeira],
                    **first_dataset.kwargs,
                )

        return first_dataset, second_dataset

    _sflux_index = '001'

    # @SCHISMStage.cache_files('sflux/')
    # def _create_sflux(self, resolver_partial):
    #     resolver = resolver_partial(create_work_path=True)

    #     try:
    #         resolver.create_sflux_air(
    #                           name=f'sflux_air_1.{self._sflux_1_file_index}.nc')

    #         if self.simulation_kind \
    #                             in self._radiation_requirement_simulation_kinds:
    #             resolver.create_sflux_rad(
    #                           name=f'sflux_rad_1.{self._sflux_1_file_index}.nc')

    #         resolver.create_inputs_txt()

    #     finally:
    #         resolver.close()

    @SCHISMStage.cache_files('sflux/')
    def _create_sflux(self, fst_dataset, snd_dataset=None):
        self.logger.debug("sflux datasets 1:%s 2:%s", fst_dataset, snd_dataset)

        sflux_path = self.path / 'sflux'
        resolver = Sflux(
            first = fst_dataset,
            second = snd_dataset,

            date_time = self.date_time,
            period = self.period,
            extent = self.bbox,
            path = sflux_path,

            logger = self.logger,
        )

        try:
            resolver.create_air(self._sflux_index)

            if self.is_baroclinic:
                resolver.create_radiation(self._sflux_index)

        finally:
            resolver.close()

        resolver.create_legacy_inputs()

        return resolver

    # TODO: should be a class on its own
    def _format_bctides_in(self):
        bctides_json_path = self.path / 'bctides.json'
        bctides_json = json.loads( bctides_json_path.read_bytes() ) \
                                       if bctides_json_path.exists() else dict()

        #base_date = context['date_time'].strftime('%m/%d/%Y %H:%M:%S')
        base_date = self.date_time.strftime('%Y-%m-%d %H:%M:%S')

        tidal_potential_header = \
                       '{freq_count} 0 ! nfreq tidal pot., cut-off depth for tp'
        tidal_potential_freqs = bctides_json.get('Tidal Potential') or ()
        tidal_potential = utils.format_lines(
            tidal_potential_header,
            *tidal_potential_freqs,

            freq_count = len(tidal_potential_freqs)
        )

        nodal_factors_header = '{freq_count} ! nfreq at bounds'
        nodal_factors_freqs = bctides_json.get('Nodal Factors') or ()
        nodal_factors = utils.format_lines(
            nodal_factors_header,
            *nodal_factors_freqs,

            freq_count = len(nodal_factors_freqs)
        )

        # def bctides_json_bnd_(kind):
        #     try:
        #         return bctides_json['Boundaries'][kind]
        #     except KeyError:
        #         return len(boundaries) * ( tuple(), )

        bctides_json_bnd_ = lambda kind: (
            bctides_json['Boundaries'][kind] if bctides_json
                                        else len(self.boundaries) * ( tuple(), )
        )

        bnd_elevations = bctides_json_bnd_('Elevations')
        bnd_velocities = bctides_json_bnd_('Velocities')

        boundary_header = (
            '{{node_count}} {elev_flag} {vel_flag} {temp_flag} {salt_flag}'
                     ' !{{kind}}: node count, elev, vel, temp and salt bc flags'
        )

        ocean_bc_flags = lambda bnd_index: dict(
            elev_flag = 3 if bnd_elevations[bnd_index] else 4,
            vel_flag = 5 if bnd_velocities[bnd_index] else 0,
        )
        river_bc_flags = dict(
            elev_flag = 0,
            vel_flag = 2,
        )
        barotropic_bc_flags = dict(
            temp_flag = 0,
            salt_flag = 0,
        )
        barotropic_boundary_template = {
            BoundaryKind.OCEAN: lambda bnd_index: (
                boundary_header.format(
                    **ocean_bc_flags(bnd_index),
                    **barotropic_bc_flags,
                ),
                *bnd_elevations[bnd_index],
                *bnd_velocities[bnd_index],
            ),
            BoundaryKind.RIVER: lambda _: (
                boundary_header.format(
                    **river_bc_flags,
                    **barotropic_bc_flags,
                ),
                f'{{{ForcingKind.FLOW}}} !flux',
            ),
        }
        baroclinic_boundary_template = {
            BoundaryKind.OCEAN: lambda bnd_index: (
                boundary_header.format(
                    **ocean_bc_flags(bnd_index),
                    temp_flag = 4,
                    salt_flag = 4,
                ),
                *bnd_elevations[bnd_index],
                *bnd_velocities[bnd_index],
                '1. ! temperature (TEM_3D.th) nudging factor',
                '1. ! salinity (SAL_3D.th) nudging factor',
            ),
            BoundaryKind.RIVER: lambda _: (
                boundary_header.format(
                    **river_bc_flags,
                    temp_flag = 2,
                    salt_flag = 2,

                ),
                f'{{{ForcingKind.FLOW}}} ! flux',
                f'{{{ForcingKind.TEMPERATURE}}} ! temperature',
                '1. ! temperature nudging factor',
                f'{{{ForcingKind.SALINITY}}} ! salinity',
                '1. ! salinity nudging factor',
            ),
        }
        microbiology_boundary_template = {
            BoundaryKind.OCEAN: lambda bnd_index: (
                boundary_header.format(
                    **ocean_bc_flags(bnd_index),
                    temp_flag = 4,
                    salt_flag = 4,
                ),
                *bnd_elevations[bnd_index],
                *bnd_velocities[bnd_index],
                '1. ! temperature (TEM_3D.th) nudging factor',
                '1. ! salinity (SAL_3D.th) nudging factor',
            ),
            BoundaryKind.RIVER: lambda _: (
                boundary_header.format(
                    **river_bc_flags,
                    temp_flag = 2,
                    salt_flag = 2,

                ),
                f'{{{ForcingKind.FLOW}}} ! flux',
                f'{{{ForcingKind.TEMPERATURE}}} ! temperature',
                '1. ! temperature nudging factor',
                f'{{{ForcingKind.SALINITY}}} ! salinity',
                '1. ! salinity nudging factor',
            ),
        }

        simulation_kind__boundary_templates__map = {
            SimulationKind.BAROTROPIC: barotropic_boundary_template,
            SimulationKind.BAROTROPIC_WAVES: barotropic_boundary_template,
            SimulationKind.BAROCLINIC: baroclinic_boundary_template,
            SimulationKind.BAROCLINIC_WAVES: baroclinic_boundary_template,
        }
        boundary_templates = simulation_kind__boundary_templates__map[
                                                           self.simulation_kind]
        format_boundary = lambda index, details: '\n'.join(
              boundary_templates[ details['kind'] ](index) ).format_map(details)

        open_boundaries_header = '{bnd_count} ! open boundary segments'
        open_boundaries = utils.format_lines(
            open_boundaries_header,
            *( format_boundary(*enum_details)
                               for enum_details in enumerate(self.boundaries) ),

            bnd_count = len(self.boundaries)
        )

        bctides_in_template_path = self.static_path / 'bctides.in.template'
        bctides_in = utils.format_lines(
            bctides_in_template_path.read_text(),

            BASE_DATE = base_date,
            TIDAL_POTENTIAL = tidal_potential,
            NODAL_FACTORS = nodal_factors,
            OPEN_BOUNDARIES = open_boundaries,
        )
        bctides_in_path = self.path / 'bctides.in'
        bctides_in_path.write_text(bctides_in)

        self.logger.info('formatted bctides.in from template %r',
                                                       bctides_in_template_path)

    # TODO: move to prepare
    def _create_bctides_in(self):

        def elevation_condition(boundary):
            if boundary['kind'] == BoundaryKind.OCEAN:
                return bctides.SpaceTimeVarying()

            return bctides.Nothing()

        def velocity_condition(boundary):
            if boundary['kind'] == BoundaryKind.RIVER:
                return bctides.Constant(boundary[ForcingKind.FLOW])

            return bctides.Nothing()

        def temperature_salinity_condition(boundary, forcing_kind):
            if self.is_baroclinic:

                factor = 0. if forcing_kind == ForcingKind.TEMPERATURE else 1.

                if boundary['kind'] == BoundaryKind.OCEAN:
                    return bctides.NudgedSpaceTimeVarying(inflow_factor=factor)

                else:
                    return bctides.NudgedConstant(
                        value = boundary[forcing_kind],
                        inflow_factor = factor,
                    )

            return bctides.Nothing()

        # TODO: move to prepare_fib
        def tracer_conditions(boundary):
            if self.simulation_kind in SimulationKind.MICROBIOLOGY_KINDS:
                return (
                    bctides.NudgedMicrobiologyConstants(
                        e_coli = boundary[ForcingKind.E_COLI],
                        enterococcus = boundary[ForcingKind.ENTEROCOCCUS],
                        inflow_factor = 1.,
                    ),
                )

            return ()

        bctides_in = bctides.BCTidesIn(
            # header = ,
            # tp_cut_off_depth = ,
            open_boundaries = tuple(
                bctides.OpenBoundary(
                    node_count = boundary['node_count'],
                    elevation = elevation_condition(boundary),
                    velocity = velocity_condition(boundary),
                    temperature = temperature_salinity_condition(boundary,
                                                       ForcingKind.TEMPERATURE),
                    salinity = temperature_salinity_condition(boundary,
                                                          ForcingKind.SALINITY),
                    tracers = tracer_conditions(boundary)
                ) for boundary in self.boundaries
            )
        )

        bctides_fes_path = self.path / 'bctides.in-fes'

        if bctides_fes_path.exists():
            bctides_fes = bctides.BCTidesIn.from_file(bctides_fes_path)

            bctides_in.tidal_potential = bctides_fes.tidal_potential
            bctides_in.tidal_forcing = bctides_fes.tidal_forcing

            for bnd, bc_in, bc_fes in zip(self.boundaries,
                       bctides_in.open_boundaries, bctides_fes.open_boundaries):

                if bnd['kind'] == BoundaryKind.OCEAN:
                    bc_in.elevation = bc_fes.elevation
                    bc_in.velocity = bc_fes.velocity

        self.bctides_in = bctides_in
        # self.logger.info('created %s', self.bctides_in)


from wiff.forcings.targets.schism.open_bnd import BaseOpenBoundary


class Forcings0v58(Forcings0):

    param_nml = SCHISMStage.Input(dict, output=True)

    def _create_th_nc(self, file_name, vertical_levels_count=1, **kwargs):
        th_file_path = self.path / file_name
        open_boundary_nodes = tuple(
            boundary['node_count'] for boundary in self.boundaries
                                       if boundary['kind'] == BoundaryKind.OCEAN
        )

        BaseOpenBoundary.create_th_nc(th_file_path, sum(open_boundary_nodes),
                                                vertical_levels_count, **kwargs)
        self.logger.debug(f"created %s.nc", file_name)

    @Forcings0.cache_files('elev2D.th.nc')
    def _resolve_ocean_boundary_forcings_using_prism(self):
        super()._resolve_ocean_elev_forcings_using_prism()

        self._create_th_nc('elev2D.th')

    @Forcings0.cache_files('bctides.in-fes', 'uv3D.th.nc')
    def _resolve_ocean_boundary_forcings_using_fes2014(self):
        self._execute_fes2014('--netcdf')

    @Forcings0.cache_files('elev2D.th.nc')
    def _resolve_ocean_boundary_elev_forcings_using_cmems(self, domain):
        super()._resolve_ocean_boundary_elev_forcings_using_cmems(domain)

        self._create_th_nc('elev2D.th')

    @Forcings0.cache_files('SAL_3D.th.nc', 'TEM_3D.th.nc')
    def _resolve_ocean_boundary_salt_temp_forcings_using_cmems(self, domain):
        super()._resolve_ocean_boundary_salt_temp_forcings_using_cmems(domain)

        self._create_th_nc('SAL_3D.th', self.vertical_levels_count)
        self._create_th_nc('TEM_3D.th', self.vertical_levels_count)

    _sflux_index = '0001'

    def _resolve_atmospheric_forcings(self):
        super()._resolve_atmospheric_forcings()

        self.param_nml.optional['nws'] = self.param_in['nws']
        self.param_nml.optional['wtiminc'] = self.param_in['wtiminc']
        self.param_nml.optional['inv_atm_bnd'] = self.param_in['inv_atm_bnd']

    def _create_sflux(self, *args):
        sflux = super()._create_sflux(*args)

        if sflux:
            sflux.create_inputs()
