from functools import partial
from itertools import chain

from wiff.engine.parts import simulation

from .base import SCHISMStage
from ..common import SimulationKind
from ..data.hgrid import HorizontalGrid


class PrepareFIB0(SCHISMStage):

    #bctides = SCHISMStage.Input()
    param_nml = SCHISMStage.Input()
    fib_flag = SCHISMStage.Input()
    fib_flag_1_e_coli = SCHISMStage.Input(optional=True)
    fib_flag_1_enterococcus = SCHISMStage.Input(optional=True)
    fib_sediments_fraction = SCHISMStage.Input(optional=True)
    fib_sediments_rate = SCHISMStage.Input(optional=True)
    e_coli_initial_condition = SCHISMStage.Input(optional=True)
    enterococcus_initial_condition = SCHISMStage.Input(optional=True)
    fib_sources = SCHISMStage.Input(tuple)

    def execute(self):
        if self.simulation_kind not in SimulationKind.MICROBIOLOGY_KINDS:
            self.logger.debug(
                "Simulation mode (%s) doesn't use FIB! (only %s)",
                self.simulation_kind,
                ', '.join(SimulationKind.MICROBIOLOGY_KINDS)
            )
            return

        if not (self.fib_flag and 1 <= self.fib_flag <= 5):
            raise ValueError(f"{type(self).__name__}.fib_fag MUST be 1-5!")

        hgrid = HorizontalGrid.open(self.path/'hgrid.gr3')

        if self.param_nml.hotstart:
            self._create_initial_condition_files(hgrid)

        self._create_grid_files(hgrid)
        self._handle_sources(hgrid)
        self._update_configuration()

    def _create_initial_condition_files(self, hgrid):
        self._resolve_grid_file('fib_hvar_1.ic', 'e_coli_initial_condition',
                                                                          hgrid)
        self._resolve_grid_file('fib_hvar_2.ic', 
                                        'enterococcus_initial_condition', hgrid)

    def _create_grid_files(self, hgrid):
        self._resolve_grid_file('sinkfib.gr3', 'fib_sediments_rate', hgrid)
        self._resolve_grid_file('fraction_fib.gr3', 'fib_sediments_fraction',
                                                                          hgrid)

        if self.fib_flag == 1:
            self._resolve_grid_file('kkfib_1.gr3', 'fib_flag_1_e_coli', hgrid)
            self._resolve_grid_file('kkfib_2.gr3', 'fib_flag_1_enterococcus',
                                                                          hgrid)

    # TODO: check and report more stuff, like when the file already exists and a
    # constant value is also supplied; maybe raise an exception if stricter
    def _resolve_grid_file(self, name, input_name, hgrid):
        file_path = self.path / name
        input_attr = getattr(self, input_name)

        if input_attr is None:

            if file_path.exists():
                self.logger.info("%s already exists", file_path)
            else:
                self.logger.warning("neither %s exists nor %s is supplied!", 
                                                          file_path, input_name)

        else:
            hgrid.constant_grid(input_attr).save(file_path)

    def _handle_sources(self, hgrid):
        self._resolve_source_sink_in(len(hgrid.elements), self.fib_sources)

        vsource = ( source['forcings']['flow'] for source in self.fib_sources )

        msource_baroclinic = (
            (source['forcings']['temperature'], source['forcings']['salinity'])
                                                  for source in self.fib_sources
        ) if self.simulation_kind in SimulationKind.BAROCLINIC_KINDS else ()

        msource_fib = (
            (source['forcings']['ecoli'], source['forcings']['enterococcus'])
                                                  for source in self.fib_sources
        ) if self.simulation_kind in SimulationKind.BAROCLINIC_KINDS else ()

        period = self.param_nml.period + self.param_nml.timestep

        def source_lines(values):
            values_str = ' '.join( map(str, values) )
            return (
                f"0  {values_str}",
                f"{int( period.total_seconds() )}  {values_str}",
            )

        self._write_lines_to_file('vsource.th', *source_lines(vsource) )
        self._write_lines_to_file('msource.th', *source_lines(
                          chain( *zip(*msource_baroclinic), *zip(*msource_fib) )
        ) )

    def _resolve_source_sink_in(self, elements_count, sources):
        self._write_lines_to_file('gen_source.in',
            '1',
            f'1 {elements_count}',
        )

        self._write_lines_to_file('ss_pt.bp',
            'sources location',
            len(sources),
            *( f"{index} {' '.join(source['coordinates'])} -1"
                for index, source in enumerate(sources, start=1) ),
        )

        # since gen_source expects hgrid.gr3 to use the same coordinate system
        # as the points in the ss_pt.bp file (always WGS84), hgrid.ll (always
        # WGS84) is temporarily renamed to hgrid.gr3
        self.move_file('hgrid.gr3', 'hgrid.gr3_')
        self.move_file('hgrid.ll', 'hgrid.gr3')

        self.run_executable('gen_source')

        self.move_file('hgrid.gr3', 'hgrid.ll')
        self.move_file('hgrid.gr3_', 'hgrid.gr3')

    def _write_lines_to_file(self, name, *lines):
        self.path.joinpath(name).write_text( '\n'.join( map(str, lines) ) )

    def _update_configuration(self):
        self.param_nml.optional['flag_fib'] = self.fib_flag
        if self.fib_sources:
            self.param_nml.optional['if_source'] = 1


def _main(param_nml_path):
    import logging
    from ..data.param import ParamNml

    logging.basicConfig(level='DEBUG')

    PrepareFIB0('fib_test',
        simulation_kind = SimulationKind.BAROCLINIC_MICROBIOLOGY,
        param_nml = ParamNml.from_file(param_nml_path),
        fib_flag = 1,
        fib_flag_1_e_coli = .1,
        fib_flag_1_enterococcus = .2,
        e_coli_initial_condition = 1.,
        enterococcus_initial_condition = 2.,
        fib_sediments_rate = 3.,
        fib_sediments_fraction = .4,
    ).execute()

if __name__ == '__main__':
    from sys import argv
    _main(*argv[1:])