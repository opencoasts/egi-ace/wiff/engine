import netCDF4
from datetime import datetime
from itertools import chain
from pathlib import Path

from wiff.engine.parts import Simulation
from wiff.engine.stages.local_workplace import LocalWorkplace0
from wiff.models.ww3 import stages as ww3_stages
from wiff.data.netcdf.editing import Editing as ncEditing

from .base import SCHISMStage
from ..common import SimulationKind
from ..data.hgrid import HorizontalGrid


class PrepareWWM0(SCHISMStage):

    forcings = SCHISMStage.Input()
    ww3_prepared_path_template = SCHISMStage.Input(optional=True)
    ww3_simulation_path_template = SCHISMStage.Input(optional=True)

    is_cartesian = SCHISMStage.Input(False)
    force_coldstart = SCHISMStage.Input(False)
    hotstart_path = SCHISMStage.Input(optional=True)
    wwm_hotstart_path = SCHISMStage.Input(optional=True)
    wwm_stations = SCHISMStage.Input(dict)
    wwminput_nml = SCHISMStage.Input(dict)
    # param_in = SCHISMStage.Input(dict)

    xz_bin = SCHISMStage.Input('/usr/bin/xz')
    ww3_bin_path = SCHISMStage.Input(Path.cwd)
    ww3_static_path = SCHISMStage.Input(Path.cwd)
    ww3_mpirun_args = SCHISMStage.Input(tuple)

    ww3_spectra_nodes = SCHISMStage.Output(tuple)

    static_files = dict(
        wwminput_nml_template = 'wwminput.nml.template',
    )

    def execute(self):
        if self.simulation_kind not in SimulationKind.WAVES_KINDS:
            self.logger.debug("Simulation mode (%s) doesn't use WWM! (only %s)",
                   self.simulation_kind, ', '.join(SimulationKind.WAVES_KINDS) )
            return

        hgrid_path = self.path / 'hgrid.ll'
        grid = HorizontalGrid.open(hgrid_path)

        self._handle_boundaries(grid)

        if self.forcings['wave_boundaries']:

            if self.ww3_simulation_path_template:
                ww3_simulation = datetime.strftime(self.date_time,
                                              self.ww3_simulation_path_template)
                self._join_ww3_spec( Path(ww3_simulation) )
            else:
                self._run_ww3_simulation()

        self._resolve_wwmbnd_gr3(grid)
        hotstarted = self._resolve_wwm_hotstart()
        self._format_wwminput_nml(hotstarted)

    def _handle_boundaries(self, grid:HorizontalGrid):
        boundaries_nodes = tuple( grid.boundary_nodes(index-1)
                                 for index in self.forcings['wave_boundaries'] )
        indexes, coordinates = zip( *chain.from_iterable(boundaries_nodes) )

        self.forcings.update(
            wave_boundary_indexes = indexes,
            wave_boundary_points = tuple( (x, y) for x, y ,_ in coordinates )
        )

    _ww3_spec_glob = 'ww3.????????_spec.nc'
    _wwm_spec_name = 'ww3_spec.nc'

    def _join_ww3_spec(self, ww3_spec_path):
        output_file = self.path / self._wwm_spec_name
        input_files = sorted( ww3_spec_path.glob(self._ww3_spec_glob) )

        ncEditing.join_files_along_time(output_file, input_files,
            exclude_attrs=(
                'variables_time_axis_map',
                'southernmost_latitude',
                'northernmost_latitude',
                'latitude_resolution',
                'westernmost_longitude',
                'easternmost_longitude',
                'longitude_resolution',
                'minimum_altitude',
                'maximum_altitude',
                'altitude_resolution',
                'start_date',
                'stop_date',
            )
        )

        self.logger.info("WW3's wave spectra %s merged from %s", output_file,
                                                                    input_files)

    @SCHISMStage.cache_files(_wwm_spec_name)
    def _run_ww3_simulation(self):

        ww3_context = Simulation('_ww3',
            _logger_root = self.logger,

            stage_context = dict(
                date_time = self.date_time,
                period = self.period,

                bin_path = self.ww3_bin_path,
                static_path = self.ww3_static_path,
            ),
            stage_templates = (
                LocalWorkplace0.template('workplace',
                    root_path = self.path,
                ),
                ww3_stages.BindPrepared.template('bind',
                    prepared_path = datetime.strftime(self.date_time,
                                               self.ww3_prepared_path_template),
                ),
                # TODO: get boundary points from hgrid instead of context
                ww3_stages.Compute.template('compute',
                    mpirun_args = self.ww3_mpirun_args,
                    output_points = self.forcings['wave_boundary_points'],
                ),
                ww3_stages.Outputs.template('outputs',
                ),
            ),
        ).run()

        if ww3_context.get('_stages_failed'):
            raise Exception('WW3 simulation failed!')

        self._join_ww3_spec(ww3_context['path'])

    def _resolve_wwmbnd_gr3(self, grid:HorizontalGrid):
        wwmbnd = grid.wwm_boundary(self.forcings['wave_boundaries'])

        wwmbnd_path = self.path / 'wwmbnd.gr3'
        wwmbnd.save(wwmbnd_path)

        self.logger.info("WWM's boundary grid %s created", wwmbnd_path)

    _wwm_hotstart_name = 'wwm_hotstart.nc'

    def _resolve_wwm_hotstart(self):
        coldstated_msg = "COLDSTARTED WWM simulation run!"
        hotstarted_msg = "HOTSTARTED WWM simulation run (%s)!"

        if self.force_coldstart:
            self.logger.warning("Forced " + coldstated_msg)
            return False

        if self.wwm_hotstart_path:
            self.logger.info(hotstarted_msg, self.wwm_hotstart_path)
            return True

        else:
            if not self.hotstart_path:
                self.logger.info(coldstated_msg)
                return False

            self.wwm_hotstart_path = \
                           self.hotstart_path.with_name(self._wwm_hotstart_name)

        try:
            self.link_file(self.wwm_hotstart_path, self._wwm_hotstart_name)

        except FileNotFoundError: # maybe it's compressed
            wwm_hotstart_xz_path = self.wwm_hotstart_path.with_suffix('.nc.xz')
            wwm_hotstart_name = f'{self._wwm_hotstart_name}.xz'
            self.link_file(wwm_hotstart_xz_path, wwm_hotstart_name)

            xz_args = '--decompress --keep --force --verbose'.split()
            self.run_executable(self.xz_bin, *xz_args, wwm_hotstart_name)

        self.logger.info(hotstarted_msg, self.wwm_hotstart_path)
        return True

    def _format_wwminput_nml(self, hotstarted):
        template_path = self.link_static_file('wwminput_nml_template')
        wwminput_nml_path = template_path.with_suffix('')

        begin = self.date_time
        end = begin + self.period

        station_names = map(repr, self.wwm_stations.keys() )
        station_x_coords, station_y_coords = \
             zip( *self.wwm_stations.values() ) if self.wwm_stations else (), ()

        wwminput_nml_path.write_text(
            template_path.read_text().format(
                BEGTC = begin,
                ENDTC = end,
                PROC__LSPHE = 'F' if self.is_cartesian else 'T',
                # PROC__DELTC = int(self.param_in['dt']) *
                #                                int(self.param_in['nstep_wwm']),
                PROC__DELTC = self.wwminput_nml['PROC_DELTC'],
                INIT__LHOTR = 'T' if hotstarted else 'F',
                ENGS__BRHD = self.wwminput_nml['ENGS_BRHD'],
                # PROC__BEGTC = wwm_begin,
                # PROC__ENDTC = wwm_end,
                # BOUC__BEGTC = wwm_begin,
                # BOUC__ENDTC = wwm_end,
                # WIND__BEGTC = wwm_begin,
                # WIND__ENDTC = wwm_end,
                # HISTORY__BEGTC = wwm_begin,
                # HISTORY__ENDTC = wwm_end,
                # STATION__BEGTC = wwm_begin,
                # STATION__ENDTC = wwm_end,
                STATION__DELTC = self.wwminput_nml['PROC_DELTC'],
                STATION__OUTSTYLE = repr('NC' if self.wwm_stations else 'NO'),
                STATION__IOUTS = len(self.wwm_stations),
                STATION__NOUTS = ', '.join(station_names),
                STATION__XOUTS = ', '.join( map(str, station_x_coords) ),
                STATION__YOUTS = ', '.join( map(str, station_y_coords) ),
                # HOTFILE__BEGTC = wwm_begin,
                # HOTFILE__ENDTC = wwm_end,
            )
        )

        self.logger.info("WWM's configuration %s formated from %s",
                                               wwminput_nml_path, template_path)


class PrepareWWM0v58(PrepareWWM0):

    param_nml = SCHISMStage.Input()

    _wwminput_nml_hgrid_file = 'hgrid_WWM.gr3'
    _wwminput_nml_datetime_fmt = '%Y%m%d. %H%M%S'
    _wwminput_nml_input_wave_file = 'ww3_spec.nc'
    _wwminput_nml_boundaries_file = 'wwmbnd.gr3'
    _wwminput_nml_stations_output_file = 'outputs/wwm_stations'

    def _format_wwminput_nml(self, hotstarted):
        nml = self.wwminput_nml

        nml['PROC']['LSPHE'] = self.param_nml.coordinate_system == \
                                self.param_nml.CoordinateSystemOption.GEOGRAPHIC

        self.link_file('hgrid.gr3', self._wwminput_nml_hgrid_file)
        nml['GRID']['FILEGRID'] = self._wwminput_nml_hgrid_file

        nml['BOUC']['FILEWAVE'] = self._wwminput_nml_input_wave_file
        nml['BOUC']['FILEBOUND'] = self._wwminput_nml_boundaries_file

        nml['INIT']['LHOTR'] = hotstarted

        begin = self.date_time
        end = begin + self.period
        begin_fmt = self.date_time.strftime(self._wwminput_nml_datetime_fmt)
        end_fmt = end.strftime(self._wwminput_nml_datetime_fmt)

        for group in 'PROC', 'BOUC', 'HOTFILE':
            nml[group]['BEGTC'] = begin_fmt
            nml[group]['ENDTC'] = end_fmt

        if self.wwm_stations:
            station_x_coords, station_y_coords = zip(
                                                   *self.wwm_stations.values() )
            station_group = dict(
                OUTSTYLE = 'NC',
                FILEOUT = self._wwminput_nml_stations_output_file,
                BEGTC = begin_fmt,
                DELTC = nml['PROC']['DELTC'],
                UNITC = nml['PROC']['UNITC'],
                ENDTC = end_fmt,
                IOUTS = len(self.wwm_stations),
                NOUTS = list( self.wwm_stations.keys() ),
                XOUTS = list(station_x_coords),
                YOUTS = list(station_y_coords),
            )
            for name, value in station_group.items():
                nml['STATION'][name] = value

        nml_path = self.path / 'wwminput.nml'
        nml.write(nml_path)
        self.logger.info("WWM's configuration saved to %s", nml_path)
