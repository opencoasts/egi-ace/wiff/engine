import logging
from datetime import datetime, timedelta
from pathlib import Path
from typing import Iterable, Sequence, Union

_logger = logging.getLogger(__name__)


class TextInterface:

    logger = _logger.getChild(__qualname__)

    def __init__(self, timestep:timedelta, nodes:Sequence[str],
         title:str='SWMM5', description:str='', concentraions:Sequence[str]=()):

        self.title = title
        self.description = description
        self.timestep = timestep
        self.concentrations = tuple(concentraions)
        self.nodes = tuple(nodes)
        self.series_map = { node: list() for node in self.nodes }

    def get_series(self, node:str) -> Sequence:
        return self.series_map[node]

    def set_series(self, node:str, series:Sequence):
        self.series_map[node] = series
        return self


class TextInterfaceReader:

    logger = _logger.getChild(__qualname__)

    def __init__(self, lines:Iterable[str]):
        self.lines = lines
        self._parse_lines()

    @classmethod
    def from_file(cls, path:Path):
        '''creates new instance from the file defined in `path`'''
        def file_lines_iter():
            with path.open() as file:
                for line in file:
                    yield line
        return cls(file_lines_iter())

    def _parse_lines(self):
        '''parses every line in `self.lines`'''
        self.title = self._parse_line(separator=None)
        self.description = self._parse_line(separator=None)
        # self.timestep = timedelta(
        #     seconds = self._parse_count_line('reporting time step in sec')
        # )
        # self.constituents = self._parse_names_block(
        #                               'number of constituents as listed below:')
        # self.nodes = self._parse_names_block(
        #                               'number of constituents as listed below:')
        self.timestep = timedelta(seconds=self._parse_count_line())
        self.constituents = self._parse_names_block()
        self.nodes = self._parse_names_block()
        self.node_series_map = self._parse_nodes_timeseries()

    def _parse_data(self, line, separator:Union[str,None]) -> str:
        if separator is None:
            return line.strip()

        data, is_separated, remaining = line.partition(separator)
        return data.strip()

    def _parse_line(self, separator:Union[str,None]='#') -> str:
        return self._parse_data(next(self.lines), separator)

    def _parse_count_line(self, separator:str='-') -> int:
        return int(self._parse_line(separator))

    # def _parse_count_line(self, perfix_match:str, separator:str='-') -> int:
    #     header_line = self._parse_line()
    #     count, separated, remaning = header_line.partition(separator)

    #     if not separated:
    #         raise ValueError(f"separator '{separator}' not found"
    #                                   f" in block header line '{header_line}'!")
    #     if not remaning.starts_with(perfix_match):
    #         raise ValueError(f"text does NOT start with '{perfix_match}'"
    #                                   f" in block header line '{header_line}'!")

    #     return int(count)

    def _parse_names_block(self) -> Sequence[str]:
        count = self._parse_count_line()
        return tuple( self._parse_line() for _ in range(count) )

    def _parse_nodes_timeseries(self, separator:str='#'):
        header_line = self._parse_line()
        # hkind, hyear, hmonth, hday, hhour, hminute, hsecond, *hvalues = \
        #                     ( column.strip() for column in header_line.split() )

        series_map = { node: list() for node in self.nodes }
        for line in self.lines:
            node, year, month, day, hour, minute, second, *values = \
                                       self._parse_data(line, separator).split()
            timestamp = datetime(
                            *map(int,(year, month, day, hour, minute, second)) )
            flow, *concentrations = tuple(map(float, values))

            series_map[node].append( (timestamp, flow, tuple(concentrations)) )

        return series_map

    def get_overflow_nodes(self, threshold:float=0.) -> Sequence[str]:
        def flow_in_(series):
            for timestamp, flow, _ in series:
                yield flow

        return tuple( node for node, series in self.node_series_map.items()
                                          if max(flow_in_(series)) > threshold )

    def __str__(self) -> str:
        return self.title

    def __repr__(self) -> str:
        return f"'{self}' / timestep={self.timestep}" \
            f"; constituents={','.join(self.constituents)}" \
            f"; nodes={','.join(self.nodes)}"


if __name__ == '__main__':
    from sys import argv
    file_path, = argv[1:]
    swmm_flow = TextInterfaceReader.from_file(Path(file_path))
    print(f"overflow: {', '.join(swmm_flow.get_overflow_nodes())}\n{swmm_flow!r}")
