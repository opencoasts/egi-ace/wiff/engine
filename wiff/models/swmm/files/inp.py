import logging
from abc import ABC, abstractmethod
from datetime import date, datetime, time, timedelta
from enum import Enum
from functools import partial, total_ordering, wraps
from pathlib import Path
from typing import Any, Iterable, Mapping, Tuple, Union

_logger = logging.getLogger(__name__)

def _echo(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        # self may be an instance or a class (from a classmethod)
        kind = self if type(self) is type(object) else type(self)
        self.logger.debug("%s.%s(%s %s)",
                                     kind.__name__, func.__name__, args, kwargs)

        return func(self, *args, **kwargs)

    return wrapper


class _Section(ABC):

    logger = _logger.getChild(__qualname__)

    def __init__(self, name:str):
        self.name = name.upper()

    # @abstractmethod
    # def check(self, **kwargs):
    #     '''should check the correctness and return self'''

    @abstractmethod
    def render(self):
        '''should produce an interable of strings'''

        return f'[{self.name}]'

    _comment_prefix = ';'

    @classmethod
    @abstractmethod
    # def parse(self, lines, **kwargs):
    def parse(self, lines:Iterable[str]):
        '''should produce a mapping to be used by build to call __init__'''

        for line in lines:
            line = line.strip()

            if line.startswith(self._comment_prefix):
                continue

            yield line

    # @classmethod
    # def build(self, lines, *, init_kwargs=dict(), **kwargs):
    #     return self(**self.parse(lines, **kwargs), **init_kwargs)

    @classmethod
    def build(self, lines, **kwargs):
        return self(**self.parse(lines), **kwargs)

    def __repr__(self):
        return f'Section [{self.name}]'

    def __str__(self):
        return '\n'.join(self.render())


class GenericSection(_Section):

    @_echo
    def __init__(self, name:str, text:str):
        super().__init__(name)
        self.text = text.strip()

    def render(self):
        yield super().render()
        yield self.text

    @classmethod
    def parse(self, lines):
        return dict(
            text = '\n'.join(super().parse(lines))
        )


class Title(GenericSection):

    @_echo
    def __init__(self, text:str):
        super().__init__('TITLE', text)


class _SettingsSection(_Section):

    class _EnumValue(Enum):
        def __str__(self):
            return str(self.value)

    class BooleanValue:

        _values_map = dict(
            YES = True,
            TRUE = True,
            NO = False,
            FALSE = False,
        )

        def __init__(self, value:Union[bool, str]):
            self.value = value if isinstance(value, bool) \
                                            else self._values_map[value.strip()]

        def __str__(self):
            return 'YES' if self.value else 'NO'

    @total_ordering
    class _ComparableValue:
        def __eq__(self, other):
            return self.value == other.value
        def __lt__(self, other):
            return self.value < other.value

    class _DateTimeValue(_ComparableValue):

        def __init__(self, value:Union[datetime, time, date, str], format:str):
            self.value = value if isinstance(value, (datetime, time, date)) \
                                           else datetime.strptime(value, format)
            self.format = format

        def __str__(self):
            return self.value.strftime(self.format)

    TimeValue = partial(_DateTimeValue, format='%H:%M:%S')
    DateValue = partial(_DateTimeValue, format='%m/%d/%Y')
    YearlyDateValue = partial(_DateTimeValue, format='%m/%d')

    class StepValue(_ComparableValue):

        def __init__(self, value:Union[timedelta, str],
                                                   allow_decimal_seconds=False):
            value_parts = value.strip().split(':')

            if allow_decimal_seconds and len(value_parts) == 1:
                seconds, = value_parts
                self.value = timedelta(seconds=float(seconds))
                self.decimal_seconds = True
            else:
                hours, minutes, seconds = map(int, value_parts)
                self.value = timedelta(hours=hours, minutes=minutes,
                                                                seconds=seconds)
                self.decimal_seconds = False

        def __str__(self):
            return f'{self.value.total_seconds()}' if self.decimal_seconds \
                                                            else str(self.value)

    DecimalStepValue = partial(StepValue, allow_decimal_seconds=True)

    # class _NumberValue:

    #     def __init__(self, kind, value:Union[float, int, str]):
    #         self.value = kind(value)

    #     def __str__(self):
    #         return str(self.value)

    # IntValue = partial(_NumberValue, int)
    # FloatValue = partial(_NumberValue, float)
    IntValue = int
    FloatValue = float

    # maybe move this kind of check to the owner/container and not on the option
    # itself; besides value ranging, it'd allow simple post-init cross checking
    class ValueRange:

        def __init__(self, entry_class, *, min=None, max=None):
            self.entry_class = entry_class
            self.min = min
            self.max = max

        def __call__(self, value:Any):
            option = self.entry_class(value)

            if self.min is not None and option < self.min:
                raise ValueError(f'{value} must be >= than {self.min}')
            if self.max is not None and option > self.max:
                raise ValueError(f'{value} must be <= than {self.max}')

            return option

    NonNegativeIntValue = ValueRange(IntValue, min=0)
    NonNegativeFloatValue = ValueRange(FloatValue, min=0.0)

    valid_options = dict()

    @_echo
    @abstractmethod
    def __init__(self, name, entries):
        super().__init__(name)
        self.entries = entries

    def render(self):
        yield super().render()
        for key, value in self.entries.items():
            yield f'{key:<30} {value}'

    @classmethod
    def _parse_item(self, line:str):
        key, space, value = line.strip().partition(' ')
        return key.strip(), self.valid_options[key](value.strip())

    @classmethod
    def parse(self, lines):
        return dict( map(self._parse_item, super().parse(lines)) )


class Options(_SettingsSection):

    class FlowUnitsValue(_SettingsSection._EnumValue):
        CFS = 'CFS'
        GPM = 'GPM'
        MGD = 'MGD'
        CMS = 'CMS'
        LPS = 'LPS'
        MLD = 'MLD'

    class InfiltrationValue(_SettingsSection._EnumValue):
        HORTON = 'HORTON'
        MODIFIED_HORTON = 'MODIFIED_HORTON'
        GREEN_AMPT = 'GREEN_AMPT'
        MODIFIED_GREEN_AMPT = 'MODIFIED_GREEN_AMPT'
        CURVE_NUMBER = 'CURVE_NUMBER'

    class FlowRoutingValue(_SettingsSection._EnumValue):
        STEADY = 'STEADY'
        KINWAVE = 'KINWAVE'
        DYNWAVE = 'DYNWAVE'

    class LinkOffsetsValue(_SettingsSection._EnumValue):
        DEPTH = 'DEPTH'
        ELEVATION = 'ELEVATION'

    class ForceMainEquationValue(_SettingsSection._EnumValue):
        H_W = 'H-W'
        D_W = 'D-W'

    class InertialDampingValue(_SettingsSection._EnumValue):
        NONE = 'NONE'
        PARTIAL = 'PARTIAL'
        FULL = 'FULL'

    class NormalFlowLimitedValue(_SettingsSection._EnumValue):
        SLOPE = 'SLOPE'
        FROUDE = 'FROUDE'
        BOTH = 'BOTH'
        
    class SurchargeMethodValue(_SettingsSection._EnumValue):
        EXTRAN = 'EXTRAN'
        SLOT = 'SLOT'

    valid_options = dict(
        FLOW_UNITS = FlowUnitsValue,
        INFILTRATION = InfiltrationValue,
        FLOW_ROUTING = FlowRoutingValue,
        LINK_OFFSETS = LinkOffsetsValue,
        FORCE_MAIN_EQUATION = ForceMainEquationValue,
        IGNORE_RAINFALL = _SettingsSection.BooleanValue,
        IGNORE_SNOWMELT = _SettingsSection.BooleanValue,
        IGNORE_GROUNDWATER = _SettingsSection.BooleanValue,
        IGNORE_RDII = _SettingsSection.BooleanValue,
        IGNORE_ROUTING = _SettingsSection.BooleanValue,
        IGNORE_QUALITY = _SettingsSection.BooleanValue,
        ALLOW_PONDING = _SettingsSection.BooleanValue,
        SKIP_STEADY_STATE = _SettingsSection.BooleanValue,
        SYS_FLOW_TOL = _SettingsSection.FloatValue,
        LAT_FLOW_TOL = _SettingsSection.FloatValue,
        START_DATE = _SettingsSection.DateValue,
        START_TIME = _SettingsSection.TimeValue,
        END_DATE = _SettingsSection.DateValue,
        END_TIME = _SettingsSection.TimeValue,
        REPORT_START_DATE = _SettingsSection.DateValue,
        REPORT_START_TIME = _SettingsSection.TimeValue,
        SWEEP_START = _SettingsSection.YearlyDateValue,
        SWEEP_END = _SettingsSection.YearlyDateValue,
        DRY_DAYS = _SettingsSection.NonNegativeFloatValue,
        REPORT_STEP = _SettingsSection.StepValue,
        RULE_STEP = _SettingsSection.StepValue,
        WET_STEP = _SettingsSection.StepValue,
        DRY_STEP = _SettingsSection.StepValue,
        ROUTING_STEP = _SettingsSection.DecimalStepValue,
        LENGTHENING_STEP = _SettingsSection.DecimalStepValue,
        VARIABLE_STEP = _SettingsSection.ValueRange(
                               _SettingsSection.NonNegativeFloatValue, max=2.0),
        MINIMUM_STEP = _SettingsSection.NonNegativeFloatValue,
        INERTIAL_DAMPING = InertialDampingValue,
        NORMAL_FLOW_LIMITED = NormalFlowLimitedValue,
        SURCHARGE_METHOD = SurchargeMethodValue,
        MIN_SURFAREA = _SettingsSection.NonNegativeFloatValue,
        MIN_SLOPE = _SettingsSection.ValueRange(
                            _SettingsSection.NonNegativeFloatValue, max=99.999),
        MAX_TRIALS = _SettingsSection.NonNegativeIntValue,
        HEAD_TOLERANCE = _SettingsSection.FloatValue,
        THREADS = _SettingsSection.NonNegativeIntValue,
    )

    def __init__(self, **entries):
        super().__init__('OPTIONS', entries)


class Report(_SettingsSection):

    class EntityFilter:

        class EntityFilterValue(_SettingsSection._EnumValue):
            ALL = 'ALL'
            NONE = 'NONE'

        def __init__(self, filter:Union[EntityFilterValue, Tuple[str], str]):
            if isinstance(filter, self.EntityFilterValue):
                self.filter = filter
            elif isinstance(filter, tuple):
                self.filter = filter
            else:
                try:
                    self.filter = self.EntityFilterValue(filter)
                except:
                    self.filter = tuple(filter.split())

        def __str__(self):
            return ' '.join(self.filter) if isinstance(self.filter, tuple) \
                                                           else str(self.filter)

    valid_options = dict(
        DISABLED = _SettingsSection.BooleanValue,
        INPUT = _SettingsSection.BooleanValue,
        CONTINUITY = _SettingsSection.BooleanValue,
        FLOWSTATS = _SettingsSection.BooleanValue,
        CONTROLS = _SettingsSection.BooleanValue,
        SUBCATCHMENTS = EntityFilter,
        NODES = EntityFilter,
        LINKS = EntityFilter,
        LID = str, #Name Subcatch Fname
    )

    def __init__(self, **entries):
        super().__init__('REPORT', entries)

    @classmethod
    def _parse_item(self, line:str):
        key, value = super()._parse_item(line)

        if key == 'LID':
            name, space, details = value.partition(' ')
            key = f'{key}_{name.strip()}'
            value = details.strip()
            return key, details
        
        return key, value

    def render(self):
        for line in super().render():
            if line.startswith('LID'):
                yield line.replace('_', ' ', 1)
            else:
                yield line


class Files(_SettingsSection):

    valid_options = dict(
        USE_RAINFALL = Path,
        SAVE_RAINFALL = Path,
        USE_RUNOFF = Path,
        SAVE_RUNOFF = Path,
        USE_HOTSTART = Path,
        SAVE_HOTSTART = Path,
        USE_RDII = Path,
        SAVE_RDII = Path,
        USE_INFLOWS = Path,
        SAVE_OUTFLOWS = Path,
    )

    def __init__(self, **entries):
        super().__init__('FILES', entries)

    @classmethod
    def _parse_item(self, line:str):
        action, space, value = line.strip().partition(' ')
        kind, space, path = value.strip().partition(' ')
        return f'{action.strip()}_{kind.strip()}', path.strip()

    def render(self):
        for line in super().render():
            yield line.replace('_', ' ', 1)


if __name__ == '__main__':
    title = Title.build(
        lines = (
            ';;Project Title/Notes',
            '',
            'asdasd',
            '',
            ';asdasd',
            'zxcxvxcv',
            '',
        )
    )
    print(str(title))

    options = Options.build(
        (
            ';;Option             Value',
            'FLOW_UNITS           CMS',
            'INFILTRATION         CURVE_NUMBER',
            'FLOW_ROUTING         DYNWAVE',
            'LINK_OFFSETS         DEPTH',
            'MIN_SLOPE            0',
            'ALLOW_PONDING        YES',
            'SKIP_STEADY_STATE    NO',

            'IGNORE_RAINFALL      YES',
            'START_DATE           11/01/2015',
            'START_TIME           00:00:00',
            'REPORT_START_DATE    11/01/2015',
            'REPORT_START_TIME    00:00:00',
            'END_DATE             11/01/2015',
            'END_TIME             06:00:00',
            'SWEEP_START          01/01',
            'SWEEP_END            12/31',
            'DRY_DAYS             0',
            'REPORT_STEP          0:05:00',
            'WET_STEP             00:01:00',
            'DRY_STEP             00:01:00',
            'ROUTING_STEP         0:00:05 ',
            'RULE_STEP            00:00:00',

            'INERTIAL_DAMPING     NONE',
            'NORMAL_FLOW_LIMITED  SLOPE',
            'FORCE_MAIN_EQUATION  H-W',
            'VARIABLE_STEP        0.75',
            'LENGTHENING_STEP     1',
            'MIN_SURFAREA         1.14',
            'MAX_TRIALS           8',
            'HEAD_TOLERANCE       0.0015',
            'SYS_FLOW_TOL         5',
            'LAT_FLOW_TOL         5',
            'MINIMUM_STEP         0.5',
            'THREADS              16',
        )
    )
    print(str(options))

    report = Report.build(
        (
            'DISABLED NO',
            'INPUT NO',
            'CONTINUITY YES',
            'FLOWSTATS YES',
            'CONTROLS YES',
            'SUBCATCHMENTS ALL',
            'NODES NONE',
            'LINKS A B C D',
            'LID A sA fA',
            'LID B sB fB',
        )
    )
    print(str(report))

    files = Files.build(
        (
            'USE RAINFALL /use/rainfall',
            'SAVE RAINFALL /save/rainfall',
            'USE RUNOFF /use/runoff',
            'SAVE RUNOFF /save/runoff',
            'USE HOTSTART /use/hotstart',
            'SAVE HOTSTART /save/hotstart',
            'USE RDII /use/rdii',
            'SAVE RDII /save/rdii',
            'USE INFLOWS /use/inflows',
            'SAVE OUTFLOWS /save/outflows',
        )
    )
    print(str(files))

    subcatchments = GenericSection.build(
        (
            ';;Name           Rain Gage        Outlet           Area     %Imperv  Width    %Slope   CurbLen  SnowPack        ',
            ';;-------------- ---------------- ---------------- -------- -------- -------- -------- -------- ----------------',
            'BC1.1            Precip_Albufeira CN.22.16         2.84     95       129      14.5     0                        ',
            'BC1.2            Precip_Albufeira CN.22.14         2.02     95       99       17.1     0                        ',
            'BC2              Precip_Albufeira CN.21_275        0.39     100      39       1.0      0                        ',
            'BC3              Precip_Albufeira CN.20_139        1.38     50       66       19.0     0                        ',
            'BC4.1            Precip_Albufeira CN.18.8          7.55     85       210      6.9      0                        ',
            'BC4.2            Precip_Albufeira CN.18.18         7.38     90       205      9.7      0                        ',
            'BC4.3            Precip_Albufeira CxB01            1.55     100      78       12.5     0                        ',
            'BC4.4            Precip_Albufeira CN.18.6.4        5.12     85       160      15.6     0                        ',
            'BC5              Precip_Albufeira 64               0.28     100      71       37.5     0                        ',
        ),
        name='SUBCATCHMENTS'
    )
    print(str(subcatchments))