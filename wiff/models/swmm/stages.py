from datetime import timedelta
from pathlib import Path

from wiff.engine.parts import Stage
from wiff.engine.mixins.file_handler import FileHandler
from wiff.engine.mixins.process_handler import ProcessHandler


class SWMMStage0(Stage, FileHandler):

    date_time = Stage.Input()
    period = Stage.Input()

    _latest_run_link = '_latest'


class Prepare0(SWMMStage0):

    input_file = SWMMStage0.Input('inpfile')
    force_coldstart = SWMMStage0.Input(False)

    series_interval = SWMMStage0.Input(optional=True)
    simulation_series_path_template = SWMMStage0.Input(optional=True)

    def execute(self):
        is_hotstarted = self._resolve_hotstart()
        self._render_input_file(is_hotstarted)


    def _resolve_hotstart(self):
        coldstart_msg = "COLDSTARTED!!! [reason: %s]"

        if self.force_coldstart:
            self.logger.warning(coldstart_msg, 'forced')
            return False

        if not (self.series_interval and self.simulation_series_path_template):
            self.logger.warning(coldstart_msg, 'no series')
            return False

        path_fn = lambda date_time: Path(
            date_time.strftime(self.simulation_series_path_template),
            self._latest_run_link,
            self._output_state_folder,
        )

        file_path = self.lookup_hotstart(self.date_time,
                                     self.series_interval, self.period, path_fn)

        if not file_path:
            self.logger.warning(coldstart_msg, 'no hotstart available')
            return False

        self.link_file(file_path, self._input_state_folder)
        self.logger.info("HOTSTARTED!!! [%s]", file_path)
        return True

    def _render_input_file(self, hotstart=False):

        def render_template(static_name, **tags):
            path = Path(self.static_files[static_name])
            text = self.read_text_file(path)
            self.create_text_file(path.with_suffix(''), text, **tags)

        render_template('model',
            START = self.date_time,
            END = self.date_time + self.period,
        )

        for name in ('basin', 'drainage_network', 'porous_media', 'run_off',
                                                                  'vegetation'):
            render_template(name,
                CONTINUOUS = int(continuous)
            )



class Compute0(SWMMStage0, ProcessHandler):

    swmm_land_bin = SWMMStage0.Input('swmm')
    input_file = SWMMStage0.Input('inpfile')
    report_file = SWMMStage0.Input('rptfile')
    output_file = SWMMStage0.Input('outfile')

    def execute(self):
        self.run_executable('/bin/bash', f'{self.bin_path}/shim.sh',
            f'{self.bin_path}/{self.mohid_land_bin}',
            '--config', f'{self.config_file}',
        )
