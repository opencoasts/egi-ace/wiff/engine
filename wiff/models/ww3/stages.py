from datetime import timedelta, date, time, datetime
from pathlib import Path

from wiff.engine.parts import Stage
from wiff.engine.mixins.file_handler import FileHandler
from wiff.engine.mixins.process_handler import ProcessHandler


class WW3Stage(Stage, FileHandler, ProcessHandler):

    default_period = timedelta(hours=48)
    datetime_today = lambda: datetime.combine( date.today(), time() )

    path = Stage.Input(Path.cwd)
    period = Stage.Input(default_period)
    date_time = Stage.Input(datetime_today)

    output_point_prefix = 'spec'
    output_point_timestep = timedelta(hours=1)

    def format_ww3_inp_template(self, name, **tags):
        ww3_inp_template_path = self.static_path / f'{name}.template'
        ww3_inp_template = ww3_inp_template_path.read_text()

        ww3_inp_path = self.path / name
        ww3_inp_path.write_text( ww3_inp_template.format(**tags) )

    def run_ww3_bin(self, name, mpi=False):
        bin_path = self.bin_path / name
        out_path = self.path / f'{name}.out'

        run_method = self.run_executable_mpi if mpi else self.run_executable

        with out_path.open('w') as out_file:
            run_method(bin_path, check=True, verbose=False, stdout=out_file,
                                                                stderr=out_file)


from wiff.core import utils


class Prepare(WW3Stage):

    force_coldstart = WW3Stage.Input(False)
    series_interval = WW3Stage.Input(optional=True)
    simulation_series_path_template = WW3Stage.Input(optional=True)

    latest_run_link = Stage.Input(optional=True)

    static_files = dict(
        grid_inp = 'ww3_grid.inp',
        mod_def = 'mod_def.ww3',
        strt_inp = 'ww3_strt.inp',
        mesh = 'OPENCoastS_NA.msh',
    )

    def execute(self):
        # self._resolve_local_static() # temporary

        self._resolve_model_definition()
        self._bind_restart_file() or self._resolve_coldstart()

    def _resolve_model_definition(self):
        try:
            self.link_static_file('mod_def')

        except FileNotFoundError:
            self.logger.debug("%s model definition file not found",
                                                   self.static_files['mod_def'])

            self.link_static_file('grid_inp')
            self.link_static_file('mesh')
            self.run_ww3_bin('ww3_grid')

    def _bind_restart_file(self):
        coldstart_msg = "COLDSTARTED!!! [reason: %s]"

        if self.force_coldstart:
            self.logger.warning(coldstart_msg, 'forced')
            return

        if not (self.series_interval and self.simulation_series_path_template):
            self.logger.warning(coldstart_msg, 'no series')
            return

        restart_timedeltas = utils.timedelta_range(self.period,
                                                     delta=self.series_interval)
        restart_datetimes = ( self.date_time - restart_td
                                          for restart_td in restart_timedeltas )

        for index, date_time in enumerate(restart_datetimes, start=1):

            restart_path = Path(
                date_time.strftime(self.simulation_series_path_template),
                self.latest_run_link, f'restart{index:03}.ww3',
            )
            self.logger.debug('checking restart file from %s at %r',
                                                        date_time, restart_path)

            if restart_path.is_file():
                self.link_file(restart_path, 'restart.ww3', resolve=True)

                self.logger.info('HOTSTARTED from %s at %r', date_time,
                                                                   restart_path)
                return restart_path

        self.logger.warning(coldstart_msg, 'no restart file(s) available')

    def _resolve_coldstart(self):
        self.link_static_file('strt_inp')
        self.run_ww3_bin('ww3_strt')


from datetime import time

from wiff.forcings.providers.noaa.nomads import NOMADS as noaa_NOMADS

from wiff.core.utils import QueuedExecution


class Forcings(WW3Stage):

    extent = WW3Stage.Input()

    static_files = dict(
        prnc_inp = 'ww3_prnc.inp',
    )

    def execute(self):
        self._fetch_gfs_0p25_1hr_wind()
        self._resolve_inputs()

    @QueuedExecution('gfs_wind')
    def _fetch_gfs_0p25_1hr_wind(self):
        gfs = noaa_NOMADS('gfs_0p25_1hr',
            date_time = self.date_time,
            work_path = self.path,
            period = self.period,
            extent = self.extent,
        )
        try:
            gfs.create_sflux_wind()
        finally:
            gfs.close()

    def _resolve_inputs(self):
        # self._resolve_local_static() # temporary
        self.link_static_file('prnc_inp')
        self.run_ww3_bin('ww3_prnc')


from pathlib import Path

from wiff.engine.parts import Stage
from wiff.engine.mixins.file_handler import FileHandler


class BindPrepared(Stage, FileHandler):

    path = Stage.Input(Path.cwd)
    prepared_path = Stage.Input()

    prepared_files = (
        'mod_def.ww3',
        'restart.ww3',
        'wind.ww3',
    )

    def execute(self):
        self._link_prepared_files()

    local_prepared_dir = '_prepared'

    def _link_prepared_files(self):
        prepared_local_path = self.path / self.local_prepared_dir

        try:
            self.link_file(self.prepared_path, prepared_local_path,
                                                                 relative=False)
        # except FileNotFoundError:
        #     self.logger.exception()
        except FileExistsError:
            pass

        for prepared_file in map(Path, self.prepared_files):
            local_prepared_file_path = prepared_local_path / prepared_file
            self.link_file(local_prepared_file_path)


import json
from datetime import timedelta
from itertools import chain
from math import ceil


class Compute(WW3Stage):

    output_points = WW3Stage.Input(tuple)
    # output_fields = WW3Stage.Input(tuple, output=True)
    restart_interval = WW3Stage.Input(timedelta, alias='series_interval')

    def execute(self):
        output_points = self.output_points or \
                                    self._get_output_points_from_geojson() or ()
        resampled_output_points = output_points and \
                                     self._resample_output_points(output_points)

        self._run_simulation(resampled_output_points)

    output_points_geojson = 'boundary.json'

    def _get_output_points_from_geojson(self):
        json_path = self.path / self.output_points_geojson

        try:
            json_dict = json.loads( json_path.read_text() )
            bnd_points = self.get_boundary_points_from_geojson(json_dict)
            return tuple( chain.from_iterable( bnd_points.values() ) )

        except FileNotFoundError:
            return

    @staticmethod
    def get_boundary_points_from_geojson(json_dict):
        coordinates_from_ = lambda feature: feature['geometry']['coordinates']
        is_ww3_ = lambda feature: feature['properties'].get('fmodel') == 'WW3'

        return dict(
            ( index, coordinates_from_(feature) )
                for index, feature in enumerate(json_dict['features'], start=1)
                                                             if is_ww3_(feature)
        )

    shel_max_output_points = 30

    def _resample_output_points(self, output_points):
            resample_step = ceil(len(output_points)/self.shel_max_output_points)

            if resample_step > 1:
                self.logger.warning('resample step = %d', resample_step)

            return output_points[::resample_step]

    def _run_simulation(self, output_points):
        self._format_shel_inp(output_points)
        self.run_ww3_bin('ww3_shel', mpi=True)

    shel_inp = 'ww3_shel.inp'

    def _format_shel_inp(self, output_points):
        output_point_lines = list( 
            f"{lon:<11.6f}  {lat:<11.6f}  '{self.output_point_prefix}{index}'"
                for index, (lon, lat) in enumerate(output_points, start=1)
        )
        if output_point_lines:
            output_point_lines.append("0.0  0.0  'STOPSTRING'")

        output_point_interval = timedelta(0) if not output_points else \
                                                      self.output_point_timestep

        self.format_ww3_inp_template(self.shel_inp,
            BEGIN_DATE_TIME = self.date_time,
            END_DATE_TIME = self.date_time + self.period,

            POINT_TIMESTEP_SECS = int( output_point_interval.total_seconds() ),
            POINT_LIST = '\n   '.join(output_point_lines),

            RESTART_INTERVAL_SECS = int( self.restart_interval.total_seconds() )
        )


import re
from netCDF4 import MFDataset
from numpy import deg2rad, sin, cos

from wiff.data.netcdf.creating import ncUGRID


class Outputs(WW3Stage):

    output_field = WW3Stage.Input(False)
    output_points = WW3Stage.Input(optional=True)

    ounp_inp = 'ww3_ounp.inp'
    ounf_inp = 'ww3_ounf.inp'

    def execute(self):
        self.output_steps = 1 + ( self.period // self.output_point_timestep )

        self._handle_point_output()
        self._handle_field_outputs()

    def _handle_point_output(self):
        output_point_indexes = self._get_requested_point_indexes_from_log()
        self.logger.debug("point indexes = %s", output_point_indexes)

        if output_point_indexes:
            self.format_ww3_inp_template(self.ounp_inp,
                BEGIN_DATE_TIME = self.date_time,
                OUTPUT_STEPS = self.output_steps,
                POINT_INDEX_LIST = '\n  '.join(output_point_indexes),
            )
            self.run_ww3_bin('ww3_ounp')

    def _get_requested_point_indexes_from_log(self):
        log_path = self.path / 'log.ww3'

        if not log_path.exists():
            return

        log_lines_iter = iter( log_path.read_text().splitlines() )

        for line in log_lines_iter:
            if 'Point output requested' in line:
                break
        else:
            return

        output_point_index_pattern = (
            r'(?P<index> \d+)'
            r'\s+ (?: \| .* ){2} \| \s+'
            f"{self.output_point_prefix}"
        )
        output_point_index_regex = re.compile(output_point_index_pattern,
                                                                     re.VERBOSE)

        searched_log_lines = tuple(
                          map(output_point_index_regex.search, log_lines_iter) )

        return tuple( line['index'] for line in searched_log_lines if line )

    def _handle_field_outputs(self):
        if self.output_field:
            self.format_ww3_inp_template(self.ounf_inp,
                BEGIN_DATE_TIME = self.date_time,
                OUTPUT_STEPS = self.output_steps,
            )
            self.run_ww3_bin('ww3_ounf')

            self._create_ww3_field_outputs_to_ugrid('ww3.????????.nc',
                                                                'ww3_fields.nc')

    def _create_ww3_field_outputs_to_ugrid(self, in_file_pattern, out_file):
        in_filepath_pattern = str( self.path / in_file_pattern )
        in_ds = MFDataset(in_filepath_pattern)
        self.logger.debug("%s", in_ds)

        time_var = in_ds.variables['time'][...]

        coords = dict(
            lat = in_ds.variables['latitude'][...],
            lon = in_ds.variables['longitude'][...],
        )

        nc = ncUGRID(self.path / out_file,
            time_data = in_ds.variables['time'][...],
            time_attrs = dict(
                standard_name = in_ds.variables['time'].standard_name,
                units = in_ds.variables['time'].units
            ),
            coordinates=coords,
            elements=in_ds.variables['tri'][...]
        )

        hs_data = in_ds.variables['hs'][...]
        dir_rad = deg2rad( in_ds.variables['dir'][...] + 180. )
        hs_u_data = hs_data * sin(dir_rad)
        hs_v_data = hs_data * cos(dir_rad)

        nc.add_variable('hs_u', hs_u_data, dict(
            standard_name = 'u-sea_surface_wave_significant_height_component',
            long_name = "U-component of Sea Surface Wave Significat Height",
            units = 'm',
        ) )
        nc.add_variable('hs_v', hs_v_data, dict(
            standard_name = 'v-sea_surface_wave_significant_height_component',
            long_name = "V-component of Sea Surface Wave Significat Height",
            units = 'm',
        ) )
        nc.add_variable('t02', in_ds.variables['hs'][...], dict(
            standard_name = 'sea_surface_wind_wave_mean_period_from_variance_'
                                     'spectral_density_second_frequency_moment',
            long_name = "Mean Period T02",
            units = 's',
        ) )
        pp_data = 1 / in_ds.variables['fp'][...]
        nc.add_variable('pp', pp_data, dict(
            standard_name = 'sea_surface_wind_wave_period',
            long_name = "Sea Surface Wind Wave Period",
            units = 's',
        ) )
        nc.close()


if __name__ == '__main__':

#     import logging
#     from datetime import datetime, date, time, timedelta
#     engine.core.parts import Simulation, Series

#     logging.basicConfig(level=logging.DEBUG)

#     def new_simulation(prepared_path=None, output_points=None):
#         step_init_common = dict(
#         )

#         steps = list()

#         if prepared_path:
#             bind = pre_compute = BindPrepared('bind',
#                 prepared_path = prepared_path,
#             )
#             steps.append(bind)

#         else:
#             prepare = Prepare('prepare',
#                 **step_init_common,
#             )
#             steps.append(prepare)

#             forcings = pre_compute = Forcings('forcings',
#                 dependencies = (prepare, ),
#                 **step_init_common,
#                 extent = dict(
#                     lat=(0., 75.),
#                     lon=(-98., 13.)
#                 )
#             )
#             steps.append(forcings)

#         compute = Compute('compute',
#             dependencies = (pre_compute, ),
#             output_points = output_points,
#             **step_init_common,
#         )
#         steps.append(compute)

#         steps.append(
#             Outputs('outputs',
#                 dependencies = (compute, ),
#                 output_points = output_points,
#                 **step_init_common,
#             )
#         )

#         return Simulation('ww3',
#             steps = steps,
#             period = timedelta(hours=48),
#         )

#     one_day = timedelta(days=1)
#     today = datetime.combine( date.today(), time(0) ) - one_day * 1

#     main_ww3 = Series('waves_na',
#         simulation = new_simulation(),
#         interval = one_day,
#         begin = today - one_day,
#         end = today,
#         path = Path.cwd(),
#     )
#     main_ww3_context = main_ww3.run_simulation(
# #        steps = (finalize, ),
#         date_time = today,
#     )

#     new_simulation(
#         prepared_path = main_ww3_context['path'],
#         output_points = (
#             (-8.726389, 40.734461),
#             (-8.734045, 40.734664),
#             (-8.751085, 40.734317),
#             (-8.767922, 40.733308),
#             (-8.784399, 40.730485),
#             (-8.800178, 40.725905),
#             (-8.814936, 40.719661),
#             (-8.828373, 40.711882),
#             (-8.840216, 40.702724),
#             (-8.850221, 40.692377),
#             (-8.858188, 40.681051),
#             (-8.863954, 40.668976),
#             (-8.867402, 40.656401),
#             (-8.868465, 40.643578),
#             (-8.867121, 40.630771),
#             (-8.863399, 40.61824),
#             (-8.857377, 40.606239),
#             (-8.849178, 40.595014),
#             (-8.83897, 40.584792),
#             (-8.82696, 40.575779),
#             (-8.812895, 40.567596),
#             (-8.800485, 40.56234),
#             (-8.787495, 40.556863),
#             (-8.777442, 40.552811),
#         ),
#     ).run(
#         path = Path.cwd(),
#         date_time = today,
#     )
    import logging
    from datetime import datetime, date, time, timedelta
    from wiff.engine.parts import Simulation, Series
    from wiff.engine.stages.local_workplace import LocalWorkplace0

    logging.basicConfig(level=logging.DEBUG)

    def new_simulation(prepared_path=None, output_points=None, **stage_context):

        stage_templates = [
            LocalWorkplace0.template('workplace'),
        ]

        if prepared_path:
            stage_templates.append(
                BindPrepared.template('bind',
                    prepared_path = prepared_path,
                )
            )

        else:
            stage_templates.append(
                Prepare.template('prepare',
                )
            )

            stage_templates.append(
                Forcings.template('forcings',
                    #dependencies = (prepare, ),
                    extent = dict(
                        lat=(0., 75.),
                        lon=(-98., 13.)
                    ),
                )
            )

        stage_templates.append(
            Compute.template('compute',
                #dependencies = (pre_compute, ),
                output_points = output_points,
            )
        )

        stage_templates.append(
            Outputs.template('outputs',
                #dependencies = (compute, ),
                output_field = True,
                output_points = output_points,
            )
        )

        return Simulation.template('ww3',
            stage_templates = stage_templates,
            stage_context = stage_context,
        )

    one_day = timedelta(days=1)
    today = datetime.combine( date.today(), time(0) )
    yesterday = today - one_day

    common_context = dict(
        period = timedelta(hours=48),

        bin_path = Path('/home/jrogeiro/projects/wiff/forecasting/bin/ww3/'),
        static_path = Path(
                        '/home/jrogeiro/projects/wiff/forecasting/static/ww3/'),
    )

    main_ww3 = Series('waves_na',
        begin = yesterday,
        interval = one_day,
        #end = today,
        simulation_templates = (
            new_simulation(**common_context),
        ),
    )
    main_ww3_series = main_ww3.simulate()

    new_simulation(
        **common_context,
        date_time = yesterday,
        prepared_path = main_ww3_series[yesterday]['ww3'].ran_steps[0].path,
        output_points = (
            (-8.726389, 40.734461),
            (-8.734045, 40.734664),
            (-8.751085, 40.734317),
            (-8.767922, 40.733308),
            (-8.784399, 40.730485),
            (-8.800178, 40.725905),
            (-8.814936, 40.719661),
            (-8.828373, 40.711882),
            (-8.840216, 40.702724),
            (-8.850221, 40.692377),
            (-8.858188, 40.681051),
            (-8.863954, 40.668976),
            (-8.867402, 40.656401),
            (-8.868465, 40.643578),
            (-8.867121, 40.630771),
            (-8.863399, 40.61824),
            (-8.857377, 40.606239),
            (-8.849178, 40.595014),
            (-8.83897, 40.584792),
            (-8.82696, 40.575779),
            (-8.812895, 40.567596),
            (-8.800485, 40.56234),
            (-8.787495, 40.556863),
            (-8.777442, 40.552811),
        ),
    ).render().run()
