from datetime import timedelta, date, time, datetime
from pathlib import Path

from wiff.engine.parts import Stage
from wiff.engine.mixins.file_handler import FileHandler
from wiff.engine.mixins.process_handler import ProcessHandler


class XBeachStage(Stage, FileHandler, ProcessHandler):

    datetime_today = lambda: datetime.combine( date.today(), time() )
    default_simulation_period = timedelta(hours=48)

    path = Stage.Input(Path.cwd)
    date_time = Stage.Input(datetime_today)
    period = Stage.Input(default_simulation_period)


from .contrib.nc_to_sp2 import main as nc_to_sp2


class BindSCHISM(XBeachStage):

    schism_circulation_stations = XBeachStage.Input()
    schism_wave_spectrum_stations = XBeachStage.Input() # wave_spectrum OR wave

    schism_simulation_path_template = XBeachStage.Input('.')
    schism_stations_elevation_path = XBeachStage.Input(optional=True)
    schism_stations_wave_path = XBeachStage.Input(optional=True)

    def execute(self):
        self.convert_elevation()
        self.convert_spectrum()

    schism_stations_elevation_file = 'outputs/staout_1'
    xbeach_elevation_file = 'elev_xb.txt'

    def _schism_path(self, *suffixes):
        return Path(
            self.date_time.strftime(self.schism_simulation_path_template),
                                                                       *suffixes
        )

    def convert_elevation(self):
        schism_stations_elevation_path = self.schism_stations_elevation_path \
                       or self._schism_path(self.schism_stations_elevation_file)

        if not schism_stations_elevation_path.is_file():
            raise ValueError(f"{schism_stations_elevation_path} not found!")

        if not self.schism_circulation_stations:
            raise ValueError("No elevation station indexes supplied!")

        self.link_file(schism_stations_elevation_path)

        def extract_elevations_lines():
            stations_file_lines = \
                 schism_stations_elevation_path.read_text().strip().splitlines()

            for line in stations_file_lines:
                timestamp, *elevations = line.split()

                station_elevations = map(
                    lambda index: float(elevations[index-1]),
                                                self.schism_circulation_stations
                )
                line_elements = (float(timestamp), *station_elevations)
                yield ' '.join( map(str, line_elements) )

        self.create_text_file(self.xbeach_elevation_file,
                                       '\n'.join( extract_elevations_lines() ) )

    schism_stations_wave_file = 'outputs/wwm_stations.nc'
    xbeach_sp2_file_prefix = 'cg' # is it a good attr name?

    def convert_spectrum(self):
        schism_stations_wave_path = self.schism_stations_wave_path or \
                               self._schism_path(self.schism_stations_wave_file)
        self.link_file(schism_stations_wave_path)

        nc_to_sp2('wwm', schism_stations_wave_path, self.xbeach_sp2_file_prefix,
                                  self.schism_wave_spectrum_stations, self.path)


class Compute(XBeachStage):

    def execute(self):
        self._run_simulation()
    
    xbeach_bin = 'xbeach-r5527'

    def _run_simulation(self):
        bin_path = self.bin_path / self.xbeach_bin
        stdout_path = self.path / 'xbeach.out'

        self.run_executable_mpi(bin_path, check=True, verbose=False)


if __name__ == '__main__':
    import logging
    from datetime import datetime, date, time, timedelta
    from wiff.engine.parts import Simulation

    logging.basicConfig(level=logging.DEBUG)

    one_day = timedelta(days=1)
    today = datetime.combine( date.today(), time(0) )
    yesterday = today - one_day

    schism_outputs = Path('/_data/runs/schism-testing/_latest/outputs')

    Simulation('xbeach',
        stage_context = dict(
            date_time = yesterday,
        ),
        stage_templates = (
            # Workplace.template('workplace'),

            BindSCHISM.template('bind',
                path = Path('/_data/runs/xbeach-testing'),
                file_overwrite = True,

                # schism_path = Path(''),
                schism_stations_elevation_path = schism_outputs / 'staout_1',
                schism_circulation_stations = (1, 2),
                schism_stations_wave_path = schism_outputs / 'wwm_sta.nc',
                schism_wave_spectrum_stations = (1, 2),
            ),
        ),
    ).run()
